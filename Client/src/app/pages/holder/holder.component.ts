import { Component, OnInit } from '@angular/core';
import { NavItem, NavItemType } from '../../lbd/services/interface';

@Component({
  selector: 'app-holder',
  templateUrl: './holder.component.html',
  styleUrls: ['./holder.component.scss']
})
export class HolderComponent implements OnInit {
  public navItems: NavItem[];
  public sidenavItems: any[];
  public logoDropdown: any;
  public sidebarData: any;
  public fixedPluginData: any;
  constructor() { }
  ngOnInit() {
    this.fixedPluginData = {
      filters: [
        { class: '', color: 'black' },
        { class: 'badge-azure', color: 'azure' },
        { class: 'badge-green', color: 'green' },
        { class: 'badge-blue', color: 'blue' },
        { class: 'badge-orange', color: 'orange' },
        { class: 'badge-red', color: 'red' },
        { class: 'badge-purple', color: 'purple' },
        { class: '', color: 'black' },
        { class: 'badge-azure', color: 'azure' },
        { class: 'badge-green', color: 'green' },
        { class: 'badge-orange', color: 'orange' },
        { class: 'badge-red', color: 'red' },
        { class: 'badge-purple', color: 'purple' }
      ],
      bgimg: [
        { url: 'assets/images/full-screen-image-1.jpg' },
        { url: 'assets/images/full-screen-image-2.jpg' },
        { url: 'assets/images/full-screen-image-3.jpg' },
        { url: 'assets/images/full-screen-image-4.jpg' },
        { url: 'assets/images/full-screen-image-1.jpg' },
        { url: 'assets/images/full-screen-image-2.jpg' },
        { url: 'assets/images/full-screen-image-3.jpg' },
        { url: 'assets/images/full-screen-image-4.jpg' },
      ]
    }
    this.sidebarData = {
      sidenavItems: [

        { title: 'Calender', routerLink: 'calender', iconClass: 'pe-7s-date', href: 'calender' },
        {
          title: 'Components', iconClass: 'pe-7s-plugin', href: 'components', sub: [

            { title: 'Notifications', routerLink: 'noti', iconClass: 'pe-7s-science' },
            { title: 'Panels', routerLink: 'panels', iconClass: 'pe-7s-map-marker' },
            { title: 'Sweet Alerts', routerLink: 'sweetalerts', iconClass: 'pe-7s-map-marker' },
          ]
        },
        {
          title: 'Widgets', iconClass: 'pe-7s-bicycle', href: 'widgets', sub: [
            { title: 'Todo', routerLink: 'todo' },
            { title: 'Todo Summary', routerLink: 'todosum' },
            { title: 'User Information', routerLink: 'userinfo' },
            { title: 'Equation Charts', routerLink: 'eqchart' },

          ]
        },

        {
          title: 'Pages', iconClass: 'pe-7s-gift', href: 'pages', sub: [
            { title: 'Login Page', routerLink: '../login', iconClass: 'pe-7s-user' },
            { title: 'Register Page', routerLink: '../register', iconClass: 'pe-7s-note2' },
            { title: 'Lock Screen Page', routerLink: '../lockscreen', iconClass: 'pe-7s-news-paper' },
            { title: 'User Page', routerLink: 'userpage', iconClass: 'pe-7s-science' }
          ]
        },
      ],
      logoDropdown: {
        header: "Name",
        list: [
          { title: "My Profile" },
          { title: "Edit Profile" },
          { title: "Settings" }
        ]
      },
      sidebarMeta: {
        headerTextFull: 'Paper Clip',
        headerTextMini: 'PC',
        headerLink: 'http://www.google.com',
        headerLogoImg: '/assets/images/profile.jpg',
        backgroundColor: 'blue',
        backgroundImg: '../../assets/images/full-screen-image-3.jpg',
      }
    }

    this.navItems = [

      {
        type: NavItemType.NavbarRight,
        title: ' Notifications',
        iconClass: 'fa fa-bell-o',
        ListClass: 'dropdown',
        numNotifications: 3,
        dropdownClass: 'dropdown-menu',

        dropdownItems: [
          { title: 'Notification 1' },
          { title: 'Notification 2' },

          { title: 'Another Notification' }
        ]
      },
      {
        type: NavItemType.NavbarRight,
        title: 'Settings',
        iconClass: 'fa fa-list',
        ListClass: 'dropdown dropdown-with-icons  ',
        dropdownClass: 'dropdown-menu dropdown-with-icons',

        dropdownItems: [

          { title: 'Settings', icon: 'pe-7s-tools' },
          'separator',
          { title: 'Lock Screen', icon: 'pe-7s-lock' },
          { title: 'Log out', icon: 'pe-7s-close-circle', class: 'text-danger' }
        ]
      },

    ];

  }

}
