import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmoreSaleComponent } from './addmore-sale.component';

describe('AddmoreSaleComponent', () => {
  let component: AddmoreSaleComponent;
  let fixture: ComponentFixture<AddmoreSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmoreSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmoreSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
