import { Component, OnInit,Input,AfterViewInit,ViewChild,Output,EventEmitter } from '@angular/core';

declare var $

@Component({
  selector: 'app-c-select',
  templateUrl: './c-select.component.html',
  styleUrls: ['./c-select.component.scss']
})
export class CSelectComponent implements OnInit {

@Input()
public schema:any;

@Input()
public updateValue:any;




@Output() notify = new EventEmitter<any>();
@ViewChild('value') value; 

  constructor() { }

  ngOnInit() {
    if(this.updateValue){
      console.log("c-select ssssssssssssssssssss",this.updateValue)
      this.schema.schema.dataTitle=this.updateValue
    }
  }
  ngAfterViewInit(){
    $(".bootstrap-select ").addClass("dropup");
    
  }

  getValue(x,name){
    var array;
    for(let i=0;i<this.value.nativeElement.selectedOptions.length;i++){

      if(array===undefined){
        array = this.value.nativeElement.selectedOptions[0].value
      }else{
        array=array + ','+this.value.nativeElement.selectedOptions[i].value

      }
      var obj={
        name:name,
        value:array
      }
      console.log("select component value sssssssssss",obj)
    }

    this.notify.emit(obj);


  }
}
