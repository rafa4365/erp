import { Injectable } from '@angular/core';

@Injectable()
export class AuthLoginService {

  private isLoggedIn: boolean;

  constructor() { this.isLoggedIn = false }

  setLoggedIn() {
    this.isLoggedIn = true;
    localStorage.setItem('login', JSON.stringify(true));

  }

  getLoggedIn(): boolean {
    this.isLoggedIn = JSON.parse((localStorage.getItem('login')));
    return this.isLoggedIn;
  }

  setlogout() {
    this.isLoggedIn = false;
    localStorage.setItem('login', JSON.stringify(false));
  }

}
