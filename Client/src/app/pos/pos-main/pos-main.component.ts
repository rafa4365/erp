import { Component, OnInit } from '@angular/core';
import { sidebarData,navItems } from '../pos-schema';

@Component({
  selector: 'app-pos-main',
  templateUrl: './pos-main.component.html',
  styleUrls: ['./pos-main.component.scss']
})
export class PosMainComponent implements OnInit {

  constructor() {
    Object.assign(this, {sidebarData, navItems})            
   }

  ngOnInit() {}

}
