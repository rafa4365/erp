import { Component, OnInit } from '@angular/core';
import { NavbarTilteService } from '../../../lbd/services/navbar-tilte.service';

@Component({
  selector: 'app-panels',
  templateUrl: './panels.component.html',
  styleUrls: ['./panels.component.scss']
})
export class PanelsComponent implements OnInit {
public collapseData:any;
public collapseHoverData:any;

public tabsData:any;
public tabsIconsData:any;

public pageSubcategoriesData:any;

constructor(private navbarTitleService: NavbarTilteService) { }

  ngOnInit() {
    this.navbarTitleService.updateTitle('Panels');
    
    this.collapseData={
      title:'Collapsible Accordion',subTitle:'Bootstrap default style',toggleType:'collapse',list:[
        {title:'collapse-1',toggleId:'collapse-1',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'collapse-2',toggleId:'collapse-2',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'collapse-3',toggleId:'collapse-3',data:'data  Anim pariatur cliche reprehenderit,'},
      ],
    };
    this.collapseHoverData={
      title:'Collapsible Accordion',subTitle:'Bootstrap default style',toggleType:'collapse-hover',list:[
        {title:'collapse-1',toggleId:'h-collapse-1',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'collapse-2',toggleId:'h-collapse-2',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'collapse-3',toggleId:'h-collapse-3',data:'data  Anim pariatur cliche reprehenderit,'},
      ],
    };
    this.tabsData={
      title:'Tabs',subTitle:'Plain text tabs',toggleType:'tab',list:[
        {title:'tab-1',tabId:'tab-1',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'tab-2',tabId:'tab-2',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'tab-3',tabId:'tab-3',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'tab-4',tabId:'tab-4',data:'data  Anim pariatur cliche reprehenderit,'},
      ],
    };
    this.tabsIconsData={
      title:'Tabs & Icons',subTitle:'Tabs with icons and full width',toggleType:'tab',list:[
        {title:'tab-1',icon:'fa fa-info',tabId:'i-tab-1',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'tab-2',icon:'fa fa-user',tabId:'i-tab-2',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'tab-3',icon:'fa fa-cube',tabId:'i-tab-3',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'tab-4',icon:'fa fa-cog',tabId:'i-tab-4',data:'data  Anim pariatur cliche reprehenderit,'},
      ],
    };
    this.pageSubcategoriesData={
      title:'Page Subcategories',toggleType:'tab',list:[
        {title:'Description about product',subTitle:'More information here',header:'Description about product',icon:'fa fa-info',iconTitle:'Description',tabId:'p-tab-1',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'Location of product',subTitle:'Here is some text',header:'Location of product',icon:'fa fa-map-marker',iconTitle:'Location',tabId:'p-tab-2',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'Legal items',subTitle:'More information here',header:'Legal items',icon:'fa fa-legal',iconTitle:'Legal info',tabId:'p-tab-3',data:'data  Anim pariatur cliche reprehenderit,'},
        {title:'Help center',subTitle:'More information here',header:'Help center',icon:'fa fa-life-ring',iconTitle:'Help center',tabId:'p-tab-4',data:'data  Anim pariatur cliche reprehenderit,'},
      
      ],
    };
    // //console.log('data',this.collapseData)

  }

}
