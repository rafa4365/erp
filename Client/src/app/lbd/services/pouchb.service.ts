import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthLoginService } from './auth-login.service';

declare let PouchDB: any;
declare let await: any;
declare let emit: Function;
declare let window: any;
declare let attr: any;
declare let doc: any;
// declare let obj: any;

@Injectable()
export class PouchbService {

  private pouchDb: any;
  private pouchDbEventEmitter: any;
  private pouchDbSyncEventEmitter: any;

  constructor(    
    private loginService: AuthLoginService,
    private router: Router, 
    
  ) { }

  declare() {
    this.pouchDb = new PouchDB('http://localhost:3000/testdb');
    this.pouchDb.info().then((info) => {
    })
  }
  nameresult(value) {
    let val = { $regex: value.value }

    var options: Object = {
      selector: {
        $and: [
          { name: val },
          { view: { $eq: value.view } }
        ]

      }
    }
    return new Promise((resolve, reject) => {
      this.pouchDb.find(options)
        .then((result: any) => {
          resolve(result.docs)
        })
        .catch(reject)
    });

  }
  addmorestatic(value, view) {
    let val = { $regex: value.value }
    var options: Object = {
      selector: {
        $and: [
          { itemName: val },
          { view: { $eq: view } }
        ]

      }
    }
    return new Promise((resolve, reject) => {
      this.pouchDb.find(options)
        .then((result: any) => {
          resolve(result.docs[0])
        })
        .catch(reject)
    });

  }
  dbsearchselect(value) {
    let val = { $regex: value.value }

    var options: Object = {
      selector: {
        $and: [
          { [value.name]: val },
          { view: { $eq: value.view } }
        ]

      }
    }
    return new Promise((resolve, reject) => {
      this.pouchDb.find(options)
        .then((result: any) => {
          resolve(result.docs)
        })
        .catch(reject)
    });

  }
  pagination(skip, limit, design): Promise<any> {
    var options = {
      include_docs: true,
      limit: limit,
      skip: skip
    }
    return new Promise((resolve, reject) => {

      this.pouchDb.query(`${design.docDesign}/${design.view}`, options)
        .then((result: any) => {

          //  this.debug.clog('pouchdb service fetchData result','s',result)    
          resolve(result);
        })
        .catch(reject)
    });
  }
  createView(value, limit): Promise<any> {

    console.log('view generation', value);
    console.log('view id', `_design/${value.docDesign}`);

    // save the design doc
    var origDoc;
    var options = {
      selector: { _id: `_design/${value.docDesign}` }
    }
    var adoc = {
      _id: `_design/${value.docDesign}`,
      _rev: '1-lala',
      views: {
        [value.view]: {
          map: "function(doc) {\
      if(doc.view == " + JSON.stringify(value.view) + ") {\
        emit(doc.view);\
      }\
    }"}

      }
    }
    this.pouchDb.put(adoc, { force: true, allow_conflict: true })
    return new Promise((resolve, reject) => {

      this.pouchDb.find(options).then((_origDoc: any) => {
        origDoc = _origDoc;
        console.log('getting design doc', _origDoc)
        console.log('getting design doc rev', _origDoc.docs[0]._rev)
        return origDoc
      })
        .then((doc: any) => {
          var oldviews = doc.docs[0].views;
          var genviews = doc.docs[0].views;

          for (var i = 0; i < Object.keys(doc.docs[0].views).length; i++) {
            oldviews[value.view] = {
              map: "function(doc) {\
              if(doc.view == " + JSON.stringify(value.view) + ") {\
                emit(doc.view);\
              }\
            }"}

          }
          genviews = oldviews
          console.log('doc old length', i, oldviews)
          console.log('doc gen length', i, genviews)
          var ddoc = {
            _id: `_design/${value.docDesign}`,
            _rev: doc.docs[0]._rev,
            views: genviews
          }
          this.pouchDb.put(ddoc, { force: true, allow_conflict: true }).catch((err) => {
            if (err.name !== 'conflict') {
              console.log('view exists');
            }
            console.log('view error', err);
            // return origDoc

            // ignore if doc already exists
          }).then(() => {
            this.fetchData(limit, value).then((result) => {
              resolve(result)
            })
          })
        }).catch(reject)
    });

  }

  fetchData(pagelimit, design): Promise<any> {
    if (pagelimit === 'Showing All') {
      var options: Object = {
        include_docs: true,
      }
    } else {
      var options: Object = {
        include_docs: true,
        limit: pagelimit
      }
    }

    // if(pagelimit==='all'){
    //   options={
    //     include_docs:true  }
    // }
    return new Promise((resolve, reject) => {

      this.pouchDb.query(`${design.docDesign}/${design.view}`, options)
        .then((result: any) => {

          resolve(result);
        })
        .catch(reject)
    });
  }
  // //-------supplier----------
  // fetchSupplier(design): Promise<any>{
  //     var options:Object={
  //       include_docs:true,

  //   }

  //   return new Promise((resolve,reject) =>{

  //     this.pouchDb.query(`${design.docDesign}/${design.findView}`,options)
  //    .then((result:any)=>{
  //      resolve(result);
  //    })
  //    .catch(reject)
  //  });
  // }
  remove(data) {
    return new Promise((resolve, reject) => {
      this.pouchDb.remove(data)
        .then(() => {
          resolve(data)
        })
        .catch(reject)
    });
  }
  query(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.pouchDb.put(data)
        .then(() => {
          resolve(data)
        })
        .catch(reject)
    });

  }
  fetch(value):Promise<any>{
    return new Promise((resolve, reject) => {
      var option={
        include_docs: true        
      }
      this.pouchDb.get(value,option)
        .then((doc) => {
          resolve(doc)
        })
        .catch(reject)
    });
  }
  update(data): Promise<any> {
    var options = {
      force: true
    }
    return new Promise((resolve, reject) => {
      this.pouchDb.put(data, options)
        .then(() => {
          resolve(data)
        })
        .catch(reject)
    });

  }
  check(value,field, view) {
    // let val = { $regex: value }
    // var searcharray ;
    // for (var i = 0; i < temp.length; i++) {
      // searcharray = val 
    // }
    // searcharray.push({view:design.view})
    var options: Object = {
      selector: {[field]: {$regex: value}}
    }
    return new Promise((resolve, reject) => {
      this.pouchDb.find(options)
        .then((result: any) => {
          console.log('check result service',result)
          console.log('check value service',value)
          console.log('check view service',view)
          if(result.docs.length===1){
            resolve(true)
          }else if(result.docs.length===0){
            resolve(false)
            
          }
        })
        .catch(reject)
    });


  }
  login(username,pass, view) {
    var options: Object = {
      selector: {
        $and: [
          { userName: { $eq: username } },
          { password: { $eq: pass } },
          { view: { $eq: view } },
          { allowed: { $eq: true } }          
        ]
      }
    }
    return new Promise((resolve, reject) => {
      this.pouchDb.find(options)
        .then((result: any) => {
          console.log('login result service',pass)
          console.log('login value service',username)
          console.log('login view service',view)
          if(result.docs.length===1){
            if(result.docs[0].department==='hr'){
              this.loginService.setLoggedIn();
              this.router.navigate(['hrMain']);
              resolve(true)
            }
            if(result.docs[0].department==='pop'){
              this.loginService.setLoggedIn();
              this.router.navigate(['popMain']);
              resolve(true)
            }
            if(result.docs[0].department==='pos'){
              this.loginService.setLoggedIn();
              this.router.navigate(['posMain']);
              resolve(true)
            }
            if(result.docs[0].department==='admin'){
              this.loginService.setLoggedIn();
              this.router.navigate(['adminMain']);
              resolve(true)
            }
            if(result.docs[0].department==='db'){
              this.loginService.setLoggedIn();
              window.location.href = 'http://localhost:3000/_utils/#/_all_dbs';
              resolve(true)
            }
            if(result.docs[0].department==='tem'){
              this.loginService.setLoggedIn();
              this.router.navigate(['holder']);
              resolve(true)
            }
          }else if(result.docs.length===0){
            resolve(false)
            
          }
        })
        .catch(reject)
    });


  }
  search(value, temp, design) {
    let val = { $regex: value }
    var searcharray = []
    for (var i = 0; i < temp.length; i++) {
      searcharray[i] = { [temp[i].name]: val }
    }
    // searcharray.push({view:design.view})
    var options: Object = {
      selector: {
        $and: [
          { $or: searcharray },
          { view: { $eq: design.view } }
        ]

      }
    }
    return new Promise((resolve, reject) => {
      this.pouchDb.find(options)
        .then((result: any) => {
          resolve(result)
        })
        .catch(reject)
    });


  }

  ///////////---------------------Summary Function of POP Supplier ------------------///////////////////////////////////

  summaryFun(mapFunc, options): Promise<Array<any>> {
    return new Promise((resolve, reject) => {

      this.pouchDb.query(mapFunc, options)
        .then((result: any) => {
          console.log("result.rows", result.rows);
          resolve(result.rows);
        })
        .catch(reject);
    });
  }


}
