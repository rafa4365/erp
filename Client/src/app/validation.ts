export var validation = {
    email: {
        regex: /^([A-Z|a-z|0-9](\.|_){0,1})+[A-Z|a-z|0-9]\@([A-Z|a-z|0-9])+((\.){0,1}[A-Z|a-z|0-9]){2}\.[a-z]{2,3}$/,
        msg: 'Email must be in  proper formate i.e. abc@abc.com .'
    },
    username: {
        regex: /^([A-Za-z0-9]){3,20}$/gm,
        msg: 'Value must be from 3 to 20 characters in length, only allow letters and numbers, no special characters, full line is evaluated'
    },
    password: {
        regex: /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,15})/g,
        msg: 'Ensure that password is 8 to 15 characters long and contains a mix of upper and lower case characters, one numeric and one special character'
    },
    fullname: {
        regex: /^\s*(?:((?:Dr.)|(?:Mr.)|(?:Mr?s.)|(?:Miss)|(?:2nd\sLt.)|(?:Sen\.?))\s+)?((?:\w+)|(?:\w\.))(?:\s+((?:\w\.?)|(?:\w\w+)))?(?:\s+((?:[OD]['â€™]\s?)?[-\w]+))(?:,?\s+( (?:[JS]r\.?)|(?:Esq\.?)|(?:((?:M)|(?:Ph)|(?:Ed)) \.?\s*D\.?)|(?: R\.?N\.?)|(?:IV)|(?:VI)|(?:V)|(?: I+)))?\s*$/,
        msg: 'Please Write Full name i.e. Rana Faheem.'
    },
    phone: {
        regex: /^(\+[0-9]{1,3}|0)[0-9]{3}( ){0,1}[0-9]{7,8}\b/,
        msg: 'Please Write 11 Digit Phone number i.e. 0423XXXXXXX.'
    },
    address: {
        regex: /^([a-zA-z0-9 ,#.]{3,70})$/,
        msg: 'Address must Contain 1 to 35 Words or Avoid Special Chearacters.'
    },
    information: {
        regex: /^([a-zA-z0-9 ,#.";?<>?,./':~@#$%^&*!()_+=-|1]{3,300})$/,
        msg: 'Information must Contain 1 to 70 Words or Avoid Special Characters.'
    },
    purchaseorder: {
        regex: /([PO][O2]{1,2})([0-9]{6}$)/m,
        msg: 'Purchase Order must contain PO prefix and must contain 6 Digit number i.e. PO123456.'
    },
    saleorder: {
        regex: /([SO][O2]{1,2})([0-9]{6}$)/m,
        msg: 'Sale Order must contain SO prefix and must contain 6 Digit number i.e. SO123456.'
    },
    deliverynote: {
        regex: /([DN][N2]{1,2})([0-9]{6}$)/m,
        msg: 'Delivery Note no. must contain DN prefix and must contain 6 Digit number i.e. DN123456.'
    },
    salequote: {
        regex: /([SQ][Q2]{1,2})([0-9]{6}$)/m,
        msg: 'Sale Quote must contain SQ prefix and must contain 6 Digit number i.e. SQ123456.'
    },
    expenceno: {
        regex: /([BE][E2]{1,2})([0-9]{6}$)/m,
        msg: 'Business Expence must contain BE prefix and must contain 6 Digit number i.e. BE123456.'
    },
    purchaseinvoice: {
        regex: /([PI][I2]{1,2})([0-9]{6}$)/m,
        msg: 'Purchase Invoice must contain PI prefix and must contain 6 Digit number i.e. PI123456.'
    },
    saleinvoice: {
        regex: /([SI][I2]{1,2})([0-9]{6}$)/m,
        msg: 'Sale Invoice must contain SI prefix and must contain 6 Digit number i.e. SI123456.'
    },
    itemcode: {
        regex: /([IC][C2]{1,2})([0-9]{6}$)/m,
        msg: 'Item Code must contain IC prefix and must contain 6 Digit number i.e. IC123456.'
    },
    shortdescription: {
        regex: /^([a-zA-z0-9 ,#.";?<>?,./':~@#$%^&*!()_+=-|1]{3,50})$/,
        msg: 'must Contain 1 to 20 Words.'
    },
    description: {
        regex: /^([a-zA-z0-9 ,#.";?<>?,./':~@#$%^&*!()_+=-|1]{3,100})$/,
        msg: 'must Contain 1 to 20 Words.'
    },
    price: {
        regex: /^(?!0)(\d){1,7}$/,
        msg: 'must Contain lessr then 7 digit Positive Number.'
    },
    unit: {
        regex: /((em|ex|%|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|Unit|vmax|kg|unit|Kg))/,
        msg: 'em|ex|%|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|unit|vmax|Kg'
    },
    itemname: {
        regex: /^([a-zA-z0-9]{3,70})$/,
        msg: 'must not Contain Special Cherecters. '
    }
    // ,

    // date: {
    //     regex: /^((0[1-9]|1[0-2])[\/](0[1-9]|[12]\d|3[01])[\/](19|20)\d{2})(.)((([0]?[1-9]|1[0-2])(:[0-5]\d)(\ [AaPp][Mm]))|(([0|1]\d?|2[0-3])(:[0-5]\d)))$/,
    //     msg: 'Please Check it Again i.e. 12/04/2017 12:00 AM '
    // }
}

