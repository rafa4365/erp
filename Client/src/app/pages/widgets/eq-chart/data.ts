var date={
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-chevron-up",
      down: "fa fa-chevron-down",
      previous: 'fa fa-chevron-left',
      next: 'fa fa-chevron-right',
      today: 'fa fa-screenshot',
      clear: 'fa fa-trash',
      close: 'fa fa-remove'
    }
}

export var eq_data={
    title:'Testing', 
    type:null,
    table: {
      docDesign: 'widgets',
      view: 'appuser',
      export: false,
      colView: false,
      gridView: false,
      refresh: true,
      search: false,
      dateRange: false  ,
      settings:false,
      options:{
        display:true,
        rowView:false,
        rowDelete:true,
        rowUpdate:true,
      },
    },
    numrow: [10, 25, 100],
    submit:true,
    rows:[
        {col:[
          {class:'col-md-5',data:{label:'Company',name:'company',type:'text',fieldtype:'input',showCol:true,placeholder:'Company',value:'',valid:'',ragexp:'^([a-z A-Z]{3,})$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-3',data:{label:'Username',name:'username',type:'text',fieldtype:'input',showCol:false,placeholder:'Username',value:'',valid:'',ragexp:'^([a-z0-9]{5,})$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-4',data:{label:'Email address',name:'email',type:'text',fieldtype:'input',showCol:false,placeholder:'Email',value:'',valid:'',ragexp:'^([a-z A-Z]{3,})$',class:'form-control',err:'this is invalid error '}}
        ]},
        {col:[
          {class:'col-md-6',data:{label:'First Name',name:'firstName',type:'text',fieldtype:'input',showCol:true,placeholder:'First Name',value:'',valid:'',ragexp:'^([a-zA-Z]{1,})$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-6',data:{label:'Last Name',name:'lastName',type:'text',fieldtype:'input',showCol:false,placeholder:'Last Name',value:'',valid:'',ragexp:'^([a-zA-Z]{1,})$',class:'form-control',err:'this is invalid error '}},
        ]},
        {col:[
          {class:'col-md-6',data:{label:'Password',name:'password',type:'password',fieldtype:'input',showCol:false,placeholder:'Password',value:'',valid:'',ragexp:'^([a-zA-Z0-9\!\@\$\%\^\&\*]{8,})$',class:'form-control',err:'this is invalid error '}}
        ]},
        {col:[
          {class:'col-md-12',data:{label:'Address',name:'address',type:'text',fieldtype:'input',showCol:false,placeholder:'Address',value:'',valid:'',ragexp:'^([a-z A-Z 0-9 \#]{5,})$',class:'form-control',err:'this is invalid error '}},
        ]},
        {col:[
          {class:'col-md-4',data:{label:'City',name:'city',type:'text',fieldtype:'input',showCol:true,placeholder:'City',value:'',valid:'',ragexp:'^([a-z A-Z]{3,})$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-4',data:{label:'Country',name:'country',type:'text',fieldtype:'input',showCol:true,placeholder:'Country',value:'',valid:'',ragexp:'^([a-z A-Z]{3,})$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-4',data:{label:'Postal Code',name:'postCode',type:'text',fieldtype:'input',showCol:true,placeholder:'Zip Code',value:'',valid:'',ragexp:'^([0-9]{5,})$',class:'form-control',err:'this is invalid error '}},
        ]},
        {col:[
          {class:'col-md-12',data:{label:'About Me',name:'aboutMe',type:'textarea',rows:'5',fieldtype:'textarea',showCol:false,placeholder:'Here can be your description',value:'',valid:'',ragexp:'^([a-z A-Z 0-9 \#]{5,})$',class:'form-control',err:'this is invalid error of text area'}},
        ]},
        {col:[
          {class:'col-md-12',data:{name:'expense',fieldtype: 'addmoreexpensevalid',showCol:false,view:'expenseCategory',docDesign:'pop',searchvalue:'expencename',value:'',valid:null}}
        ]},
        {col:[
          {class:'col-md-12',data:{name:'customer',fieldtype: 'nameresult',showCol:false,view:'supplier',docDesign:'pop',searchvalue:'name',resultvalue:'address',value:'',valid:null}},
        ]},
        {col:[
          {class:'col-md-12',data:{name:'inventory',fieldtype: 'addmorevalid',showCol:false,view:'inventory',docDesign:'pop',searchvalue:'saleprice',value:'',valid:null}},
        ]},
        {col:[
          {class:'col-md-6',data:{label:'Joining Date',options:date,class: 'datetimepicker', placeholder: 'Joining date here',name:'joinDate',fieldtype:'datepicker',showCol:false,value:'',valid:'',ragexp:'^([a-zA-z0-9 /:]{5,})$',err:'this is invalid error of text area'}},
          {class:'col-md-3',data:{label:'leaving Date',options:date,class: 'datetimepicker', placeholder: 'leaving date here',name:'leaveDate',fieldtype:'datepicker',showCol:false,value:'',valid:'',ragexp:'^([a-zA-z0-9 /:]{5,})$',err:'this is invalid error of text area'}},
          {class:'col-md-3',data:{label:'Date',options:date,class: 'datetimepicker', placeholder: 'date here',name:'Date',fieldtype:'datepicker',showCol:false,value:'',valid:'',ragexp:'^([a-zA-z0-9 /:]{5,})$',err:'this is invalid error of text area'}},
        ]},
        // {col:[
        //   {class:'col-md-12',data:{fieldtype: 'addmorepayslip',showCol:false,view:'inventory',docDesign:'pop',searchvalue:'salePrice'}},
        // ]},

         ]
        }