import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoRefComponent } from './auto-ref.component';

describe('AutoRefComponent', () => {
  let component: AutoRefComponent;
  let fixture: ComponentFixture<AutoRefComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoRefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoRefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
