import { Component, OnInit } from '@angular/core';
import { sidebarData,navItems } from '../pop-schema';


@Component({
  selector: 'app-pop-main',
  templateUrl: './pop-main.component.html',
  styleUrls: ['./pop-main.component.scss']
})
export class PopMainComponent implements OnInit {
  
  constructor() { 
    Object.assign(this, {sidebarData, navItems})        
  }

  ngOnInit() {}

}
