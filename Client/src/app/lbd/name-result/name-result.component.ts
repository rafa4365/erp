import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';
declare var $;

@Component({
  selector: 'app-name-result',
  templateUrl: './name-result.component.html',
  styleUrls: ['./name-result.component.scss']
})
export class NameResultComponent implements OnInit {
  @Input() public schema: any;
  @Input() public update: any;
  
  @Output() notify = new EventEmitter<any>();

  select2data:any=[];
  result:any;
  firstoption='';

  constructor(private db: PouchbService) { }

  ngOnInit() {
    var obj={
      view:this.schema.view,
      docDesign:this.schema.docDesign
    }
    this.db.fetchData('Showing All',obj).then((r:any)=>{
      for(var i=0;i< r.rows.length;i++){
        this.select2data[i]={
          id:r.rows[i].doc[this.schema.resultvalue],
          text:r.rows[i].doc[this.schema.searchvalue]
        }
      }
      console.log('fetch result ',this.select2data)
      $(".js-data-example").select2({
        placeholder: "Select",
        allowClear: true,
        
        data: this.select2data
      })
    })
    console.log('nameresult', this.schema)
// if(this.update){
//   $(".js-data-example").val('hello').trigger('change');
// }
    

$(".js-data-example").on('select2:select',  (e)=> {
  // Do something
  console.log('select2 value',e.target.value)
  console.log('select2 value',e)
  this.result=e.target.value;

 
  
  var obj={
    [this.schema.searchvalue]:e.params.data.text,
    [this.schema.resultvalue]:e.params.data.id,
  }
  this.notify.emit(obj);
  
});
this.updatevalues(this.update)

  }
  updatevalues(value){
    if(value){
      this.result=value[this.schema.resultvalue]
      this.firstoption=value[this.schema.searchvalue]
  
    }
  }

}
