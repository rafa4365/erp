import { Component, OnInit,Input ,Output, EventEmitter} from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';

declare var $
@Component({
  selector: 'app-table-search',
  templateUrl: './table-search.component.html',
  styleUrls: ['./table-search.component.scss']
})
export class TableSearchComponent implements OnInit {
  searchVar:any;
  dataArray:any=[];
  @Input() public schema:any;

  @Output() returnSearchData= new EventEmitter<any>();
  

  constructor(    private db:PouchbService  ) { }

  ngOnInit() {
    console.log('search input values',this.schema)
    
  }
  search(value){
    
        if(value!=''){
          $('#paginate').pagination('hide')
          $('#pagelimit').hide();
          this.db.search(value,this.schema.form,this.schema.table).then((result:any)=>{
            this.dataArray=result.docs
            console.log('search',this.dataArray)
            this.returnSearchData.emit(this.dataArray)
          })
          
        }else{
          if(this.schema.dpSize!='Showing All'){
            $('#paginate').pagination('show')
          }
          $('#pagelimit').show();
          
          this.dataArray=[]
            this.db.fetchData(this.schema.dpSize,this.schema.table).then((doc)=>{
            for(var i=0;i < doc.rows.length;i++){
              this.dataArray[i]=doc.rows[i].doc;
            };
            this.returnSearchData.emit(this.dataArray)
            
          });
          
        }
      }

}
