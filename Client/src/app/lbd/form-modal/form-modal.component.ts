import { Component, OnInit ,Input,Output,EventEmitter,AfterViewInit} from '@angular/core';
import { ScriptLoaderService } from '../services/script-loader.service';

declare var $
declare var UIkit

@Component({
  selector: 'app-form-modal', 
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss']
})
export class FormModalComponent implements OnInit {
  static currentId = 1;
  // @Output()  updatevalues:EventEmitter<string> = new EventEmitter();
  
  @Input()
  public schema: any;
  
  @Input()
  public static: any;
    
  @Input()
  public rowData: any;

  @Input()
  public rowId: any;

  public formId: any;
  public updateData:any;

  public modalId: string;
  public modalName: string;
  public buttonform:any
  date;
  range;
  color;
  lang;
  // type='Create';

  constructor(    private _script: ScriptLoaderService  ) { 

  }

  ngOnInit() {
    this.static=={type:'Create', buttonClass:'btn',modalTitle:'Create'};
    this.updateData={
      rowData:this.rowData,
      rowId:this.rowId
    }
    // this.type='Create';

    // this.modalId=`modalId${FormModalComponent.currentId++}`
    // this.modalName=`modalName${FormModalComponent.currentId++}`
    // if(this.rowId===undefined){
    //   this.buttonform=`${this.static.type}form`      
    // }else{
    // this.buttonform=`${this.static.type}form${this.rowId}`
    // }
    // console.log('update data formmodal',this.updateData)
  }

data(){
  this.updateData={
    rowData:this.rowData,
    rowId:this.rowId
  }

  // this.updatevalues.emit(this.updateData);
  // console.log("row data", this.rowData)
  // console.log("row data", this.rowId)

  // var updateProperties=new Object;
  // for(var i =0;i<this.experiment.form.length;i++){
  //   updateProperties[this.experiment.form[i].name]=this.experiment.form[i].property[0]=values[this.experiment.form[i].name];
  // };
  // updateProperties['_id']=values['_id'];
  // updateProperties['_rev']=values['_rev'];
  // this.updategroup= this.fb.group(updateProperties);
  
}
// open(){
//   UIkit.modal('#modal-sections', {esc_close: true, bg_close: true, keyboard:true, stack:true}).show();
// }
ngAfterViewInit(){
  // $(".dropdown-menu").removeClass("open");
  this._script.load('app-form-modal',
  '../../assets/js/bootstrap-checkbox-radio-switch-tags.js',
  '../../assets/js/light-bootstrap-dashboard.js' )
  
}

}
