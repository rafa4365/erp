import { Component, OnInit ,Input,Output,EventEmitter,AfterViewInit} from '@angular/core';
declare var $
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  static currentId = 1;
  
  @Input()
  public schema: any;
  
  @Input()
  public static: any;
    
  @Input()
  public formId: any;
  
  @Output()  updatevalues:EventEmitter<string> = new EventEmitter();
  
  public modalId: string;
  public modalName: string;
  public buttonform:any
  constructor() { } 

  ngOnInit() {
    this.modalId=`modalId${ModalComponent.currentId++}`
    this.modalName=`modalName${ModalComponent.currentId++}`
    if(this.formId===undefined){
      this.buttonform=`${this.static.type}form`      
    }else{
    this.buttonform=`${this.static.type}form${this.formId}`
    }
  }
data(){
  this.updatevalues.emit('completed');
}
ngAfterViewInit(){
  $(".dropdown-menu").removeClass("open");
  
}
}
