import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmorestaticComponent } from './addmorestatic.component';

describe('AddmorestaticComponent', () => {
  let component: AddmorestaticComponent;
  let fixture: ComponentFixture<AddmorestaticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmorestaticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmorestaticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
