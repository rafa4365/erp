//core
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
//charts 
import { NgxChartsModule } from '@swimlane/ngx-charts';
//validation
import { CustomFormsModule } from 'ng2-validation'
//bootstraping
import { AppComponent } from './app.component';
//lbd
import { FixedPluginComponent } from './lbd/fixed-plugin/fixed-plugin.component';
import { SidebarComponent } from './lbd/sidebar/sidebar.component';
import { NavbarComponent } from './lbd/navbar/navbar.component';
import { NotificationComponent } from './lbd/notification/notification.component';
import { SweetAlertsComponent } from './lbd/sweet-alerts/sweet-alerts.component';
import { InputComponent } from './lbd/input/input.component';
import { PaginationComponent } from './lbd/pagination/pagination.component';
import { CollapseComponent } from './lbd/collapse/collapse.component';
import { TabsComponent } from './lbd/tabs/tabs.component';
import { FooterComponent } from './lbd/footer/footer.component';
//pages
import { NotificationsComponent } from './pages/components/notifications/notifications.component';
import { PanelsComponent } from './pages/components/panels/panels.component';

import { CalenderComponent } from './pages/calender/calender.component';
import { LoginPageComponent } from './pages/pages/login-page/login-page.component';
import { RegisterPageComponent } from './pages/pages/register-page/register-page.component';
import { LockScreenPageComponent } from './pages/pages/lock-screen-page/lock-screen-page.component';
import { UserPageComponent } from './pages/pages/user-page/user-page.component';
import { SweetAlertComponent } from './pages/sweet-alert/sweet-alert.component';
import { HolderComponent } from './pages/holder/holder.component';

import { NavbarTilteService } from './lbd/services/navbar-tilte.service';
import { NavbarItemsComponent } from './lbd/navbar-items/navbar-items.component';
import { SidebarItemsComponent } from './lbd/sidebar-items/sidebar-items.component';
import { LoadingPageComponent } from './pages/loading-page/loading-page.component';
import { NotificationService } from './lbd/services/notification.service';
import { SweetAlertsService } from './lbd/services/sweet-alerts.service';
import { SubcategoriesComponent } from './lbd/subcategories/subcategories.component';
import { CalenderccComponent } from './lbd/calendercc/calendercc.component';
import { ModalComponent } from './lbd/modal/modal.component';
import { CTableComponent } from './lbd/c-table/c-table.component';
import { VisibleRowComponent } from './lbd/visible-row/visible-row.component';

import { SearchBarComponent } from './lbd/search-bar/search-bar.component';
import { ScriptLoaderService } from './lbd/services/script-loader.service';
import { FormComponent } from './lbd/form/form.component';
import { GetdataService } from './lbd/services/getdata.service';
import { ProgressBarComponent } from './lbd/progress-bar/progress-bar.component';
import { SliderComponent } from './lbd/slider/slider.component';
import { TagsComponent } from './lbd/tags/tags.component';
import { CSelectComponent } from './lbd/c-select/c-select.component';
import { SwitchComponent } from './lbd/switch/switch.component';
import { DatepickerComponent } from './lbd/datepicker/datepicker.component';

import { PouchbService } from './lbd/services/pouchb.service';

import { TodoComponent } from './pages/widgets/todo/todo.component';
import { TodoService } from './lbd/services/todo.service';
import { DebugService } from './lbd/services/debug.service';
import { DataRangePickerComponent } from './lbd/data-range-picker/data-range-picker.component';
import { Select2Component } from './lbd/select2/select2.component';
import { TypeaheadComponent } from './lbd/typeahead/typeahead.component';
import { DateRangPickerLbdComponent } from './lbd/date-rang-picker-lbd/date-rang-picker-lbd.component';
import { RangeSliderComponent } from './lbd/range-slider/range-slider.component';
import { DynFieldComponent } from './lbd/dyn-field/dyn-field.component';
import { TableSearchComponent } from './lbd/table-search/table-search.component';
import { FormModalComponent } from './lbd/form-modal/form-modal.component';
import { TFormComponent } from './lbd/t-form/t-form.component';
import { TemplateFormComponent } from './pages/forms/template-form/template-form.component';
import { T2fschemaService } from './lbd/services/t2fschema.service';
import { AddmoreComponent } from './lbd/addmore/addmore.component';

//----------HR components--------------------------
import { EmployeeComponent } from './hr/employee/employee.component';
import { PaySlipComponent } from './hr/pay-slip/pay-slip.component';
import { HRReportsComponent } from './hr/hr-reports/hr-reports.component';
import { HRSummaryComponent } from './hr/hr-summary/hr-summary.component';
import { HRMainComponent } from './hr/hr-main/hr-main.component';

//----------POP  components--------------------------
import { PopMainComponent } from './pop/pop-main/pop-main.component';
import { SupplierComponent } from './pop/supplier/supplier.component';
import { ExpenseCategoryComponent } from './pop/expense-category/expense-category.component';
import { ExpenseComponent } from './pop/expense/expense.component';
import { PurchaseInvoiceComponent } from './pop/purchase-invoice/purchase-invoice.component';
import { PurchaseOrderComponent } from './pop/purchase-order/purchase-order.component';
import { PopInventoryComponent } from './pop/pop-inventory/pop-inventory.component';
import { PopReportsComponent } from './pop/pop-reports/pop-reports.component';
import { PopSummaryComponent } from './pop/pop-summary/pop-summary.component';

//-----------POS components
import { PosMainComponent } from './pos/pos-main/pos-main.component';
import { CustomerComponent } from './pos/customer/customer.component';
import { DeliveryNotesComponent } from './pos/delivery-notes/delivery-notes.component';
// import { PosInventoryComponent } from './pos/pos-inventory/pos-inventory.component';
import { SaleInvoiceComponent } from './pos/sale-invoice/sale-invoice.component';
import { SaleQuoteComponent } from './pos/sale-quote/sale-quote.component';
import { SaleOrderComponent } from './pos/sale-order/sale-order.component';
import { PosReportsComponent } from './pos/pos-reports/pos-reports.component';
import { PosSummaryComponent } from './pos/pos-summary/pos-summary.component';

//-------------Admin Components------------------
import { AdminMainComponent } from './admin/admin-main/admin-main.component';
import { AdminSettingComponent } from './admin/admin-setting/admin-setting.component';
import { AdminPermissionComponent } from './admin/admin-permission/admin-permission.component';
import { AdminReportsComponent } from './admin/admin-reports/admin-reports.component';
import { AdminSummaryComponent } from './admin/admin-summary/admin-summary.component';
import { AddmorestaticComponent } from './lbd/addmorestatic/addmorestatic.component';

import { AuthLoginService } from './lbd/services/auth-login.service';
import { AuthLoginGuard } from './lbd/guard/auth-login.guard';
import { TodoSummaryComponent } from './pages/widgets/todo-summary/todo-summary.component';

import { ValidationService } from './lbd/services/validation.service';
import { SearchselectComponent } from './lbd/searchselect/searchselect.component';
import { NameResultComponent } from './lbd/name-result/name-result.component';
import { ProfilepageComponent } from './pages/profilepage/profilepage.component';
import { ValidFormComponent } from './pages/forms/valid-form/valid-form.component';
import { UsersComponent } from './pages/widgets/users/users.component';
import { ValidTableComponent } from './lbd/valid-table/valid-table.component';
import { ImageUploaderComponent } from './lbd/image-uploader/image-uploader.component';
import { EqChartComponent } from './pages/widgets/eq-chart/eq-chart.component';
import { AddmoreSaleComponent } from './lbd/addmore-sale/addmore-sale.component';
import { AddmoreExpenseComponent } from './lbd/addmore-expense/addmore-expense.component';
import { AddmorePayslipComponent } from './lbd/addmore-payslip/addmore-payslip.component';
import { DynFieldService } from './lbd/services/dyn-field.service';
import { AddmoreValidComponent } from './lbd/addmore-valid/addmore-valid.component';
import { AddmoreExpenseValidComponent } from './lbd/addmore-expense-valid/addmore-expense-valid.component';
import { FormValidComponent } from './lbd/form-valid/form-valid.component';
import { ViewComponent } from './lbd/view/view.component';
import { FormSimpleComponent } from './lbd/form-simple/form-simple.component';
import { ServerSettingsComponent } from './pagespages/server-settings/server-settings.component';
import { ServerReplicationService } from './lbd/services/server-replication.service';
import { AutoRefComponent } from './lbd/auto-ref/auto-ref.component';
import { RefnumComponent } from './lbd/refnum/refnum.component';

const routes: Routes = [

  { path: '', redirectTo: '/login', pathMatch: 'full' },

  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegisterPageComponent },
  { path: 'lockscreen', component: LockScreenPageComponent },
  {
    path: 'holder',
    component: HolderComponent,
    canActivate: [AuthLoginGuard],
    children: [
      { path: '', redirectTo: '/holder/calender', pathMatch: 'full' },

      { path: 'noti', component: NotificationsComponent },
      { path: 'sweetalerts', component: SweetAlertComponent },
      { path: 'panels', component: PanelsComponent },
      { path: 'calender', component: CalenderComponent },
      { path: 'userpage', component: UserPageComponent },
      { path: 'todo', component: TodoComponent },
      { path: 'todosum', component: TodoSummaryComponent },
      { path: 'temform', component: TemplateFormComponent },
      { path: 'formmodal', component: FormModalComponent },
      { path: 'profile', component: ProfilepageComponent },
      { path: 'userinfo', component: UsersComponent },
      { path: 'validform', component: FormValidComponent },
      { path: 'view', component: ViewComponent },
      { path: 'eqchart', component: EqChartComponent },
    ]
  },

  {
    path: 'hrMain',
    component: HRMainComponent,
    canActivate: [AuthLoginGuard],
    children: [
      { path: '', redirectTo: '/hrMain/employee', pathMatch: 'full' },
      { path: 'validform', component: FormValidComponent },

      { path: 'employee', component: EmployeeComponent },
      { path: 'paySlip', component: PaySlipComponent },
      { path: 'hrReports', component: HRReportsComponent },
      { path: 'hrSummary', component: HRSummaryComponent },
      { path: 'temform', component: TemplateFormComponent },
      { path: 'profile', component: ProfilepageComponent },
      { path: 'view', component: ViewComponent },


    ]
  },
  {
    path: 'popMain',
    component: PopMainComponent,
    canActivate: [AuthLoginGuard],
    children: [
      { path: '', redirectTo: '/popMain/supplier', pathMatch: 'full' },
      { path: 'supplier', component: SupplierComponent },
      { path: 'expenseCategory', component: ExpenseCategoryComponent },
      { path: 'expense', component: ExpenseComponent },
      { path: 'purchaseInvoice', component: PurchaseInvoiceComponent },
      { path: 'purchaseOrder', component: PurchaseOrderComponent },
      { path: 'popInventory', component: PopInventoryComponent },
      { path: 'popReports', component: PopReportsComponent },
      { path: 'popSummary', component: PopSummaryComponent },
      { path: 'profile', component: ProfilepageComponent },
      { path: 'validform', component: FormValidComponent },
      { path: 'view', component: ViewComponent },

      { path: 'temform', component: TemplateFormComponent },

    ]
  },

  {
    path: 'posMain',
    component: PosMainComponent,
    canActivate: [AuthLoginGuard],
    children: [
      { path: '', redirectTo: '/posMain/customer', pathMatch: 'full' },
      { path: 'customer', component: CustomerComponent },
      { path: 'deliveryNote', component: DeliveryNotesComponent },
      // { path: 'posInventory', component: PosInventoryComponent },
      { path: 'saleInvoice', component: SaleInvoiceComponent },
      { path: 'saleOrder', component: SaleOrderComponent },
      { path: 'saleQuote', component: SaleQuoteComponent },
      { path: 'posReports', component: PosReportsComponent },
      { path: 'posSummary', component: PosSummaryComponent },
      { path: 'profile', component: ProfilepageComponent },
      { path: 'validform', component: FormValidComponent },
      { path: 'view', component: ViewComponent },

      { path: 'temform', component: TemplateFormComponent },

    ]
  },

  {
    path: 'adminMain',
    component: AdminMainComponent,
    canActivate: [AuthLoginGuard],
    children: [
      { path: '', redirectTo: '/adminMain/adminSummary', pathMatch: 'full' },
      { path: 'adminSetting', component: AdminSettingComponent },
      { path: 'adminPermission', component: AdminPermissionComponent },
      { path: 'adminReports', component: AdminReportsComponent },
      { path: 'adminSummary', component: AdminSummaryComponent },
      //-----------hr ----
      { path: 'employee', component: EmployeeComponent },
      { path: 'paySlip', component: PaySlipComponent },
      { path: 'hrReports', component: HRReportsComponent },
      { path: 'hrSummary', component: HRSummaryComponent },
      //---------POP----------
      { path: 'supplier', component: SupplierComponent },
      { path: 'expenseCategory', component: ExpenseCategoryComponent },
      { path: 'expense', component: ExpenseComponent },
      { path: 'purchaseInvoice', component: PurchaseInvoiceComponent },
      { path: 'purchaseOrder', component: PurchaseOrderComponent },
      { path: 'popInventory', component: PopInventoryComponent },
      { path: 'popReports', component: PopReportsComponent },
      { path: 'popSummary', component: PopSummaryComponent },

      //---------POS----------
      { path: 'customer', component: CustomerComponent },
      { path: 'deliveryNote', component: DeliveryNotesComponent },
      // { path: 'posInventory', component: PosInventoryComponent },
      { path: 'saleInvoice', component: SaleInvoiceComponent },
      { path: 'saleOrder', component: SaleOrderComponent },
      { path: 'saleQuote', component: SaleQuoteComponent },
      { path: 'posReports', component: PosReportsComponent },
      { path: 'posSummary', component: PosSummaryComponent },
      { path: 'profile', component: ProfilepageComponent },
      { path: 'validform', component: FormValidComponent },
      { path: 'view', component: ViewComponent },
      { path: 'serverSetting', component: ServerSettingsComponent },
      
      { path: 'temform', component: TemplateFormComponent },


    ]
  },
];

@NgModule({
  declarations: [

    //bootstraping
    AppComponent,
    //lbd
    FixedPluginComponent,
    SidebarComponent,
    NavbarComponent,
    NotificationComponent,
    SweetAlertsComponent,
    InputComponent,
    PaginationComponent,
    CollapseComponent,
    TabsComponent,
    FooterComponent,
    //pages
    NotificationsComponent,
    PanelsComponent,

    CalenderComponent,
    LoginPageComponent,
    RegisterPageComponent,
    LockScreenPageComponent,
    UserPageComponent,
    SweetAlertComponent,
    HolderComponent,
    NavbarItemsComponent,
    SidebarItemsComponent,
    LoadingPageComponent,
    SubcategoriesComponent,
    CalenderccComponent,
    ModalComponent,
    CTableComponent,
    VisibleRowComponent,
    SearchBarComponent,
    FormComponent,
    ProgressBarComponent,
    SliderComponent,
    TagsComponent,
    CSelectComponent,
    SwitchComponent,
    DatepickerComponent,

    TodoComponent,
    DataRangePickerComponent,
    Select2Component,
    TypeaheadComponent,
    DateRangPickerLbdComponent,
    RangeSliderComponent,
    DynFieldComponent,
    TableSearchComponent,
    FormModalComponent,
    TFormComponent,
    TemplateFormComponent,
    AddmoreComponent,
    //-----HR component---------
    EmployeeComponent,
    PaySlipComponent,
    HRReportsComponent,
    HRSummaryComponent,
    HRMainComponent,
    //-----POP component---------
    SupplierComponent,
    ExpenseCategoryComponent,
    ExpenseComponent,
    PurchaseInvoiceComponent,
    PurchaseOrderComponent,
    PopInventoryComponent,
    PopReportsComponent,
    PopSummaryComponent,
    PopMainComponent,
    //-----POS component---------
    PosMainComponent,
    CustomerComponent,
    DeliveryNotesComponent,
    // PosInventoryComponent,
    SaleInvoiceComponent,
    SaleQuoteComponent,
    SaleOrderComponent,
    PosReportsComponent,
    PosSummaryComponent,
    //-----Admin component---------
    AdminMainComponent,
    AdminSettingComponent,
    AdminPermissionComponent,
    AdminReportsComponent,
    AdminSummaryComponent,
    AddmorestaticComponent,
    TodoSummaryComponent,
    SearchselectComponent,
    NameResultComponent,
    ProfilepageComponent,
    ValidFormComponent,
    UsersComponent,
    ValidTableComponent,
    ImageUploaderComponent,
    EqChartComponent,
    AddmoreSaleComponent,
    AddmoreExpenseComponent,
    AddmorePayslipComponent,
    AddmoreValidComponent,
    AddmoreExpenseValidComponent,
    FormValidComponent,
    ViewComponent,
    FormSimpleComponent,
    ServerSettingsComponent,
    AutoRefComponent,
    RefnumComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CustomFormsModule,
    NgxChartsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    //routing
    [RouterModule.forRoot(routes)],

  ],
  providers: [NavbarTilteService, NotificationService, SweetAlertsService, ScriptLoaderService, GetdataService, PouchbService, TodoService, DebugService, T2fschemaService, AuthLoginService, AuthLoginGuard, ValidationService, DynFieldService, ServerReplicationService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
