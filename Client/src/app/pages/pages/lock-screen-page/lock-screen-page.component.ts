import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lock-screen-page',
  templateUrl: './lock-screen-page.component.html',
  styleUrls: ['./lock-screen-page.component.scss']
})
export class LockScreenPageComponent implements OnInit {

  fixedPluginData
  constructor() { }
  
    ngOnInit() {
      this.fixedPluginData={
        filters:[
          {class:'',color:'black'},
          {class:'badge-azure',color:'azure'},
          {class:'badge-green',color:'green'},
          {class:'badge-orange',color:'orange'},
          {class:'badge-red',color:'red'},
          {class:'badge-purple',color:'purple'},
          {class:'',color:'black'},
          {class:'badge-azure',color:'azure'},
          {class:'badge-green',color:'green'},
          {class:'badge-orange',color:'orange'},
          {class:'badge-red',color:'red'},
          {class:'badge-purple',color:'purple'}
        ],
        bgimg:[
          {url:'assets/images/full-screen-image-1.jpg'},
          {url:'assets/images/full-screen-image-2.jpg'},
          {url:'assets/images/full-screen-image-3.jpg'},
          {url:'assets/images/full-screen-image-4.jpg'},
          {url:'assets/images/full-screen-image-1.jpg'},
          {url:'assets/images/full-screen-image-2.jpg'},
          {url:'assets/images/full-screen-image-3.jpg'},
          {url:'assets/images/full-screen-image-4.jpg'},
        ]
      }
    }

}
