import { Component, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
// import { emit } from 'cluster';
declare var $;
declare var moment;

@Component({
  selector: 'app-date-rang-picker-lbd',
  templateUrl: './date-rang-picker-lbd.component.html',
  styleUrls: ['./date-rang-picker-lbd.component.scss']
})
export class DateRangPickerLbdComponent implements OnInit {

  @Output() notify = new EventEmitter<any>();

  selectorDropDown = false;
  datetime;
  static currentId = 1;
  public dpId;
  constructor() { }

  ngOnInit() {
    this.dpId = `datarangepicker${DateRangPickerLbdComponent.currentId++}`
    // this.dpId=`demo`

    this.datetime = [
      {
        class: 'datetimepicker1', id: 'start', placeholder: 'Datetime Picker Here', options: {
          icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
          },
          inline: true,
          maxDate: moment(),
          format:'DD/MM/YYYY hh:mm A'
        }
      },
      {
        class: 'datetimepicker2', id: 'end', placeholder: 'Datetime Picker Here', options: {
          icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
          },
          inline: true,
          maxDate: moment(),
          format:'DD/MM/YYYY hh:mm A'
          
        }
      }

    ]
  }
  ngAfterViewInit() {
    // $('#demo').daterangepicker(this.datetime, function(start, end, label) {
    //   console.log(`New lbd date range selected:  ${start.format('YYYY-MM-DD')}  to  ${end.format('YYYY-MM-DD')}   (predefined range:   ${label})`);
    // });
    for (var i = 0; i < this.datetime.length; i++) {
      $(`.${this.datetime[i].class}`).datetimepicker(this.datetime[i].options)
    }
    $(".datetimepicker").prop("disabled", true);

  }
  selector(value) {
    console.log('%c date picker clicked ....!', 'color: #9c27b0; font-weight:bold')
    this.selectorDropDown = !value;
    console.log('%c selectorDropDown ....!', 'color: #9c27b0; font-weight:bold', value)
  }
  getvalues(value) {

    switch (value) {
      case 'today':
        console.log('today', moment().format('DD/MM/YYYY hh:mm A'));
        var today = moment().format('DD/MM/YYYY hh:mm A');
        $("#dateRange").val(today);
        var obj_today = {
          start: moment().format('DD/MM/YYYY hh:mm A'),
          end: moment().format('DD/MM/YYYY hh:mm A')

        }
        this.notify.emit(obj_today);


        break;
      case 'yesterday':
        console.log('yesterday', moment().subtract(1, 'day').format('DD/MM/YYYY hh:mm A'));
        var yesterday = moment().subtract(1, 'day').format('DD/MM/YYYY hh:mm A');
        $("#dateRange").val(yesterday);
        var obj_yesterday = {
          start: moment().subtract(1, 'day').format('DD/MM/YYYY hh:mm A'),
          end: moment().subtract(1, 'day').format('DD/MM/YYYY hh:mm A')
        }
        this.notify.emit(obj_yesterday);


        break;
      case 'last7':
        console.log('last 7 days', moment().subtract(7, 'day').format('DD/MM/YYYY hh:mm A'))
        var last7 = `last 7 days from ${moment().subtract(7, 'day').format('DD/MM/YYYY hh:mm A')} till ${moment().format('DD/MM/YYYY hh:mm A')}`
        $("#dateRange").val(last7)
        var obj_last7 = {
          start: moment().subtract(7, 'day').format('DD/MM/YYYY hh:mm A'),
          end: moment().format('DD/MM/YYYY hh:mm A')
        }
        this.notify.emit(obj_last7);

        break;
      case 'thismonth':
        console.log('this month', moment().format('DD/MM/YYYY hh:mm A'))
        var thismonth = moment().startOf('month').fromNow();
        // thismonth.format('M')
        $("#dateRange").val(`This Month ${thismonth}`)

        break;
      case 'lastmonth':
        console.log('last month', moment().format('DD/MM/YYYY hh:mm A'))
        $("#dateRange").val('Last Month')

        break;
      case 'custom':
        console.log('custom')
        console.log('values form date rang picker .. start', $('#start').get(0).value)
        console.log('values form date rang picker .. end', $('#end').get(0).value)
        // $("#dateRange").format('DD/MM/YYYY hh:mm A')
        $("#dateRange").val($('#start').get(0).value + ' To ' + $('#end').get(0).value)

        var obj_customDate = {
          start: $('#start').get(0).value,
          end: $('#end').get(0).value
        }
        this.notify.emit(obj_customDate)
        break;
    }
  }



}
