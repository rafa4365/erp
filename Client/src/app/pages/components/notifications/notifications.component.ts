import { Component, OnInit } from '@angular/core';
import {NotificationService, NotificationType, NotificationOptions}  from '../../../lbd/services/notification.service';
import { NavbarTilteService } from '../../../lbd/services/navbar-tilte.service'; 

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  
  public modalData;

  constructor(private navbarTitleService: NavbarTilteService, private notificationService: NotificationService) { }
  
  public ngOnInit() {
      this.navbarTitleService.updateTitle('Notifications');
      
      this.modalData={   
        list:[
          {buttonColor:'info',butonText:'modal',modalTitle:'Modal title', id:'id1',data:'hello this is modal body !!!!'},
          {buttonColor:'success',butonText:'modal',modalTitle:'Modal title', id:'id2',data:'hello this is modal body !!!!'},
          {buttonColor:'warning',butonText:'modal',modalTitle:'Modal title', id:'id3',data:'hello this is modal body !!!!'},
          {buttonColor:'danger',butonText:'modal',modalTitle:'Modal title', id:'id4',data:'hello this is modal body !!!!'},
          {buttonColor:'',butonText:'modal',modalTitle:'Modal title', id:'id8',data:'hello this is modal body !!!!'},
        ]        
      }
    }

  
    public showNotification(from: string, align: string,type: NotificationType) {
      // const type = Math.floor((Math.random() * 4) + 1);
  
      this.notificationService.notify(new NotificationOptions({
        message: 'Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer.',
        icon: 'pe-7s-gift',
        type: <NotificationType>(type),
        from: from,
        align: align
      }));
    }
}
