import { Component, OnInit,AfterViewInit } from '@angular/core';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';

declare var $;
declare var demo;
@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.scss']
})
export class CalenderComponent implements OnInit {
events;
today;
y;
m;
d;
  constructor(private navbarTitleService: NavbarTilteService) { }
  
    ngOnInit() {
      this.navbarTitleService.updateTitle('Calender');
      this.today = new Date();
      
                this.y = this.today.getFullYear();
                this.m = this.today.getMonth();
                this.d = this.today.getDate();
      this.events=[{
        title: 'All Day Event',
        start: new Date(this.y, this.m, 1)
    },
    {
        id: 999,
        title: 'Repeating Event',
        start: new Date(this.y, this.m, this.d - 4, 6, 0),
        allDay: false,
        className: 'event-blue'
    },
    {
        id: 999,
        title: 'Repeating Event',
        start: new Date(this.y, this.m, this.d + 3, 6, 0),
        allDay: false,
        className: 'event-blue'
    },
    {
        title: 'Meeting',
        start: new Date(this.y, this.m, this.d - 1, 10, 30),
        allDay: false,
        className: 'event-green'
    },
    {
        title: 'Lunch',
        start: new Date(this.y, this.m, this.d + 7, 12, 0),
        end: new Date(this.y, this.m, this.d + 7, 14, 0),
        allDay: false,
        className: 'event-red'
    },
    {
        title: 'LBD Launch',
        start: new Date(this.y, this.m, this.d - 2, 12, 0),
        allDay: true,
        className: 'event-azure'
    },
    {
        title: 'Birthday Party',
        start: new Date(this.y, this.m, this.d + 1, 19, 0),
        end: new Date(this.y, this.m, this.d + 1, 22, 30),
        allDay: false,
    },
    {
        title: 'Click for Creative Tim',
        start: new Date(this.y, this.m ,21),
        end: new Date(this.y, this.m ,22),
        url: 'http://www.creative-tim.com/',
        className: 'event-orange'
    },
    {
        title: 'Click for Google',
        start: new Date(this.y, this.m ,23),
        end: new Date(this.y, this.m ,23),
        url: 'http://www.creative-tim.com/',
        className: 'event-orange'
    }
]
    }

  ngAfterViewInit(){
  //   $().ready(function(){
  //     demo.initFullCalendar();
  // });
  }
}
