import { TestBed, inject } from '@angular/core/testing';

import { NavbarTilteService } from './navbar-tilte.service';

describe('NavbarTilteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavbarTilteService]
    });
  });

  it('should be created', inject([NavbarTilteService], (service: NavbarTilteService) => {
    expect(service).toBeTruthy();
  }));
});
