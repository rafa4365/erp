import { Injectable } from '@angular/core';
import { BehaviorSubject }    from 'rxjs/BehaviorSubject';

@Injectable()
export class T2fschemaService {

private myvalue = new BehaviorSubject<any>("default message") ;
public myvalue$ = this.myvalue.asObservable();

private uvalue = new BehaviorSubject<any>("default message") ;
public uvalue$ = this.uvalue.asObservable();

  constructor() { }
  setvalue(val){
   this.myvalue.next(val);
  // console.log('service val',val);
  // console.log('service myvalue',this.myvalue);
  }
  updateValue(val){
    this.uvalue.next(val);
    
  }

}
