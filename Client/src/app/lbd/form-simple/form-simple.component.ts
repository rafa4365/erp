// import { T2fschemaService } from '../../lbd/services/t2fschema.service';
import { Component, OnInit, Input,AfterViewInit, OnDestroy, ChangeDetectorRef , trigger, state, style, transition, animate} from '@angular/core';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';
import { Subscription } from 'rxjs/Subscription';
import { PouchbService } from '../../lbd/services/pouchb.service';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { ValidationService } from '../../lbd/services/validation.service';
import {NotificationService, NotificationType, NotificationOptions}  from '../../lbd/services/notification.service';

@Component({ 
  selector: 'app-form-simple',
  templateUrl: './form-simple.component.html',
  styleUrls: ['./form-simple.component.scss'],
  animations: [
    trigger('carduserprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out'),
      ])
    ]),
    trigger('cardprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1})),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0.25s ease-out')
      ])
    ])
  ]

})
export class FormSimpleComponent implements OnInit {
  @Input() form: any;
  eq_data: any;
  data: any;
  subscription1: Subscription;
  subscription2: Subscription;
  updateValues: any;
  C_udata: any = []

  constructor(
    // private t2f: T2fschemaService,
    private navbarTitleService: NavbarTilteService,
    private db: PouchbService,
    private cdr: ChangeDetectorRef,
    private _location: Location,
    private _valid: ValidationService,
    private notificationService: NotificationService,
    private router: Router) {
    // this.subscription1 = this.t2f.myvalue$.subscribe(val => this.form = val);
    // this.subscription2 = this.t2f.uvalue$.subscribe(val => this.updateValues = val);

    // if (this.form === 'default message') {
    //   this._location.back();
    // }
  }
 
  ngOnInit() {
    this.db.createView(this.form.table,this.form.numrow[0]).then((doc)=>{
      this.updateValues=doc.rows[0].doc;
      if(this.updateValues){
        this.form.type='update'
        this.updateValues['_id']=doc.rows[0].doc['_id']
        this.updateValues['_rev']=doc.rows[0].doc['_rev']
        this.updateValues['view']=doc.rows[0].doc['view']
      }

        for(var i=0;i < this.form.rows.length;i++){
          for(var j=0;j < this.form.rows[i].col.length;j++){
          this.form.rows[i].col[j].data.value=this.updateValues[this.form.rows[i].col[j].data.name]=doc.rows[0].doc[this.form.rows[i].col[j].data.name]
      };
    };
    console.log('dataArupdateValuesray ',this.updateValues)    
    })
    
    this.navbarTitleService.updateTitle('');
    this.form.submit = true;
    // this.updateval(this.form.rows);
    console.log('form init this form', this.form);
  }
  Create() {
    this._valid.formdata().then((v) => {
      console.log('create form submitted v', v); 
      this.data = v;
      this.data['_id'] = this.form.table.view;
      this.data['view'] = this.form.table.view;

      if (this.data.inventory) {
        console.log("form valid data If condition check", this.data.inventory.total)

        this.data['total'] = this.data.inventory.total;
      }

      if (this.data.expense) {
        console.log("form valid data If condition check", this.data.expense.total)

        this.data['expenseTotal'] = this.data.expense.total;
      }


        this.db.query(this.data);
      this.showNotification('top','center',2,`Form ${this.form.title} is ${this.form.type}`,'pe-7s-check')
      this._location.back();
    })
  }
  Update() {
    this._valid.formdata().then((v) => { 
      console.log('update form submitted v', v);
      console.log('update form submitted this.updateValues', this.updateValues);
      this.data = v;
      this.data['_id'] = this.updateValues._id;
      this.data['_rev'] = this.updateValues._rev;
      this.data['view'] = this.updateValues.view;
      console.log('update form submitted this.data', this.data);
      
      this.db.query(this.data);
      this.showNotification('top','center',2,`Form ${this.form.title} is ${this.form.type}`,'pe-7s-check')
      
      this._location.back();
    })
  }
  back() { this._location.back(); }

  ngDoCheck() {
    this._valid.get(this.form)
  }
  updateval(value) {
    if (this.updateValues !== '') {
      console.log("updateValues", this.updateValues)
      var i = 0
      value.forEach((element) => {
        element.col.forEach((element) => {
          element.data.value = this.updateValues[element.data.name]
          element.data.index = i++
          this.C_udata.push(element)
        })
      })
    }
    if (this.updateValues === '') {
      console.log("updateValues", this.updateValues)
      var i = 0
      value.forEach((element) => {
        element.col.forEach((element) => {
          if (element.data.fieldtype !== 'datepicker') {
            element.data.value = '';
            element.data.class = 'form-control';
          }
          if (element.data.fieldtype === 'datepicker') {
            element.data.class = 'datetimepicker';
            element.data.value = '';
          }
        })
      })
    }
  }

  // ngOnDestroy() {
  //   this.subscription1.unsubscribe(); this.subscription2.unsubscribe();
  // }
  public showNotification(from: string, align: string,type: NotificationType,message:any,icon:any) {
    this.notificationService.notify(new NotificationOptions({
      message: message,
      icon: icon,
      type: <NotificationType>(type),
      from: from,
      align: align
    }));
  }
}