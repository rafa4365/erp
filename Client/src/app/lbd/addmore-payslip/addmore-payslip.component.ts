import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
declare var $;


@Component({
  selector: 'app-addmore-payslip',
  templateUrl: './addmore-payslip.component.html',
  styleUrls: ['./addmore-payslip.component.scss']
})
export class AddmorePayslipComponent implements OnInit {
  @Input() schema;
  @Output() notify = new EventEmitter<any>();
  @Input() public update: any;

  earningobj = {
    name: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    discp: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    qty: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    rate: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    amount: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    type:'earningobjbtn'
  }
  earningobjbtn=true;
  deductionobj = {
    name: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    discp: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    amount: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    }    ,
    type:'deductionobj'
  }
  deductionobjbtn=true;
  contributionobj = {
    name: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    discp: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    amount: {
      class: 'form-control',
      valid: null,
      value: '',regex:/^([a-z A-Z.]{2,})$/,
    },
    type:'contributionobj'
  }
  contributionobjbtn=true

  earningarray: any = [];
  contributionarray: any = [];
  deductionarray: any = [];
  earningtotal = 0;

  constructor() { }

  ngOnInit() {
    // console.log('addmore valid schema', this.schema);
    // (<HTMLInputElement>document.getElementById("more")).disabled = true;

  }
  earningadd() {
    console.log("earning object", this.earningobj)
    this.earningarray.push(this.earningobj)
    console.log("earningarray array", this.earningarray)
    // this.earningobj.amount.value='';
    // this.earningobj.discp.value='';
    // this.earningobj.name.value='';
    // this.earningobj.qty.value='';
    // this.earningobj.rate.value='';
  }
  deductionadd() {
    console.log("earning object", this.deductionobj)
    this.deductionarray.push(this.deductionobj)
    console.log("deductionarray array", this.deductionarray)
    // this.deductionobj.amount.value='';
    // this.deductionobj.discp.value='';
    // this.deductionobj.name.value='';
  }
  contributionadd() {
    console.log("earning object", this.contributionobj)
    this.contributionarray.push(this.contributionobj)
    console.log("contributionarray array", this.contributionarray)
    this.check(this.contributionobj.amount,this.contributionobjbtn)
    this.check(this.contributionobj.discp,this.contributionobjbtn)
    this.check(this.contributionobj.name,this.contributionobjbtn)
    // this.contributionobj.amount.value='';
    // this.contributionobj.name.value='';
    // this.contributionobj.discp.value='';
  }
  earningremove(i) {
    this.earningarray.splice(i,1)
    
  }
  deductionremove(i) {
    this.deductionarray.splice(i,1)
    
  }
  eontributioremove(i) {
    this.contributionarray.splice(i,1)
    
  }
  math() {

  }
  totalFun() {

  }
  change(value,btn) {
    this.math();
    this.check(value,btn);
  }
  check(value,btn) {
    // var _name = new RegExp(/^([a-z A-Z.]{2,})$/);
    // var dis_exp = new RegExp(/^([a-z A-Z.]{1,})$/);
    // var qty_exp = new RegExp(/^([0-9]{1,})$/);
    // var discount_exp = new RegExp(/^([0-9]{1,})$/);
    // // var exp=[]
    this.validation(value,btn);
    // this.validation(this.obj.discp, dis_exp, this.dis);
    // this.validation(this.obj.qty, qty_exp, this.qty);
    // this.validation(this.obj.discount, discount_exp, this.discount);
  }
  validation(value,btn) {
    var regex=new RegExp(value.regex)
    if (regex.test(value.value) && value.value !== '') {
      value.class = 'form-control valid';
      value.valid = true;
      // this.enablebutton(count)
      // btn = false;
      
    }
    if (!regex.test(value.value) && value.value !== '') {
      value.class = 'form-control invalid';
      if(btn==='deductionobjbtn'){
        this.earningobjbtn=true
      }
      if(btn==='deductionobjbtn'){
        this.deductionobjbtn=true
      }
      if(btn==='contributionobjbtn'){
        this.contributionobjbtn=true
      }
      // btn = false;

      value.valid = false;
    }
    if (value.value === '' || value.value === null) {
      value.class = 'form-control';
      value.valid = '';
    }
    if (value.valid === true && value.valid === true && value.valid === true && value.valid === true) {
      btn = false;
      if(btn==='deductionobjbtn'){
        this.earningobjbtn=false
      }
      if(btn==='deductionobjbtn'){
        this.deductionobjbtn=false
      }
      if(btn==='contributionobjbtn'){
        this.contributionobjbtn=false
      }

    }
    // //  var exp =/^0*(?:[1-9][0-9]?|100)$/
    //   var exp= /^(0*[1-9][0-9]*)$/
    //   // var exp= /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;
    //   var re= new RegExp(exp) ;
    //   console.log('regex exp test :::::',re.test(this.obj.discp))
  }

}