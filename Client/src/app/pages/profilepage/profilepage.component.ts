import {Component, OnInit, trigger, state, style, transition, animate} from '@angular/core';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';

@Component({
  selector: 'app-profilepage',
  templateUrl: './profilepage.component.html',
  styleUrls: ['./profilepage.component.scss']
})
export class ProfilepageComponent implements OnInit {

  constructor(private navbarTitleService: NavbarTilteService) {}
  profilePage
  public ngOnInit() {
    this.navbarTitleService.updateTitle('User Profile');
    this.profilePage={
      title:'Profile',
      type:'create',
      table: {
        docDesign: 'widgets',
        view: 'appuser',
        export: false,
        colView: false,
        gridView: false,
        refresh: true,
        search: false,
        dateRange: false,
        settings:false,
        options:{
          display:true,
          rowView:false,
          rowDelete:true,
          rowUpdate:true,
        },
      },
      numrow: [10, 25, 100],    
      rows:[
          {col:[
            {class:'col-md-5',data:{label:'Company',name:'company',type:'text',showCol:true,placeholder:'Company',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
            {class:'col-md-3',data:{label:'Username',name:'username',type:'text',showCol:false,placeholder:'Username',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
            {class:'col-md-4',data:{label:'Email address',name:'email',type:'text',showCol:false,placeholder:'Email',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}}
          ]},
          {col:[
            {class:'col-md-6',data:{label:'First Name',name:'firstName',type:'text',showCol:true,placeholder:'First Name',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
            {class:'col-md-6',data:{label:'Last Name',name:'lastName',type:'text',showCol:false,placeholder:'Last Name',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
          ]},
          {col:[
            {class:'col-md-6',data:{label:'Username',name:'username',type:'text',showCol:false,placeholder:'Username',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
            {class:'col-md-6',data:{label:'Password',name:'password',type:'text',showCol:false,placeholder:'Password',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}}
          ]},
          {col:[
            {class:'col-md-12',data:{label:'Address',name:'address',type:'text',showCol:false,placeholder:'Address',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
          ]},
          {col:[
            {class:'col-md-4',data:{label:'City',name:'city',type:'text',showCol:true,placeholder:'City',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
            {class:'col-md-4',data:{label:'Country',name:'country',type:'text',showCol:true,placeholder:'Country',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
            {class:'col-md-4',data:{label:'Postal Code',name:'postCode',type:'text',showCol:true,placeholder:'Zip Code',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
          ]},
          {col:[
            {class:'col-md-12',data:{label:'About Me',name:'aboutMe',type:'text',showCol:false,placeholder:'Here can be your description',value:'',valid:'',ragexp:'^([a-z]{5,})$',class:'form-control',err:'this is invalid error '}},
          ]},
           ]
          }  
  }

}
