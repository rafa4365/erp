import { Component, OnInit , AfterContentInit,Input,Output,ViewChild,EventEmitter} from '@angular/core';
declare var $

@Component({
  selector: 'app-select2',
  templateUrl: './select2.component.html',
  styleUrls: ['./select2.component.scss']
})
export class Select2Component implements OnInit {
  currentClass='select2Basic'
  @Input()
  public schema:any;
  @Input()
  public updateValue: any;
static classIncrement=1;

  @Output() notify = new EventEmitter<any>();
  @ViewChild('value') value;
  
  

  constructor() { }

  ngOnInit() {
    // var inc=
this.currentClass=`currentClass${Select2Component.classIncrement++}`
    if(this.updateValue){
      console.log("select2 update value in this component",this.updateValue)
    }

    
    
  }

  ngAfterContentInit(){
    var $disabledResults = $(`#${this.currentClass}`);
    $disabledResults.select2();
    if(this.updateValue){
      $disabledResults.val(this.updateValue).trigger("change");
    }
    
    $disabledResults.on('select2:select', (e)=> {
      console.log("e.params.data ",e.params.data)
      var data = e.params.data;

            var obj={
        name:this.value.nativeElement.name,
        value:data.id
      }
      
      
      
        this.notify.emit(obj);


 
    });

  }

}
