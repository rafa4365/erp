import { Component, OnInit, Input,AfterViewInit} from '@angular/core';
import { NavbarTilteService } from '../../../lbd/services/navbar-tilte.service';
import {NotificationService, NotificationType, NotificationOptions}  from '../../../lbd/services/notification.service';
import { Location } from '@angular/common';
import { T2fschemaService } from '../../../lbd/services/t2fschema.service';
import { PouchbService } from '../../../lbd/services/pouchb.service';

@Component({
  selector: 'app-valid-form',
  templateUrl: './valid-form.component.html',
  styleUrls: ['./valid-form.component.scss']
})

export class ValidFormComponent implements OnInit {

      inputArray:any;
      @Input() Page:any;
      CreateSubscription:any
      UpdateSubscription:any
      updateValues:any

  constructor(
    private _location: Location,
    private notificationService: NotificationService,
    private db:PouchbService,    
    private t2f: T2fschemaService    
  ) {
    this.CreateSubscription = this.t2f.myvalue$.subscribe(val => this.Page = val);
    this.UpdateSubscription = this.t2f.uvalue$.subscribe(val => this.updateValues = val);

   }

  ngOnInit() {
    if (!this.Page.title) {
      this._location.back();
    } 
    // (<HTMLInputElement>document.getElementById("submit_btn")).disabled = true;          
    this.inputArray={};
    if (this.updateValues!=='' && this.Page.type!=='create') {
      console.log('oninit this.updateValues!==default message',this.updateValues!=='default message')
      for(var a=0;a<this.Page.rows.length;a++){
        for(var b=0;b<this.Page.rows[a].col.length;b++){
        this.Page.rows[a].col[b].data.value=this.updateValues[this.Page.rows[a].col[b].data.name]
        this.inputArray[this.Page.rows[a].col[b].data.name]=this.Page.rows[a].col[b].data.value        
        var re = new RegExp(this.Page.rows[a].col[b].data.ragexp);
        if(re.test(this.Page.rows[a].col[b].data.value)){
          // this.Page.rows[a].col[b].data.class='form-control valid'
          this.Page.rows[a].col[b].data.valid=true;
        }
        }
      }  
    }
  }

  public Create() { 
    console.log("this is profile form data",this.inputArray);
    this.inputArray['_id']=Date.now().toString();
    this.inputArray['view']=this.Page.table.view;
    this.db.query(this.inputArray);
    this.showNotification(
      'top',
      'center',
       2,
      'Your Form Is Submited !',
      'pe-7s-check');
      for(var a=0;a<this.Page.rows.length;a++){
        for(var b=0;b<this.Page.rows[a].col.length;b++){
          this.Page.rows[a].col[b].data.class='form-control';
          this.Page.rows[a].col[b].data.value=''
        }
      }
    this.back();
  }
  public Update() { 
    console.log("this is profile form data",this.inputArray);
    this.inputArray['_id']=this.updateValues._id;
    this.inputArray['view']=this.updateValues.view;
    this.inputArray['_rev']= this.updateValues._rev;
    this.db.query(this.inputArray);
    this.showNotification(
      'top',
      'center',
       3,
      'Your Form Is Updated !',
      'pe-7s-check');
      for(var a=0;a<this.Page.rows.length;a++){
        for(var b=0;b<this.Page.rows[a].col.length;b++){
          this.Page.rows[a].col[b].data.class='form-control';
          this.Page.rows[a].col[b].data.value=''
        }
      }
    this.back();
  }

  back() { 
    this._location.back();
    for(var a=0;a<this.Page.rows.length;a++){
      for(var b=0;b<this.Page.rows[a].col.length;b++){
        this.Page.rows[a].col[b].data.class='form-control';
        this.Page.rows[a].col[b].data.value=''        
      }
    } 
  }
  date(value) {
    console.log('date picker joining date',value)
  }
  nameSelect(value){
    console.log('nameSelect value',value)    
  }
  addMore(value){
    console.log('addmore value',value)    
  }

  validate(e,i,k){
    var re = new RegExp(this.Page.rows[i].col[k].data.ragexp);
    var count=0;
    var check=0;
    if (re.test(this.Page.rows[i].col[k].data.value) && e.target.className.search("ng-dirty")!==-1) {
          this.inputArray[this.Page.rows[i].col[k].data.name]=this.Page.rows[i].col[k].data.value
          this.Page.rows[i].col[k].data.class='form-control valid'
          this.Page.rows[i].col[k].data.valid=true;
        } 
      else if(e.target.className.search("ng-dirty")!==-1) {
        this.Page.rows[i].col[k].data.class='form-control invalid'
        this.Page.rows[i].col[k].data.valid=false;      
      }
      else if(e.target.className.search("ng-untouched")!==-1) {
        this.Page.rows[i].col[k].data.class='form-control';
        this.Page.rows[i].col[k].data.valid='';
      }
      for(var a=0;a<this.Page.rows.length;a++){
        for(var b=0;b<this.Page.rows[a].col.length;b++){
          check++;          
        if(this.Page.rows[a].col[b].data.valid===false){
          (<HTMLInputElement>document.getElementById("submit_btn")).disabled = true;
        }else if(this.Page.rows[a].col[b].data.valid===true){
          count++;
        } 
      }     
    }     
    if(count===check) {
      (<HTMLInputElement>document.getElementById("submit_btn")).disabled = false;            
    }
  }
  showNotification(from: string, align: string,type: NotificationType,message:any,icon:any) {
    this.notificationService.notify(new NotificationOptions({
      message: message,
      icon: icon,
      type: <NotificationType>(type),
      from: from,
      align: align
    }));
  }
  ngAfterViewInit(){
    if(this.Page.type==='create'){
          (<HTMLInputElement>document.getElementById("submit_btn")).disabled = true;          
    }
  }
}
