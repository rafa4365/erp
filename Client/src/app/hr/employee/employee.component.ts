import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../lbd/services/script-loader.service';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';
import { employee } from '../hr-schema';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  constructor(
    private navbarTitleService: NavbarTilteService,
    private _script: ScriptLoaderService,
  ) {
    Object.assign(this, { employee })
  }

  ngOnInit() {
    this.navbarTitleService.updateTitle('Employee');

  }
  ngAfterViewInIt() {
    this._script.load('app-extended-forms',
      '../../assets/js/bootstrap-checkbox-radio-switch-tags.js',
      '../../assets/js/light-bootstrap-dashboard.js')
  }

}
