import { Component, OnInit,Input ,AfterViewInit,Output,EventEmitter} from '@angular/core';

declare var $;
declare var moment;
@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent implements OnInit {
  
@Input() public schema:any;
@Input() public updateDate:any;

@Output() notify = new EventEmitter<any>();
nowdate={
name:null,
value:null
// valid:''
};
classd="datetimepicker";

  constructor() { }

  ngOnInit() {
    console.log("this.schema ",this.schema,this.schema.name)
    console.log("this.updateDate",this.updateDate)
    // this.schema.options.minDate=null
    // this.schema.options.maxDate=null
    
  }
  
ngAfterViewInit(){
    if(this.updateDate){
      // this.schema=this.updateDate
      this.schema.class='datetimepicker';
    }
    if(this.schema.enable==='now'){
      console.log('datepicker validation now',this.schema.enable) 
      delete this.schema.options.minDate
      delete this.schema.options.maxDate
      this.nowdate.value=moment().format('D/MM/YYYY h:mm:ss A');
      this.nowdate.name=this.schema.name
      this.getDate(this.nowdate.value,this.nowdate.name)
      this.schema.options.minDate=moment().add(1, 'days')
      this.schema.options.maxDate=moment().add(1, 'days') 
  $(`.${this.schema.class}`).datetimepicker(this.schema.options)
          //  if(this.updateDate){$(`.${this.schema.class}`).val(this.updateDate)}
    }
    if(this.schema.enable==='after'){
      console.log('datepicker validation after',this.schema.enable)
      // this.schema.options.minDate=null
      // $(`.${this.schema.class}`).MinDate(moment())
      delete this.schema.options.minDate
      delete this.schema.options.maxDate
      this.schema.options.minDate=moment()
      this.schema.options.format='D/MM/YYYY h:mm:ss A'
  $(`.${this.schema.class}`).datetimepicker(this.schema.options)
  // if(this.updateDate){$(`.${this.schema.class}`).val(this.updateDate)}
  
    }
    if(this.schema.enable==='before'){
      console.log('datepicker validation before',this.schema.enable)
      // this.schema.options.minDate=null
      // $(`.${this.schema.class}`).MaxDate(moment())
      delete this.schema.options.maxDate
      delete this.schema.options.minDate
      this.schema.options.maxDate=moment()
      this.schema.options.format='D/MM/YYYY h:mm:ss A'
      
      // this.schema.options.maxDate=moment()
  $(`.${this.schema.class}`).datetimepicker(this.schema.options)
  // if(this.updateDate){$(`.${this.schema.class}`).val(this.updateDate)}
  
    }
    if(this.schema.enable===null){
      console.log('datepicker validation null',this.schema.enable)  
      delete this.schema.options.minDate
      delete this.schema.options.maxDate
      this.schema.options.format='D/MM/YYYY h:mm:ss A'
      
  $(`.${this.schema.class}`).datetimepicker(this.schema.options)
  // if(this.updateDate){$(`.${this.schema.class}`).val(this.updateDate)}
  
    }

  
}
getDate(value,name){
  var obj={
    name:name,
    value:value
  }
  // console.log("lbd date value ------------",value)
  this.notify.emit(obj);

}
}
