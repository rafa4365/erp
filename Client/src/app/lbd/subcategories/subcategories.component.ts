import { Component, OnInit ,Input} from '@angular/core';

@Component({
  selector: 'app-subcategories',
  templateUrl: './subcategories.component.html',
  styleUrls: ['./subcategories.component.scss']
})
export class SubcategoriesComponent implements OnInit {
  @Input()
  public properties: any;
  constructor() { }

  ngOnInit() {
  }

}
