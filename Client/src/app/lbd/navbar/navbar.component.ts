import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { NavbarTilteService } from '../services/navbar-tilte.service';
import { NavItem, NavItemType } from '../services/interface';
import { AuthLoginService } from '../../lbd/services/auth-login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  // encapsulation: ViewEncapsulation.Native
})
export class NavbarComponent implements OnInit {
  public title: string;
  @Input()
  public navItems: NavItem[];

  constructor(private navbarTitleService: NavbarTilteService, private loginService: AuthLoginService) { }

  // @Input() title: string;

  public ngOnInit(): void {
    this.navbarTitleService.titleChanged$.subscribe(title => {
      this.title = title;
    });

  }

  public get leftNavItems(): NavItem[] {
    return this.navItems.filter(i => i.type === NavItemType.NavbarLeft);
  }

  public get rightNavItems(): NavItem[] {
    return this.navItems.filter(i => i.type === NavItemType.NavbarRight);
  }
  logout() {
    this.loginService.setlogout();

  }
}
