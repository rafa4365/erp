const { app, BrowserWindow } = require('electron')
    // const express = require('./server/main'); //your express app
    // const remote = require('electron').remote;

let win
let splash

function splashScreen() {
    splash = new BrowserWindow({
        width: 390,
        height: 600,
        frame: false,
        titleBarStyle: 'hidden-inset',
    });
    splash.loadURL(`file://${__dirname}/splashScreen.html`)
    splash.webContents.on('did-finish-load', function() {
        splash.show();
    });
    splash.on('closed', () => {
        splash = null;
    })
    setTimeout(() => {

        win = new BrowserWindow({
            width: 1200,
            height: 630,
            fullscreen: true,
            frame: false,
            titleBarStyle: 'hidden-inset',
        });
        splash.close();
        win.hide();
        win.loadURL(`http://localhost:4200`)

        win.on('closed', () => {
            win = null;
        })
        win.webContents.on('did-finish-load', function() {
            win.show();
        });

        win.focus();
    }, 4000)

}

function closeapp() {
    // var window = remote.getCurrentWindow();
    win.close();
}
app.on('ready', splashScreen)

app.on('all-window-closed', () => {
    app.quit();
})