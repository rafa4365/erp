import { NavItem, NavItemType } from '../lbd/services/interface';
import { validation } from '../validation';

var date = {
  icons: {
    time: "fa fa-clock-o",
    date: "fa fa-calendar",
    up: "fa fa-chevron-up", 
    down: "fa fa-chevron-down",
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-screenshot',
    clear: 'fa fa-trash',
    close: 'fa fa-remove'
  }
}
//admin main
export var sidebarData = {
  sidenavItems: [
    { title: 'Admin Summary', routerLink: 'adminSummary', iconClass: 'pe-7s-display1' },


    {
      title: 'POS', iconClass: 'pe-7s-shopbag', href: 'pos', sub: [
        { title: 'Customer', routerLink: 'customer', iconClass: 'pe-7s-server' },
        { title: 'SaleQuote', routerLink: 'saleQuote', iconClass: 'pe-7s-config' },
        { title: 'Sale Order', routerLink: 'saleOrder', iconClass: 'pe-7s-config' },
        { title: 'Sale Invoice', routerLink: 'saleInvoice', iconClass: 'pe-7s-config' },
        { title: 'Delivery Note', routerLink: 'deliveryNote', iconClass: 'pe-7s-tools' },
        // { title: 'pos Inventory', routerLink: 'posInventory', iconClass: 'pe-7s-gleam' },
        // { title: 'pos Reports', routerLink: 'posReports', iconClass: 'pe-7s-config' },
        { title: 'Pos Summary', routerLink: 'posSummary', iconClass: 'pe-7s-config' }
      ]
    },
    {
      title: 'POP', iconClass: 'pe-7s-cart', href: 'pop', sub: [
        { title: 'Supplier', routerLink: 'supplier', iconClass: 'pe-7s-server' },
        { title: 'Pop Inventory', routerLink: 'popInventory', iconClass: 'pe-7s-config' },
        { title: 'Purchase Order', routerLink: 'purchaseOrder', iconClass: 'pe-7s-config' },
        { title: 'Purchase Invoice', routerLink: 'purchaseInvoice', iconClass: 'pe-7s-config' },
        { title: 'Expense Category', routerLink: 'expenseCategory', iconClass: 'pe-7s-tools' },
        { title: 'Expense', routerLink: 'expense', iconClass: 'pe-7s-gleam' },
        // { title: 'Pop Reports', routerLink: 'popReports', iconClass: 'pe-7s-config' },
        { title: 'Pop Summary', routerLink: 'popSummary', iconClass: 'pe-7s-config' }
      ]
    },
    {
      title: 'HR', iconClass: 'pe-7s-id', href: 'hr', sub: [

        { title: 'Employee', routerLink: 'employee', iconClass: 'pe-7s-server' },
        { title: 'PaySlip', routerLink: 'paySlip', iconClass: 'pe-7s-tools' },
        // { title: 'Hr Reports', routerLink: 'hrReports', iconClass: 'pe-7s-gleam' },
        { title: 'Hr Summary', routerLink: 'hrSummary', iconClass: 'pe-7s-config' }
      ]
    },
    { title: 'Admin Permission', routerLink: 'adminPermission', iconClass: 'pe-7s-attention' },
    // { title: 'Admin Reports', routerLink: 'adminReports', iconClass: 'pe-7s-gleam' },
    { title: 'Admin Setting', routerLink: 'adminSetting', iconClass: 'pe-7s-config' },
    { title: 'Server Setting', routerLink: 'serverSetting', iconClass: 'pe-7s-config' },
    

  ],
  logoDropdown: {
    header: "Name",
    list: [
      { title: "My Profile", link: "profile" },
      // { title: "Edit Profile" },
      // { title: "Settings" }
    ]
  },
  sidebarMeta: {
    headerTextFull: 'Paper Clip',
    headerTextMini: 'PC',
    headerLink: 'http://www.google.com',
    headerLogoImg: '/assets/images/profile.jpg',
    backgroundColor: 'blue',
    backgroundImg: '../../assets/images/full-screen-image-3.jpg',
  }
}
export var navItems:NavItem[] = [
    
          // {
          //   type: NavItemType.NavbarRight,
          //   title: ' Notifications',
          //   iconClass: 'fa fa-bell-o',
          //   ListClass: 'dropdown',
          //   numNotifications: 3,
          //   dropdownClass: 'dropdown-menu',
    
          //   dropdownItems: [
          //     { title: 'Notification 1' },
          //     { title: 'Notification 2' },
    
          //     { title: 'Another Notification' }
          //   ]
          // },
          {
            type: NavItemType.NavbarRight,
            title: 'Settings',
            iconClass: 'fa fa-list',
            ListClass: 'dropdown dropdown-with-icons  ',
            dropdownClass: 'dropdown-menu dropdown-with-icons',
    
            dropdownItems: [
    
              { title: 'Settings', icon: 'pe-7s-tools' ,routerLink:'adminSetting'},
              'separator',
              { title: 'Log out', icon: 'pe-7s-close-circle', class: 'text-danger' }
            ]
          },
    
        ];


        //settings form
        export var settings = {
          title: 'Settings',
          type: 'create',
          table: {
            docDesign: 'admin',
            view: 'settings',
            export: false,
            colView: false,
            gridView: false,
            refresh: true,
            search: false,
            dateRange: false,
            settings: false,
            options: {
              display: true,
              rowView: false,
              rowDelete: true,
              rowUpdate: true,
            },
          },
          numrow: [10, 25, 100],
          rows: [
            {
              col: [
                { class: 'col-md-6', data: { label: 'Company Name', name: 'companyname', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Company Name', value: '', valid: '', ragexp: validation.itemname.regex, class: 'form-control', err: validation.itemname.msg } },
                { class: 'col-md-6', data: { label: 'Branch Name', name: 'branchname', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Branch Name', value: '', valid: '', ragexp: validation.shortdescription.regex, class: 'form-control', err: validation.itemname.msg } },
              ]
            },
            {
              col: [
                { class: 'col-md-6', data: { label: 'City', name: 'city', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'City', value: '', valid: '', ragexp: validation.itemname.regex, class: 'form-control', err: validation.itemname.msg } },
                { class: 'col-md-6', data: { label: 'Country', name: 'country', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Country', value: '', valid: '', ragexp: validation.itemname.regex, class: 'form-control', err: validation.itemname.msg } },
              ]
            },
            {
              col: [
                { class: 'col-md-4', data: { label: 'Web Site', name: 'website', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Web Site', value: '', valid: '', ragexp: validation.address.regex, class: 'form-control', err: validation.itemname.msg } },
                { class: 'col-md-4', data: { label: 'Telephone Number', name: 'telephonenumber', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Telephone Number', value: '', valid: '', ragexp: validation.phone.regex, class: 'form-control', err: validation.phone.msg } },
                { class: 'col-md-4', data: { label: 'Mobile Number', name: 'mobilenumber', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Mobile Number', value: '', valid: '', ragexp: validation.phone.regex, class: 'form-control', err: validation.phone.msg } },
              ]
            },
        
            {
              col: [
                { class: 'col-md-12', data: { label: 'logo', name: 'logo', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Logo', value: '', valid: '', ragexp: validation.shortdescription.regex, class: 'form-control', err: validation.shortdescription.msg } }
              ]
            },
        
            {
              col: [
                { class: 'col-md-12', data: { label: 'Address', name: 'address', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Address Goes Here..!', value: '', valid: '', ragexp: validation.address.regex, class: 'form-control', err: validation.address.regex } },
              ]
            }
        
          ]
        }
