import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateRangPickerLbdComponent } from './date-rang-picker-lbd.component';

describe('DateRangPickerLbdComponent', () => {
  let component: DateRangPickerLbdComponent;
  let fixture: ComponentFixture<DateRangPickerLbdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateRangPickerLbdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateRangPickerLbdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
