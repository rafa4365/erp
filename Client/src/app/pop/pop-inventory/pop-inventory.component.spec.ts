import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopInventoryComponent } from './pop-inventory.component';

describe('PopInventoryComponent', () => {
  let component: PopInventoryComponent;
  let fixture: ComponentFixture<PopInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
