import { Component, OnInit ,AfterViewInit,Input} from '@angular/core';
import { SweetAlertsService , swalType, swalOptions } from '../services/sweet-alerts.service';

declare var $;

@Component({
  selector: 'app-calendercc',
  templateUrl: './calendercc.component.html',
  styleUrls: ['./calendercc.component.scss']
})
export class CalenderccComponent implements OnInit {
  @Input()
  public event: any; 
  calender;
  today;
  swal;
  event_title;
  static currentId = 1;
  public calenderId: string;
  constructor(private SweetAlertsService: SweetAlertsService) { }

  ngOnInit() {
    this.calenderId = `lbd-calender-${CalenderccComponent.currentId++}`;

    
    
  }
  ngAfterViewInit(){

   this.calender = $('#fullCalendar123');
    
          this.today = new Date();
    
         this.calender.fullCalendar({
              header: {
                  left: 'title',
                  center: 'month,agendaWeek,agendaDay',
                  right: 'prev,next today'
              },
              defaultDate: this.today,
              selectable: true,
              selectHelper: true,
              titleFormat: {
                  month: 'MMMM YYYY', // September 2015
                  week: "MMMM D YYYY", // September 2015
                  day: 'D MMM, YYYY' // Tuesday, Sep 8, 2015
              },
              select: (start, end)=> {
                this.SweetAlertsService.swAlert(new swalOptions({
                  title: 'Create an Event',
                  html: '<br><input class="form-control" placeholder="Event Title" id="input-field">',
                  showCancelButton: true,
                  closeOnConfirm: true,
                  showConfirmButton: true,  
                  closeOnCancel:true  ,                                 
                  start:start,
                  end:end,
                  calender:this.calender
                }),9);

              },
              editable: true,
              eventLimit: true, // allow "more" link when too many events
    
    
              // color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
              events: this.event
          });
    
    
  }
}
