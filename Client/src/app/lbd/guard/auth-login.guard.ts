import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthLoginService } from '../services/auth-login.service';

@Injectable()
export class AuthLoginGuard implements CanActivate {

  constructor(private loginService: AuthLoginService, private router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let status: boolean = this.loginService.getLoggedIn();
    // setTimeout
    if (status === false) {
      // status=true
      this.router.navigate(['/'])
    }
    return this.loginService.getLoggedIn();
  }
}
