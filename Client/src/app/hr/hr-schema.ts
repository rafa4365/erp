import { NavItem, NavItemType } from '../lbd/services/interface';
import { validation } from '../validation';

var date = {
  icons: {
    time: "fa fa-clock-o",
    date: "fa fa-calendar",
    up: "fa fa-chevron-up",
    down: "fa fa-chevron-down",
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-screenshot',
    clear: 'fa fa-trash',
    close: 'fa fa-remove'
  }
}
//--------------------------hr main---------------------

//hr main
export var sidebarData = {
  sidenavItems: [
    { title: 'Hr Summary', routerLink: 'hrSummary', iconClass: 'pe-7s-display1' },
    { title: 'Employee', routerLink: 'employee', iconClass: 'pe-7s-id' },
    { title: 'PaySlip', routerLink: 'paySlip', iconClass: 'pe-7s-cash' },
    // { title: 'Hr Reports', routerLink: 'hrReports', iconClass: 'pe-7s-gleam' },


  ],
  logoDropdown: {
    header: "Name",
    list: [
      { title: "My Profile", link: "profile" },
      // { title: "Edit Profile" },
      // { title: "Settings" }
    ]
  },
  sidebarMeta: {
    headerTextFull: 'Paper Clip',
    headerTextMini: 'PC',
    headerLink: 'http://www.google.com',
    headerLogoImg: '/assets/images/profile.jpg',
    backgroundColor: 'blue',
    backgroundImg: '../../assets/images/full-screen-image-3.jpg',
  }
}

export var navItems: NavItem[] = [


  {
    type: NavItemType.NavbarRight,
    title: 'Settings',
    iconClass: 'fa fa-list',
    ListClass: 'dropdown dropdown-with-icons  ',
    dropdownClass: 'dropdown-menu dropdown-with-icons',

    dropdownItems: [

      { title: 'Log out', icon: 'pe-7s-close-circle', class: 'text-danger' }
    ]
  },

];


// employee

//----------------------------hr  employee-----------------------------------

export var employee = {
  title: 'Employee',
  type: null,
  table: {
    docDesign: 'hr',
    view: 'employee',
    export: false,
    colView: true,
    gridView: true,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: false,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [
    {
      col: [
        { class: 'col-md-4', data: { label: 'Name', name: 'name', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Name', value: '', valid: '', ragexp: validation.fullname.regex, class: 'form-control', err: validation.fullname.msg } },
        { class: 'col-md-4', data: { label: 'TelePhone', name: 'telephone', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'TelePhone', value: '', valid: '', ragexp: validation.phone.regex, class: 'form-control', err: validation.phone.msg } },
        { class: 'col-md-4', data: { label: 'Mobile', name: 'mobile', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Mobile', value: '', valid: '', ragexp: validation.phone.regex, class: 'form-control', err: validation.phone.msg } }
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Email address', name: 'email', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Email', value: '', valid: '', ragexp: validation.email.regex, class: 'form-control', err: validation.email.msg } },
        { class: 'col-md-6', data: { label: 'Address', name: 'address', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Address', value: '', valid: '', ragexp: validation.address.regex, class: 'form-control', err: validation.address.msg } }
      ]
    },
    {
      col: [
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { label: 'Additional Information', name: 'AdditionalInfo', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.information.regex, class: 'form-control', err: validation.information.msg } }
      ]
    },

  ]
}

// pay slip


export var pay_slip = {
  title: 'pay slip',
  type: null,
  table: {
    docDesign: 'hr',
    view: 'paySlip',
    export: false,
    colView: false,
    gridView: false,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: true,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [

    {
      col: [
        { class: 'col-md-4', data: { label: 'Date', options: date, enable: 'before', class: 'datetimepicker', placeholder: 'PaySlip date here', name: 'payslipDate', fieldtype: 'datepicker', showCol: true, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
        // { class: 'col-md-4 col-md-offset-4', data: { label: 'Due Date', options: date, class: 'datetimepicker', placeholder: 'Due Date', name: 'DueDate', fieldtype: 'datepicker', showCol: false, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'Employee', fieldtype: 'nameresult', showCol: false, view: 'employee', docDesign: 'hr', searchvalue: 'name', resultvalue: 'address', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'payslip', fieldtype: 'addmorepayslip', showCol: false,value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { label: 'Notes', name: 'Notes', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.information.regex, class: 'form-control', err: validation.information.msg } },
      ]
    },
  ]

}