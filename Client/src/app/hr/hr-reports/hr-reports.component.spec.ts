import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HRReportsComponent } from './hr-reports.component';

describe('HRReportsComponent', () => {
  let component: HRReportsComponent;
  let fixture: ComponentFixture<HRReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HRReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HRReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
