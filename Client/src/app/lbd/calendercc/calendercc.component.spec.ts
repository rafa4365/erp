import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalenderccComponent } from './calendercc.component';

describe('CalenderccComponent', () => {
  let component: CalenderccComponent;
  let fixture: ComponentFixture<CalenderccComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalenderccComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalenderccComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
