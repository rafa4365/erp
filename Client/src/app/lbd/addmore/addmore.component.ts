import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core'; 
// import * as Immutable from 'immutable';
@Component({
  selector: 'app-addmore',
  templateUrl: './addmore.component.html',
  styleUrls: ['./addmore.component.scss']
})
export class AddmoreComponent implements OnInit {
  @Input()  public schema: any;
  @Input()  public row: any;
  @Input()  public updateValue: any;
  

  arrayi:any=[];
  arrayj:any=[];
  @Output() notify= new EventEmitter<any>();
  @Output() add= new EventEmitter<any>();
  nestedarray=[] ;
  imute;
  constructor() {
    
   }

  ngOnInit() {
    localStorage.setItem("addmore", JSON.stringify(this.schema.schema[0]));
    this.nestedarray.push(JSON.parse(localStorage.getItem('addmore')))
    
    // console.log("nested array before update",this.updateValue)
    if(this.updateValue){

      this.nestedarray=this.updateValue
      console.log("add more component for update value",this.updateValue)
    }
    
  }
  
  more(){
    this.nestedarray.push(JSON.parse(localStorage.getItem('addmore')))
  }

  delete(i){
    this.nestedarray.splice(i,1)
    console.log('delete form t-form',i)
  }
  select2(value){
    this.nestedarray[value.name]=value.value;
  }
  getValue(){
    console.log("get value function", this.nestedarray)
    var obj={
      name: this.schema.name,
      value:this.nestedarray
    }
    this.notify.emit(obj)

  }
  getValues(nameI,value,indexI,indexJ,name){
  //   var obj={
  //     info:{
  //       index:indexJ,
  //       name:name
  //     },
  //     data:{
  //     name:nameI,
  //     value:value,
  //     index:indexI
  //   }
  //   }
  //   console.log("addmmore object values",obj)
  //   console.log("test values",this.schema.schema)
    
  //   this.arrayj.push({
  //    name:obj.data.name,
  //    value:obj.data.value,
  //  })

  //  console.log("addmore array of j",this.arrayj)
  //  console.log("schema length ",this.schema.schema[0].length)
   
  //  if(this.arrayj.length === 3){
  //    console.log("if condition ",this.arrayj.length)
   
  //    this.arrayi.push(this.arrayj)
     
  //    console.log("addmore comp values array of i",this.arrayi)
  //   //  this.arrayj.length =[];

  //  }
  // //  this.array.push({})
  // //  this.notify.emit(obj)
  }
}
