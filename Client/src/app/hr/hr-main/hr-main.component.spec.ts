import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HRMainComponent } from './hr-main.component';

describe('HRMainComponent', () => {
  let component: HRMainComponent;
  let fixture: ComponentFixture<HRMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HRMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HRMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
