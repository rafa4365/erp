import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';

declare var $;

@Component({
  selector: 'app-addmore-expense-valid',
  templateUrl: './addmore-expense-valid.component.html',
  styleUrls: ['./addmore-expense-valid.component.scss']
})
export class AddmoreExpenseValidComponent implements OnInit {
  @Input() schema;
  @Output() notify = new EventEmitter<any>();
  @Input() public update: any;
  
  obj={
    name:null,
    discp:'',
    amount:null,
  }
  array:any=[];
  total=0;
  select2data=[];
  name={
    class:'form-control',
    valid:null
  };
  dis={
    class:'form-control',
    valid:null
  };
  amount={
    class:'form-control',
    valid:null
  };
  constructor(private db: PouchbService) { }
  
  ngOnInit() {
    console.log('addmore expense valid schema',this.schema);
    
    (<HTMLInputElement>document.getElementById("moreexpense")).disabled = true;    
    this.select();
    this.updatevalues(this.update)
    
  }
  updatevalues(value){
    if(value){
      this.array=value
      this.totalFun()
      
    }
  }
  add(){
    console.log('addmore valid add function',this.obj);
    (<HTMLInputElement>document.getElementById("moreexpense")).disabled = true;    
    
    this.array.push(this.obj);
    this.totalFun() 
    this.obj={
      name:'',
      discp:'',
      amount:null,
    }
    this.check();
    this.notify.emit(this.array);
    
  }
  remove(i){
    this.array.splice(i,1)
    this.totalFun()    
    if(this.array.length===0){
      this.schema.valid=false;
      this.notify.emit(this.array);
      console.log('remove',this.schema)

    }
    
  }
  totalFun(){
    this.total=0
    this.array.forEach((element)=>{
      this.total+=parseInt(element.amount) 
    })
    this.array.total=this.total;
    
  }
  select(){
    var obj={
      view:this.schema.view,
      docDesign:this.schema.docDesign
    }
    this.db.fetchData('Showing All',obj).then((r:any)=>{
      for(var i=0;i< r.rows.length;i++){
        this.select2data[i]={
          id:i,
          text:r.rows[i].doc[this.schema.searchvalue],
          obj:r.rows[i]
        }
      }
      console.log('fetch result ',this.select2data)
      $(".addmoreexpensevalid").select2({
        placeholder: "Select",
        allowClear: true,
        
        data: this.select2data
      })
    })
    console.log('expense add more', this.schema)



$(".addmoreexpensevalid").on('select2:select',  (e)=> {
  // Do something
  // console.log('e', e.params.data.text)
  
  console.log('select2 value',this.select2data[e.target.value])
  this.obj.name=e.params.data.text
  // this.obj.discp=this.select2data[e.target.value].obj.doc.descrpition

  // this.result=e.target.value;
  this.math()
  this.change()
});
  }
  math(){

  }
  change(){
    this.math();
    this.check();
  }
  check(){
    var _name=new RegExp(/^([a-z A-Z.]{2,})$/);    
    var dis_exp=new RegExp(/^([a-z A-Z.]{1,})$/);
    var amount_exp=new RegExp(/^([0-9]{1,})$/);

    this.validation(this.obj.name,_name,this.name);    
    this.validation(this.obj.discp,dis_exp,this.dis);
    this.validation(this.obj.amount,amount_exp,this.amount);
  }
    validation(value,regex,obj){
      
      if(regex.test(value) && value!==''){
        obj.class='form-control valid';
        obj.valid=true;
      }
      if(!regex.test(value) && value!==''){
        obj.class='form-control invalid';
        (<HTMLInputElement>document.getElementById("moreexpense")).disabled = true;    
        
        obj.valid=false;
      }
      if(value ==='' || value===null){
        obj.class='form-control';
        obj.valid='';
      }
      if(this.name.valid===true && this.dis.valid===true && this.amount.valid===true){
        (<HTMLInputElement>document.getElementById("moreexpense")).disabled = false;    
        
      }
    }



}
