;import { Component, OnInit , AfterContentInit,Input,Output,ViewChild,EventEmitter,ViewEncapsulation,AfterViewInit} from '@angular/core';


declare var $
declare var Bloodhound 
declare var Handlebars

@Component({
  selector: 'app-typeahead',
  templateUrl: './typeahead.component.html',
  styleUrls: ['./typeahead.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class TypeaheadComponent implements OnInit {
  substrRegex:any

  @Input()
  public schema:any;

  @Input()
  public updateValue: any;


  @Output() notify = new EventEmitter<any>();
  // @ViewChild('value') value;

  constructor() { }

  ngOnInit() {
    console.log("typehead update value",this.updateValue)

  }

  getValue(value,name){
    var obj={
      name:name,
      value:value 
    }

    // console.log("type ahead value ",value,name)

  this.notify.emit(obj);


}
ngAfterViewInit(){
  var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
  'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
  'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
  'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
  'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
  'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
  'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
  'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];
  var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        this.substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
            if (this.substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};
// $('#m_typeahead_1, #m_typeahead_1_modal, #m_typeahead_1_validate, #m_typeahead_2_validate, #m_typeahead_3_validate').typeahead({
//   hint: true,
//   highlight: true,
//   minLength: 1
// }, {
//   name: 'states',
//   source: substringMatcher(states)
// });

 var bloodhound = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  // `states` is an array of state names defined in "The Basics"
  local: states
});

$('#m_typeahead_2').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'states',
  source: bloodhound
});


}
}
