import { Component, OnInit ,Input,AfterViewInit,Output,EventEmitter} from '@angular/core';
import { ScriptLoaderService } from '../services/script-loader.service';
import { PouchbService } from '../../lbd/services/pouchb.service';
import { T2fschemaService } from '../../lbd/services/t2fschema.service';
// import { FormBuilder, FormGroup,FormControl, Validators } from '@angular/forms';
// import { Subscription } from 'rxjs/Subscription';

declare var $;

@Component({
  selector: 'app-c-table',
  templateUrl: './c-table.component.html',
  styleUrls: ['./c-table.component.scss'] 
})
export class CTableComponent implements OnInit {

  // form: FormGroup;
  // updategroup:FormGroup;
  
  @Input()
  public experiment: any;
  defaultPageSize:any;
  dataArray: any=[];
  toggleVar:boolean=false;
  createModal:any={type:'Create', buttonClass:'btn',modalTitle:'Create'};
  updateModal:any={type:'Update',buttonIcon:'fa fa-edit',buttonClass:'table-action edit',modalTitle:'Update'};
  viewModal:any={type:'View', buttonIcon: "fa fa-eye",buttonClass:'table-action eye',modalTitle:'view'};
  setpagelimit;
  viewOption={}
  // subscription:Subscription;
  
  searchInputObject:any;
  visibleRowsInputObject:any;

  @Output() updatevalues = new EventEmitter<any>();
  
  constructor(
    private _script: ScriptLoaderService,
    private db:PouchbService,
    // private fb: FormBuilder, 
    private t2f: T2fschemaService
  ) { }

  ngOnInit() {
    this.db.createView(this.experiment.table,this.experiment.numrow[0]).then((doc)=>{
      for(var i=0;i < doc.rows.length;i++){
        this.dataArray[i]=doc.rows[i].doc;
      };
      console.log('dataArray ',this.dataArray)          
    })
    this.defaultPageSize=this.experiment.numrow[0]
    this.setpagelimit=this.experiment.numrow[0]

    //-----search -------
    this.searchInputObject={
      form:this.experiment.form,
      table:this.experiment.table,
      dpSize:this.defaultPageSize,
    }
    
    this.visibleRowsInputObject={
      num:this.experiment.numrow,
      table:this.experiment.table,
      dpSize:this.defaultPageSize,
    }
   
  this.viewOption={
    export: this.experiment.table.export || false,
    colView: this.experiment.table.colView || false,
    gridView: this.experiment.table.gridView || false,
    refresh: this.experiment.table.refresh || false,
    search: this.experiment.table.search || false,
    dateRange: this.experiment.table.dateRange || false,
  }
   
    }

  // submit(values){this.dataArray.push(values)}

  refresh(){
    this.dataArray=[];
    if(this.defaultPageSize!='Showing All'){
      $('#paginate').pagination('show')
    }
    $('#pagelimit').show();
    this.db.fetchData(this.defaultPageSize,this.experiment.table).then((doc)=>{
      for(var i=0;i < doc.rows.length;i++){
        this.dataArray[i]=doc.rows[i].doc;
      };
    });
  }

  remove(value,i){
    this.dataArray.splice(i,1)
    this.db.remove(value);
  }

  // updateform(event,values){
  //   var updateProperties=new Object;
  //   for(var i =0;i<this.experiment.form.length;i++){
  //     updateProperties[this.experiment.form[i].name]=this.experiment.form[i].property[0]=values[this.experiment.form[i].name];
  //   };
  //   updateProperties['_id']=values['_id'];
  //   updateProperties['_rev']=values['_rev'];
  //   this.updategroup= this.fb.group(updateProperties);    
  // }

  // updateQuery(obj){this.dataArray.splice(obj.i,1,obj.values)}

  search(value){this.dataArray=value}

  showCol(value,name,colstate,colindex){this.experiment.form[colindex].showCol=value;}

  toggleView(value){this.toggleVar = value}

  getpagelimit(value){this.dataArray=value}

  create(){
    this.t2f.setvalue(this.experiment) 
    this.t2f.updateValue('')
    
  }

  update(value){
    console.log("update uppppppppppppppppppp",value)
    this.t2f.setvalue(this.experiment)
    this.t2f.updateValue(value)
  }

  ngAfterViewInit(){this._script.load('app-c-table','../../assets/js/light-bootstrap-dashboard.js' )}
}
