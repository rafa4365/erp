import { TestBed, inject } from '@angular/core/testing';

import { SweetAlertsService } from './sweet-alerts.service';

describe('SweetAlertsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SweetAlertsService]
    });
  });

  it('should be created', inject([SweetAlertsService], (service: SweetAlertsService) => {
    expect(service).toBeTruthy();
  }));
});
