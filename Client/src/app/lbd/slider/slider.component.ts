import { Component, OnInit ,Input,AfterViewInit} from '@angular/core';

declare var $;
@Component({
  selector: '.app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

@Input()
public schema:any;
static currentId = 1;
public sliderId: string;

  constructor() { }

  ngOnInit() {
    this.sliderId = `lbd-slider-${SliderComponent.currentId++}`;
  }
  
  ngAfterViewInit(){
    $(`#${this.sliderId}`).slider(this.schema.options);
  }

}
