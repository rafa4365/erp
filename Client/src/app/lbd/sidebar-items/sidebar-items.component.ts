import { Component, Input } from '@angular/core';
import {  NavItem, NavItemType } from '../services/interface';

@Component({
  selector: 'app-sidebar-items',
  templateUrl: './sidebar-items.component.html',
  styleUrls: ['./sidebar-items.component.scss']
})
export class SidebarItemsComponent {
  @Input()
  navItems: NavItem[];

  @Input()
  navbarClass: string;

  @Input()
  showSeparator: boolean;

  constructor() { }
}
