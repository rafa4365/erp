import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleQuoteComponent } from './sale-quote.component';

describe('SaleQuoteComponent', () => {
  let component: SaleQuoteComponent;
  let fixture: ComponentFixture<SaleQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
