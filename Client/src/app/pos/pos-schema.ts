import { NavItem, NavItemType } from '../lbd/services/interface';
import { validation } from '../validation';

var date = {
  icons: {
    time: "fa fa-clock-o",
    date: "fa fa-calendar",
    up: "fa fa-chevron-up",
    down: "fa fa-chevron-down",
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-screenshot',
    clear: 'fa fa-trash',
    close: 'fa fa-remove'
  }
}
//--------------------------pos main---------------------

export var sidebarData = {
  sidenavItems: [
    { title: 'pos Summary', routerLink: 'posSummary', iconClass: 'pe-7s-display1' },
    { title: 'Customer', routerLink: 'customer', iconClass: 'pe-7s-add-user' },
    { title: 'sale Quote', routerLink: 'saleQuote', iconClass: 'pe-7s-note' },
    { title: 'sale Order', routerLink: 'saleOrder', iconClass: 'pe-7s-shopbag' },
    { title: 'sale Invoice', routerLink: 'saleInvoice', iconClass: 'pe-7s-note2' },
    { title: 'Delivery Note', routerLink: 'deliveryNote', iconClass: 'pe-7s-gift' },
    // { title: 'pos Inventory', routerLink: 'posInventory', iconClass: 'pe-7s-gleam' },
    // { title: 'pos Reports', routerLink: 'posReports', iconClass: 'pe-7s-config' }


  ],
  logoDropdown: {
    header: "Name",
    list: [
      { title: "My Profile", link: "profile" },
      // { title: "Edit Profile" },
      // { title: "Settings" }
    ]
  },
  sidebarMeta: {
    headerTextFull: 'Paper Clip',
    headerTextMini: 'PC',
    headerLink: 'http://www.google.com',
    headerLogoImg: '/assets/images/profile.jpg',
    backgroundColor: 'blue',
    backgroundImg: '../../assets/images/full-screen-image-3.jpg',
  }
}

export var navItems: NavItem[] = [

  {
    type: NavItemType.NavbarRight,
    title: 'Settings',
    iconClass: 'fa fa-list',
    ListClass: 'dropdown dropdown-with-icons  ',
    dropdownClass: 'dropdown-menu dropdown-with-icons',

    dropdownItems: [

      { title: 'Log out', icon: 'pe-7s-close-circle', class: 'text-danger' }
    ]
  },

];

//customer


//----------------------------pos  customer-----------------------------------

export var customer = {
  title: 'Customer',
  type: null,
  table: {
    docDesign: 'pos',
    view: 'customer',
    export: false,
    colView: true,
    gridView: true,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: false,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [
    {
      col: [
        { class: 'col-md-2 ', data: { label: 'Customer #', name: 'reference', fieldtype: 'auto', showCol: false, prefix: 'CUS', value: ''}},
        { class: 'col-md-3', data: { label: 'Name', name: 'name', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Name', value: '', valid: '', ragexp: validation.fullname.regex, class: 'form-control', err: validation.fullname.msg } },
        { class: 'col-md-3', data: { label: 'TelePhone', name: 'telephone', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'TelePhone', value: '', valid: '', ragexp: validation.phone.regex, class: 'form-control', err: validation.phone.msg } },
        { class: 'col-md-4', data: { label: 'Mobile', name: 'mobile', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Mobile', value: '', valid: '', ragexp: validation.phone.regex, class: 'form-control', err: validation.phone.msg } }
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Email address', name: 'email', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Email', value: '', valid: '', ragexp: validation.email.regex, class: 'form-control', err: validation.email.msg } },
        { class: 'col-md-6', data: { label: 'Address', name: 'address', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Address', value: '', valid: '', ragexp: validation.address.regex, class: 'form-control', err: validation.address.msg } }
      ]
    },
    {
      col: [
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { label: 'Additional Information', name: 'AdditionalInfo', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.information.regex, class: 'form-control', err: validation.information.msg } }
      ]
    },

  ]
}
//-----------------------------delivery notes

export var delivery_note = {
  title: 'delivery note',
  type: null,
  table: {
    docDesign: 'pos',
    view: 'deliveryNote',
    export: false,
    colView: false,
    gridView: false,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: true,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [
    {
      col: [
        { class: 'col-md-4 ', data: { label: 'Delivery Note #', name: 'reference_delivery', fieldtype: 'auto', showCol: false, prefix: 'DN', value: ''}}
        
        // { class: 'col-md-4', data: { label: 'Delivery Note #', name: 'delivery_note', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'DNXXXXX', value: '', valid: '', ragexp: validation.deliverynote.regex, class: 'form-control', err: validation.deliverynote.msg } },
        // { class: 'col-md-4 col-md-offset-4', data: { label: 'Sale Invoice #', name: 'reference', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'SIXXXXXX', value: '', valid: '', ragexp: validation.saleinvoice.regex, class: 'form-control', err: validation.saleinvoice.msg } }
      ]
    },
    {
      col: [
        { class: 'col-md-4', data: { label: 'Sale Order #', name: 'sale_Order', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'SOXXXXX', value: '', valid: '', ragexp: validation.saleorder.regex, class: 'form-control', err: validation.saleorder.msg } },
        // { class: 'col-md-4 col-md-offset-4', data: { label: 'Sale Invoice #', name: 'reference', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'SIXXXXXX', value: '', valid: '', ragexp: validation.saleinvoice.regex, class: 'form-control', err: validation.saleinvoice.msg } }
      ]
    },
    {
      col: [
        // { class: 'col-md-4', data: { label: 'Sale Order #', name: 'sale_Order', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'SOXXXXX', value: '', valid: '', ragexp: validation.saleorder.regex, class: 'form-control', err: validation.saleorder.msg } },
        { class: 'col-md-4', data: { label: 'Sale Invoice #', name: 'reference', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'SIXXXXXX', value: '', valid: '', ragexp: validation.saleinvoice.regex, class: 'form-control', err: validation.saleinvoice.msg } }
      ]
    },
    {
      col: [
        { class: 'col-md-4', data: { label: 'Delivery Date', options: date, enable: 'after', class: 'datetimepicker', placeholder: 'Delivery date here', name: 'deliveryDate', fieldtype: 'datepicker', showCol: true, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
        // { class: 'col-md-4 col-md-offset-4', data: { label: 'Due Date', options: date,enable:'after', class: 'datetimepicker', placeholder: 'Due Date', name: 'DueDate', fieldtype: 'datepicker', showCol: false, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'Customer', fieldtype: 'nameresult', showCol: false, view: 'customer', docDesign: 'pos', searchvalue: 'name', resultvalue: 'address', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Delivery Notes Summary', name: 'deliverysummary', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Delivery Notes Summary', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.msg } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'inventory', fieldtype: 'addmorevalid', showCol: false, view: 'inventory', docDesign: 'pop', searchvalue: 'saleprice', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { label: 'Notes', name: 'Notes', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.information.regex, class: 'form-control', err: validation.information.msg } },
      ]
    },
  ]

}


//--------------------------p invoice-------------------------


export var sale_invoice = {
  title: 'Sale Invoice',
  type: null,
  table: {
    docDesign: 'pos',
    view: 'saleInvoice',
    status:{
      type:'Receiveable',
      success:'Received'
    },
    export: true,
    colView: true,
    gridView: true,
    refresh: true,
    search: true,
    dateRange: true,
    settings: true,
    options: {
      display: true,
      rowView: true,
      rowDelete: true,
      rowUpdate: true,
      send: true,
      
    },
  },
  numrow: [10, 25, 100],
  rows: [
    {
      col: [
        // { class: 'col-md-4', data: { label: 'Sale Order #', name: 'sale_Order', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'SOXXXXX', value: '', valid: '', ragexp: validation.saleorder.regex, class: 'form-control', err: validation.saleorder.msg } },
        { class: 'col-md-4', data: { label: 'Sale Order #', name: 'reference_order', fieldtype: 'refnum', showCol: true, value: '',valid:'', prefix: 'SO',view:'saleorder',docDesign:'pos',searched:'reference_order'} },
        // { class: 'col-md-4 col-md-offset-4', data: { label: 'Sale Invoice #', name: 'reference', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'SIXXXXXX', value: '', valid: '', ragexp: validation.saleinvoice.regex, class: 'form-control', err: validation.saleinvoice.msg } },
        { class: 'col-md-4 col-md-offset-4', data: { label: 'Sale Invoice #', name: 'reference', fieldtype: 'auto', showCol: false, prefix: 'SI', value: ''} }
        
      ]
    },
    // {
    //   col: [
    //     { class: 'col-md-4 ', data: { label: 'Sale Order #', name: 'reference', fieldtype: 'auto', showCol: false, prefix: 'SO', value: ''} }
    //   ]
    // },
    {
      col: [
        { class: 'col-md-4', data: { label: 'Invoice Date', options: date, enable: 'now', class: 'datetimepicker', placeholder: 'Invoice date here', name: 'invoiceDate', fieldtype: 'datepicker', showCol: true, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
        { class: 'col-md-4 col-md-offset-4', data: { label: 'Due Date', options: date, enable: 'after', class: 'datetimepicker', placeholder: 'Due Date', name: 'DueDate', fieldtype: 'datepicker', showCol: false, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'Customer', fieldtype: 'nameresult', showCol: false, view: 'customer', docDesign: 'pos', searchvalue: 'name', resultvalue: 'address', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Description', name: 'description', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Description', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.msg } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'inventory', fieldtype: 'addmorevalid', showCol: false, view: 'inventory', docDesign: 'pop', searchvalue: 'saleprice', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { label: 'Internal Information', name: 'InternalInformation', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.information.regex, class: 'form-control', err: validation.information.msg } },
      ]
    },
  ]

}


//-----------------------------s order------------------------

export var sale_order = {
  title: 'Sale Order',
  type: null,
  table: {
    docDesign: 'pos',
    view: 'saleorder',
    export: false,
    colView: false,
    gridView: false,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: false,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [

    {
      col: [
        { class: 'col-md-4 ', data: { label: 'Sale Order #', name: 'reference_order', fieldtype: 'auto', showCol: false, prefix: 'SO', value: ''} }
      ]
    },
    {
      col: [
        { class: 'col-md-4', data: { label: 'Issue Date', options: date, enable: 'before', class: 'datetimepicker', placeholder: 'Issue date here', name: 'issueDate', fieldtype: 'datepicker', showCol: true, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
        { class: 'col-md-4 col-md-offset-4', data: { label: 'Delivery Date', options: date, enable: 'after', class: 'datetimepicker', placeholder: 'Delivery Date', name: 'DeliveryDate', fieldtype: 'datepicker', showCol: false, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'Customer', fieldtype: 'nameresult', showCol: false, view: 'customer', docDesign: 'pos', searchvalue: 'name', resultvalue: 'address', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Description', name: 'description', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Description', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.regex } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'inventory', fieldtype: 'addmorevalid', showCol: false, view: 'inventory', docDesign: 'pop', searchvalue: 'saleprice', value: '', valid: null } },
      ]
    },

    {
      col: [
        { class: 'col-md-12', data: { label: 'Delivery Address', name: 'deliveryaddress', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.address.regex, class: 'form-control', err: validation.address.msg } },
      ]
    },

    {
      col: [
        { class: 'col-md-12', data: { label: 'Delivery Instructions', name: 'deliveryinstructions', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.msg } },
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Authorised By', name: 'authorisedby', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Authorised By', value: '', valid: '', ragexp: validation.fullname.regex, class: 'form-control', err: validation.fullname.msg } },
      ]
    },


  ]
}

//-----------------------------s quote------------------------

export var sale_quote = {
  title: 'Sale Quote',
  type: null,
  table: {
    docDesign: 'pos',
    view: 'salequote',
    export: true,
    colView: true,
    gridView: true,
    refresh: true,
    search: true,
    dateRange: true,
    settings: false,
    options: {
      display: true,
      rowView: true,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [

    {
      col: [
        { class: 'col-md-4 ', data: { label: 'Sale Quote #', name: 'reference', fieldtype: 'auto', showCol: false, prefix: 'SQ', value: ''}},
        
        // { class: 'col-md-4 ', data: { label: 'Sale Quote #', name: 'reference', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'SQXXXXXX', value: '', valid: '', ragexp: validation.salequote.regex, class: 'form-control', err: validation.salequote.msg } }
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Heading', name: 'heading', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Heading', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.msg } },
      ]
    },
    {
      col: [
        { class: 'col-md-4', data: { label: 'Issue Date', options: date, enable: 'after', class: 'datetimepicker', placeholder: 'Issue date here', name: 'issueDate', fieldtype: 'datepicker', showCol: true, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
        // { class: 'col-md-4 col-md-offset-4', data: { label: 'Delivery Date', options: date,enable:'after', class: 'datetimepicker', placeholder: 'Delivery Date', name: 'DeliveryDate', fieldtype: 'datepicker', showCol: false, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'Customer', fieldtype: 'nameresult', showCol: false, view: 'customer', docDesign: 'pos', searchvalue: 'name', resultvalue: 'address', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Quote Summary', name: 'quotesummary', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Quote Summary', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.regex } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'inventory', fieldtype: 'addmorevalid', showCol: false, view: 'inventory', docDesign: 'pop', searchvalue: 'saleprice', value: '', valid: null } },
      ]
    },

    // {
    //   col: [
    //     { class: 'col-md-12', data: { label: 'Delivery Address', name: 'deliveryaddress', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.address.regex, class: 'form-control', err: validation.address.msg } },
    //   ]
    // },

    {
      col: [
        { class: 'col-md-12', data: { label: 'Notes', name: 'notes', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your Notes', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.msg } },
      ]
    },
    // {
    //   col: [
    //     { class: 'col-md-6', data: { label: 'Authorised By', name: 'authorisedby', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Authorised By', value: '', valid: '', ragexp: validation.fullname.regex, class: 'form-control', err: validation.fullname.regex } },
    //   ]
    // },


  ]
}