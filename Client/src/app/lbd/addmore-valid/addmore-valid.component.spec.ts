import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmoreValidComponent } from './addmore-valid.component';

describe('AddmoreValidComponent', () => {
  let component: AddmoreValidComponent;
  let fixture: ComponentFixture<AddmoreValidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmoreValidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmoreValidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
