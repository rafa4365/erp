import { Component, Input } from '@angular/core';
import {  NavItem, NavItemType } from '../services/interface';
import { T2fschemaService } from '../services/t2fschema.service';

declare var $
export type BackgroundColor = 'blue' | 'azure' | 'green' | 'orange' | 'red' | 'purple';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  selected :any;
  subselected :any;
      
      @Input()
      public schema: any;

  @Input()
  public headerTextFull: string;

  @Input()
  public headerTextMini: string;

  @Input()
  public headerLink: string;

  @Input()
  public headerLogoImg: string;

  @Input()
  public logoDropdown: object;

  @Input()
  public backgroundColor: BackgroundColor;

  @Input()
  public backgroundImg: string;

  @Input()
  public navItems: NavItem[];


  constructor(
    private t2f: T2fschemaService
    
  ) { 
  }

  public get backgroundStyle(): { [id: string]: string; } {
    return { 'background-image': `url(${this.backgroundImg})` };
  }

  public get sidebarItems(): NavItem[] {
    return this.navItems.filter(i => i.type === NavItemType.Sidebar);
  }

  public get navbarItems(): NavItem[] {
    return this.navItems.filter(i => i.type === NavItemType.NavbarLeft || i.type === NavItemType.NavbarRight);
  }
//   select(item) {
//     this.selected = item; 
// };
// isActive(item) {
//     return this.selected === item;
// };
// activateClass(subModule){
//   subModule.active = !subModule.active;    
// }
select(item){
  this.selected = (this.selected === item ? null : item);
}
subselect(item){
  this.subselected = (this.subselected === item ? null : item);
}
isActive(item){
 return this.selected === item;
}
subisActive(item){
  return this.subselected === item;
 }
}
 