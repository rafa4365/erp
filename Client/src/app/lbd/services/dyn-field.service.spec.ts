import { TestBed, inject } from '@angular/core/testing';

import { DynFieldService } from './dyn-field.service';

describe('DynFieldService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DynFieldService]
    });
  });

  it('should be created', inject([DynFieldService], (service: DynFieldService) => {
    expect(service).toBeTruthy();
  }));
});
