import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';


@Component({
  selector: 'app-addmorestatic',
  templateUrl: './addmorestatic.component.html',
  styleUrls: ['./addmorestatic.component.scss']
})
export class AddmorestaticComponent implements OnInit {
  @Input()  public schema: any;
  @Input()  public updateValue: any;

  @Output() notify= new EventEmitter<any>();

  addmorearray=[] ;
  total=0;

  constructor(    private db:PouchbService  ) { }
  data(){
    console.log('data',this.addmorearray)
  }
  ngOnInit() {
    localStorage.setItem("addmore", JSON.stringify(this.schema.schema[0]));
    this.addmorearray.push(JSON.parse(localStorage.getItem('addmore')))
    
    // console.log("nested array before update",this.updateValue)
    if(this.updateValue){

      this.addmorearray=this.updateValue
      console.log("add more component for update value",this.updateValue)
    }
    
  }
  
  more(){
    this.addmorearray.push(JSON.parse(localStorage.getItem('addmore')))
  }

  delete(i){
    this.addmorearray.splice(i,1)
    console.log('delete form t-form',i)
  }
  addmoreselect2(value,ia){
    var view='inventory';
    this.addmorearray[value.name]=value.value;
    console.log('addmorearray',this.addmorearray) 
    console.log('select2 values',value) 
    this.db.addmorestatic(value,view).then((d:any)=>{
      console.log('select2 db values searched',d,ia)
  // this.addmorearray[ia][1].value='d.descrpition'
    });
    
  }
  searchselect(obj){
    // if(obj.value!==''){
    //   console.log('parent addmorestatic searchselect value emited',obj,this.table.view)
    //   this.db.dbsearchselect(obj).then((result:any)=>{
      console.log('parent addmorestatic dbsearchselect value promised',obj)
      this.addmorearray[obj.index][1].value=obj.result.descrpition
      this.addmorearray[obj.index][3].value=+obj.result.costPrice
      // this.addmorearray[obj.index][1].value=obj.result.descrpition
      if(obj.value===''){
        this.addmorearray[obj.index][1].value=''
        this.addmorearray[obj.index][3].value=null
      }

    // });
    // }
  }

  getValue(i2,j2){
    // this.addmorearray[j]['amount']=this.addmorearray[j]['qty']*this.addmorearray[j]['unitprice']
    // console.log("get value function", this.addmorearray)
    // console.log("get value exp", this.addmorearray[i])
    var obj={
      name: this.schema.name,
      value:this.addmorearray
    }

    var  addMoreArray:Array<Object>=[]
    // console.log('pooooooooooooo',obj)
        // this.formValues[obj.name] = obj.value;
        
        for(var i=0; i<this.addmorearray.length;i++){
          var arrayobj={};
        for(var j=0;j<this.addmorearray[i].length;j++){      
      arrayobj[obj.value[i][j].name]= obj.value[i][j].value
    }
    addMoreArray.push(arrayobj)
  }
      var form={
        name: this.schema.name,
        value:addMoreArray
      }
    this.notify.emit(form)

  }
  ngDoCheck() {
    // console.log('changed values of addmorearray array',this.addmorearray)
    var t=0;
    var dis=0;
    for(var i=0;i<this.addmorearray.length;i++){
      // console.log('onchange loop discription',this.addmorearray[i][1].value)
      // console.log('onchange loop unit qty',this.addmorearray[i][2].value)
      // console.log('onchange loop unit price',this.addmorearray[i][3].value)
      // console.log('onchange loop discount',this.addmorearray[i][4].value)
      // console.log('onchange loop amount',this.addmorearray[i][5].value)
      this.addmorearray[i][5].value=+this.addmorearray[i][2].value*this.addmorearray[i][3].value;
      dis=+this.addmorearray[i][5].value*(this.addmorearray[i][4].value/100)
      this.addmorearray[i][5].value=+this.addmorearray[i][5].value-dis
      // this.addmorearray[i][5].value=+(this.addmorearray[i][4].value/this.addmorearray[i][5].value)*100;
      t+=this.addmorearray[i][5].value
    }
    this.total=t
  }
}