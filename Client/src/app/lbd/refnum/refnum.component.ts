import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';
import { T2fschemaService } from '../../lbd/services/t2fschema.service';

declare var $;

@Component({
  selector: 'app-refnum',
  templateUrl: './refnum.component.html',
  styleUrls: ['./refnum.component.scss']
})
export class RefnumComponent implements OnInit {
  @Input() schema:any;
  @Input() update:any;

  @Output() notify=new EventEmitter<any>();
select2data:any=[];
select2fecheddata:any=[];
firstoption='';
result;
disableRef:boolean=false;

constructor(
  private t2f: T2fschemaService,  
  private db: PouchbService
  ) { }

  ngOnInit() {
    var obj={
      view:this.schema.view,
      docDesign:this.schema.docDesign
    }
    this.db.fetchData('Showing All',obj).then((r:any)=>{
      for(var i=0;i< r.rows.length;i++){
        this.select2data[i]={
          id:i,
          text:r.rows[i].doc[this.schema.searched]
        }
        this.select2fecheddata[i]=r.rows[i].doc
      }
      console.log('fetch result ',this.select2data)
      $(".ref").select2({
        placeholder: "Select",
        allowClear: true,
        
        data: this.select2data
      })
    })
    $(".ref").select2({
      placeholder: "Select",
      allowClear: true,
      
      data: this.select2data
    })

$(".ref").on('select2:select',  (e)=> {
  // Do something
  console.log('select2 value',e.target.value)
  console.log('select2 select2fecheddata',this.select2fecheddata[e.target.value])
  this.notify.emit(this.select2fecheddata[e.target.value])
  this.result=e.target.value;

 
  
  // var obj={
  //   [this.schema.searchvalue]:e.params.data.text,
  //   [this.schema.resultvalue]:e.params.data.id,
  // }
  // this.notify.emit(obj);
  
});
this.updatevalues(this.update)

  }
  updatevalues(value){
    console.log('refnum update value >>>>>>>>>>',value)
    if(value){
      this.disableRef=true
      this.result=value
      this.firstoption=value
    }
  }
}
