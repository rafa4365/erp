import { Component, OnInit } from '@angular/core';
import { NavItem } from '../../lbd/services/interface';
import { AuthLoginService } from '../../lbd/services/auth-login.service';
import { sidebarData,navItems } from '../admin-schema';


@Component({
  selector: 'app-admin-main',
  templateUrl: './admin-main.component.html',
  styleUrls: ['./admin-main.component.scss']
})
export class AdminMainComponent implements OnInit {

  constructor(private loginService: AuthLoginService) { 
    Object.assign(this, {sidebarData, navItems})
  }

  ngOnInit() {}
  
}
