import { Component, OnInit ,AfterContentInit ,Input} from '@angular/core';
declare var $;
@Component({
  selector: 'app-data-range-picker',
  templateUrl: './data-range-picker.component.html',
  styleUrls: ['./data-range-picker.component.scss']
})
export class DataRangePickerComponent implements OnInit {
  @Input() public options:any;
static currentId=1;
public dpId;

  constructor() { }

  ngOnInit() {
this.dpId=`datarangepicker${DataRangePickerComponent.currentId++}`
this.dpId=`demo`
  }


  ngAfterContentInit(){
  $('#demo').daterangepicker(this.options, function(start, end, label) {
  console.log(`New date range selected:  ${start.format('YYYY-MM-DD')}  to  ${end.format('YYYY-MM-DD')}   (predefined range:   ${label})`);
});
  }
}
