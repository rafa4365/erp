import { Component, OnInit ,AfterViewInit} from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';
import {NotificationService, NotificationType, NotificationOptions}  from '../../lbd/services/notification.service';

declare var $;
declare var $tr;
@Component({
  selector: 'app-admin-permission',
  templateUrl: './admin-permission.component.html',
  styleUrls: ['./admin-permission.component.scss']
})
export class AdminPermissionComponent implements OnInit {
ddoc={
  docDesign: 'admin',
  view: 'registration'
}
data=[];
  constructor(
    private db: PouchbService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.db.fetchData('Showing All',this.ddoc).then((a)=>{
      console.log('permition data',a.rows);
      var j = 0; 
      for(var i=0;i<a.rows.length;i++){
        if(a.rows[i].doc.allowed===false){
          this.data[j]=a.rows[i].doc;
          j++
        }
      }
      console.log('permition this.data',this.data);
      
    })
  }
  remove(value,i){
    this.showNotification('bottom','center',1,`${value.firstName} is now blocked`,'pe-7s-close')
    this.db.remove(value);
    this.data.splice(i,1)
  }
  allow(value,i){
    this.showNotification('bottom','center',1,`${value.firstName} is now part of your company`,'pe-7s-close')
    value['allowed'] = true;
    value['_id'] = value._id;
    value['_rev'] = value._rev;
    value['view'] = this.ddoc.view;
    this.db.query(value);
    this.data.splice(i,1)
  }
ngAfterViewInit(){
  setTimeout(()=>{ 

    
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
          ],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
        }

      });


      var table = $('#datatables').DataTable();

  });
}, 3000);
}
public showNotification(from: string, align: string,type: NotificationType,message:any,icon:any) {
  this.notificationService.notify(new NotificationOptions({
    message: message,
    icon: icon,
    type: <NotificationType>(type),
    from: from,
    align: align
  }));
}  
}
