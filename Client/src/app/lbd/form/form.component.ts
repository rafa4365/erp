import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { PouchbService } from '../../lbd/services/pouchb.service';
declare var UIkit;
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  Form:FormGroup;
  createForm:any;
  updategroup:FormGroup;

  getDate:any
  getOption:any;
  // FormData:any;
  @Output() csubmit = new EventEmitter<any>();
  @Output() usubmit = new EventEmitter<any>();
  @Input()  public formId: any;
  @Input()  public formType: any;
  @Input()  public schema: any;
  @Input()  public updateData: any; 
   formProperties:object={};
   array=[];
   number;
  constructor(private fb: FormBuilder,private db:PouchbService) { }
  
  ngOnInit() {
    this.number=1;
    this.array.push(this.number);
    console.log('form component schema',this.schema)
    console.log('form component update data',this.updateData)
    // this.Form= this.fb.group(this.formdata.formVar);
    var updateProperties:object={};

    if(this.formType==='Create'){
      for(var i =0;i<this.schema.form.length;i++){
        this.formProperties[this.schema.form[i].name]=this.schema.form[i].property;
      }
      this.Form= this.fb.group(this.formProperties);
      // console.log()
    }
    if(this.formType==='Update'){ 
      for(var i =0;i<this.schema.form.length;i++){
        updateProperties[this.schema.form[i].name]=this.schema.form[i].property[0]=this.updateData.rowData[this.schema.form[i].name];
      }
      updateProperties['_id']=this.updateData.rowData['_id'];
      updateProperties['_rev']=this.updateData.rowData['_rev'];
      this.updategroup= this.fb.group(updateProperties);
    }
    
  }

  dateValue(value){

          // this.formProperties[this.schema.form[i].name]=value;

  }
  more(){
    this.array.push(this.number);
    this.number++;
  }
  d(i){
    
    this.array.splice(i,1)
    // UIkit.modal('#modal-sections', {escclose: false, bg_close: false, keyboard:false, stack:true}).show();
    // this.array.pop()
    // delete this.array[i]
    console.log('delete form t-form',i)
  }

  selectValue(value){
    console.log("select value funcion ffffffffffffffffffffffffffffffff",value)
      // this.getOption= value;
      if(this.formType==='Create'){
        for(var i =0;i<this.schema.form.length;i++){
          // formProperties[this.schema.form[i].name]=this.schema.form[i].property;
          if(this.schema.form[i].name==='color'){
            this.formProperties[this.schema.form[i].name]=value;
          }
        }
        this.Form= this.fb.group(this.formProperties);
      // this.getDate=value;
      }
  }
  
 
  submit(values){
    this.Form.reset();
    values._id= Date.now().toString();
    values.view=this.schema.table.view

  //   if(this.getDate != null){
  //     values.date = this.getDate;
  //   }

  //   if(this.getOption != null){
  //      values.language =this.getOption
  //   }

  //   if(this.getOption != null){
  //     values.color =this.getOption
  //  }


    // this.csubmit.emit(values);
    console.log('submit function --------!',values)
    
    // this.db.query(values);
  }

  updateQuery(values,formId){
    // this.dataArray.splice(i,1,values)
    // this.usubmit.emit({values,formId});
    
    values.view=this.schema.table.view
    console.log('updateQuery function..............................>!',values,formId)
    this.db.update(values);
  }
}
