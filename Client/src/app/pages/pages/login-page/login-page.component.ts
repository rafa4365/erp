import { Component, OnInit , trigger, state, style, transition, animate} from '@angular/core';
import { Router } from '@angular/router';
import { AuthLoginService } from '../../../lbd/services/auth-login.service';
import { PouchbService } from '../../../lbd/services/pouchb.service';
import { validation } from '../../../validation';

import { NotificationService, NotificationType, NotificationOptions } from '../../../lbd/services/notification.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  animations: [
    trigger('carduserprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out'),
      ])
    ]),
    trigger('cardprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1})),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0.25s ease-out')
      ])
    ])
  ]
})
export class LoginPageComponent implements OnInit {
  username='';
  password='';
loginbtn=true
usernamevalid;
passwordvalid;
uclass='form-control form-control-sm';
pclass='form-control form-control-sm';
uerr=validation.username.msg;
perr=validation.password.msg;
  constructor(
    private router: Router, 
    private loginService: AuthLoginService,
    private db: PouchbService,
    // private _location: Location,
    private notificationService: NotificationService,
  ) { }

  ngOnInit() {

  }
  login() {
    console.log(this.username, this.password)
    this.db.login(this.username,this.password,'registration').then((a)=>{
      console.log('condition return',a)
      if(a===true){
        this.showNotification('top', 'center', 2, `Wellcome`, 'pe-7s-user')
      }else if(a===false){
        this.showNotification('top', 'center', 4, `You are not allowed`, 'pe-7s-close')
      }
    })
    if (this.username === 'hrm' && this.password === '19921982!Ab') {
      this.showNotification('top', 'center', 3, `it is for testing only`, 'pe-7s-close')
      
      this.loginService.setLoggedIn();
      this.router.navigate(['/hrMain']);
    }
    if (this.username === 'pop' && this.password === '19921982!Ab') {
      this.showNotification('top', 'center', 3, `it is for testing only`, 'pe-7s-close')
      
      this.loginService.setLoggedIn();
      this.router.navigateByUrl('/popMain');
    }
    if (this.username === 'pos' && this.password === '19921982!Ab') {
      this.showNotification('top', 'center', 3, `it is for testing only`, 'pe-7s-close')
      
      this.loginService.setLoggedIn();
      this.router.navigateByUrl('/posMain');
    }
    if (this.username === 'tem' && this.password === '19921982!Ab') {
      this.showNotification('top', 'center', 3, `it is for testing only`, 'pe-7s-close')
      
      this.loginService.setLoggedIn();
      this.router.navigateByUrl('/holder');

    }
    if (this.username === 'database' && this.password === '19921982!Ab') {
      // this.router.navigateByUrl('/hrMain');    
      window.location.href = 'http://localhost:3000/_utils/#/_all_dbs';
    }
    if (this.username === 'admin' && this.password === '19921982!Ab') {
      this.showNotification('top', 'center', 3, `it is for testing only`, 'pe-7s-close')
      
      this.loginService.setLoggedIn();
      this.router.navigate(['/adminMain']);
    }
  }
  validate(val){
    var re_user= new RegExp(validation.username.regex)
    var re_pass=new RegExp(validation.password.regex)
if(val==='u'){
  this.usernamevalid=false;
  
  if(re_user.test(this.username) && this.username!==''){
    this.uclass='form-control form-control-sm valid';
    this.usernamevalid=true;
    this.loginbtn=false    
  }      
  if(!re_user.test(this.username) && this.username==''){
    this.uclass='form-control form-control-sm invalid';
    this.usernamevalid=false;
    this.loginbtn=true 
  }
}
if(val==='p'){
  this.passwordvalid=false;
  
if( re_pass.test(this.password) && this.password!==''){
  this.pclass='form-control form-control-sm valid';
  this.passwordvalid=true;
  this.loginbtn=false    
}      
if(!re_pass.test(this.password) && this.password==''){
  this.pclass='form-control form-control-sm invalid';
  this.passwordvalid=false;
  this.loginbtn=true 
} 
} 
  }
  public showNotification(from: string, align: string, type: NotificationType, message: any, icon: any) {
    this.notificationService.notify(new NotificationOptions({
      message: message,
      icon: icon,
      type: <NotificationType>(type),
      from: from,
      align: align
    }));
  }
}
