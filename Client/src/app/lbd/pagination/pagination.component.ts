import { Component, OnInit,Input , AfterViewInit,Output,EventEmitter} from '@angular/core';
 declare var $:any;
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {


  @Output() pageNum =new EventEmitter<any>();
  
  @Input()
  public totalObjects:number;

  @Input()
  public pageLimit: any;
  pagestate=[];
  pages: any
  totalPages:any=[];
  currentpage:any;
  pagemem:any;
  pagearray=[];
  constructor() { }
ngAfterViewInit(){
  $('#paginate').pagination({
    dataSource:  this.pagearray,
    pageSize: this.pageLimit,
    pageRange: 1,
    totalNumber: 44,
    showPageNumbers: true,
    showGoInput: true,
    showGoButton: false,
    activeClassName:'active', 
    className:'pull-right pagination',
    disableClassName:'disabled',
    ulClassName:'pagination',
    showNavigator: true,
    beforePageOnClick:(data)=>{
      this.pageNum.emit(parseInt(data.target.innerText));
    },
    beforeNextOnClick:(data)=>{
      this.pageNum.emit('next');
    },
    beforePreviousOnClick:(data)=>{
      this.pageNum.emit('pre');
    },
    beforeGoInputOnEnter:(data)=>{
      this.pageNum.emit(data.currentTarget.value);
    }
})
}
  ngOnInit() {
  
    for(var i =0;i<this.totalObjects;i++){
      this.pagearray.push(i)
    // console.log("page array push elements-----------------",this.pagearray)
      
    }
//------------------------total no of pages--------------
    //console.log("no of rows-----------------",this.pages = (this.totalObjects / this.pageLimit))
    this.pages=Math.ceil(this.pages)
    //console.log("no of rows-----------------",this.pages)
    for(var i=0;i<= this.pages;i++){
       this.totalPages[i]=i+1
    }

//--------------intialization of pagenation-----------
this.currentpage=1;

    if(this.totalPages.length > 10){
      this.totalPages.splice(5, 0, '...');
      this.totalPages.splice(6, this.totalPages.length-7);
    }
  }
//------------------ function of page number-----------------------

  pageNumber(value){
    this.currentpage=value;

    // var obj = this.pagestate.filter(function (obj) {
    //   //console.log('rawD',obj)
    //   return obj=== value;
    // })[0];
    // if(obj){
    //   //console.log('not intializing',this.pagestate,obj)
    // }else{
    //   this.pagestate.push(value)
    //   //console.log(' intializing',this.pagestate,obj)
      
    // }
    if(this.pagestate[1]!== value){
      this.pagestate.push(value)
        // //console.log(' intializing',this.pagestate)
    }else{
      // //console.log("not initiallizing",this.pagestate[1]!== value)
      // //console.log("not initiallizing",this.pagestate[1]!= value)
    }
    this.pagestate.splice(0,this.pagestate.length-2)
    // //console.log("pagestate splice(0)",this.pagestate)
    

    // this.pagestate.push(value)
    // //console.log("pagestate push(0)",this.pagestate)
    
    
        // this.pagestate.push(1)
        // //console.log("pagestate push(1)",this.pagestate)

        // //console.log("pagestate length",)
    
    
    // if(this.pagestate[this.pagestate.length-1]=!value){
    // //console.log("condition check",this.pagestate[this.pagestate.length-1])
      
    //   this.pagestate.push(value)
    // //console.log(" if pagestate push(values)",this.pagestate)
    
    // }

    // //console.log("pagestate push(values)",this.pagestate)
    
    
    // //console.log("current page -----------------",this.currentpage)
    // //console.log("page state -----------------",this.pagestate)
    
//------------------- button changes page number----------------

    if(this.currentpage>=5 && value < 16){

      //console.log('change pagination ')
      //console.log('start ',this.totalPages)
      if(this.pagestate[2]>this.pagestate[1]){
        this.totalPages.splice(1, 2,'...');
        this.totalPages.splice(4, 0,value+1);
        this.currentpage++      
        
      }
      if( this.pagestate[0]>this.pagestate[1]){
        this.totalPages.splice(1, 2,'...');
        //console.log("splice(1, 2,'...')",this.totalPages)
        this.totalPages.splice(2, 0,value-1);
        //console.log("splice(2, 1,value-1)",this.totalPages)        
        this.totalPages.splice(3, 1,value);
        //console.log("splice(3, 1,value-2)",this.totalPages)        
        this.totalPages.splice(4, 1,value+1);
        //console.log("splice(4, 1,value+1)",this.totalPages)  
        this.currentpage--      
      }
      if(this.pagestate[1]>this.pagestate[0] ){
        this.totalPages.splice(1, 2,'...');
        this.totalPages.splice(4, 0,value+1);
        this.currentpage++
      }
      if(value==5 || value==this.pages-4  ){
        this.totalPages.splice(1, 2,'...');
        this.totalPages.splice(4, 0,value+1);
        this.currentpage++
      }
      
      // // if(this.currentpage > value){
      // this.totalPages.splice(1, 2,'...');
      // //console.log( "1, 2,'...'",this.totalPages)
      // //   // this.totalPages.splice(4, 0,value-1);
      // //   //console.log( "4, 0,value-1",this.totalPages)
      // // }
      // // if(this.currentpage < value){
      // //   this.totalPages.splice(4, 0,value+1);
      // //   //console.log( "4, 0,value+1",this.totalPages)
      // // }
  
      // this.totalPages.splice(4, 0,value+1);
      // //console.log( "4, 0,value+1",this.totalPages)
      // // this.totalPages.splice(3, );
      // // this.totalPages.splice(4, 1);
      
      
      
    }
    if(value==this.pages-5){
      this.totalPages=[];
      this.totalPages.splice(0,1,1)
      this.totalPages.splice(1,1,'...')
      this.totalPages.splice(2,1,this.pages-5)
      this.totalPages.splice(3,1,this.pages-4)
      this.totalPages.splice(4,1,this.pages-3)
      this.totalPages.splice(5,1,this.pages-2)
      this.totalPages.splice(6,1,this.pages-1)
      this.totalPages.splice(7,1,this.pages)
      this.currentpage=this.pages-5;
    }

//--------------last page button-------------------
    if(value==this.pages){
      //console.log("this is last page -----------------",value)
      ////$(".page-next").addClass("disabled");
      //$(".page-last").addClass("disabled");
      this.totalPages=[];
      this.totalPages.splice(0,1,1)
      this.totalPages.splice(1,1,'...')
      this.totalPages.splice(2,1,this.pages-4)
      this.totalPages.splice(3,1,this.pages-3)
      this.totalPages.splice(4,1,this.pages-2)
      
      this.totalPages.splice(5,1,this.pages-1)
      this.totalPages.splice(6,1,this.pages)
      this.currentpage=this.pages;
      //$(".page-first").removeClass("disabled");
      //$(".page-pre").removeClass("disabled");
    }
//--------------first page button-------------------
    
    else if(value==1){
      //console.log("this is fist page -----------------",value)
      //$(".page-first").addClass("disabled");
      //$(".page-pre").addClass("disabled");
      this.totalPages=[];
      this.totalPages.splice(0,1,1)
      this.totalPages.splice(1,1,2)
      this.totalPages.splice(2,1,3)
      this.totalPages.splice(3,1,4)
      this.totalPages.splice(4,1,5)
      
      this.totalPages.splice(5,1,'...')
      this.totalPages.splice(6,1,this.pages)
      //$(".page-next").removeClass("disabled");
      //$(".page-last").removeClass("disabled");
    }
//--------------enlabling navgiation button ------------------
    
    else{
      //$(".page-next").removeClass("disabled");
      //$(".page-last").removeClass("disabled");
      //$(".page-first").removeClass("disabled");
      //$(".page-pre").removeClass("disabled");
      //console.log("page name Function -----------------",value)
    }
    
  }

//--------------nav buttons  functions of first last next prev-------------------

  navButton(value){
    
    //console.log(" top page Squence Function -----------------",value)
    //console.log(" top current page -----------------",this.currentpage)
    
    switch(value){

//--------------nav button switch condition of first -----------------------------
      
      case 'first':
      // //$(".page-first").addClass("active");
      // $("first").addClass("active");

      this.totalPages=[];
      this.totalPages.splice(0,1,1)
      this.totalPages.splice(1,1,2)
      this.totalPages.splice(2,1,3)
      this.totalPages.splice(3,1,4)
      this.totalPages.splice(4,1,5)
      
      this.totalPages.splice(5,1,'...')
      this.totalPages.splice(6,1,this.pages)
      this.currentpage=1;
      break
//--------------nav button switch condition of last -----------------------------

      case 'last':
      //$(".page-last").addClass("active");
      //$(".page-last").addClass("disabled");
      this.totalPages=[];
      this.totalPages.splice(0,1,1)
      this.totalPages.splice(1,1,'...')
      this.totalPages.splice(2,1,this.pages-4)
      this.totalPages.splice(3,1,this.pages-3)
      this.totalPages.splice(4,1,this.pages-2)
      
      this.totalPages.splice(5,1,this.pages-1)
      this.totalPages.splice(6,1,this.pages)
      this.currentpage=this.pages;
      //console.log( "1, 2,'...'",this.totalPages)
      break
//--------------nav button switch condition of next -----------------------------


      case 'next':

      if(this.currentpage>=5 && this.currentpage<=this.pages-5 ){
        //console.log("page Squence Function -----------------",this.currentpage)
        this.totalPages.splice(1, 2,'...'); 
        //console.log(" this.currentpage -----------------",this.currentpage)
        //console.log(" splice(1, 2,'...') -----------------",this.totalPages)
        
        this.totalPages.splice(4, 0,this.currentpage+2);
        //console.log("this.currentpage+2 -----------------",this.currentpage)
        //console.log("splice(4.0,'...') -----------------",this.totalPages)
        
        
        this.totalPages.splice(5, 0, '...');
        //console.log("splice(5,0,''); -----------------",this.currentpage)
        //console.log("splice(5,0,''); -----------------",this.totalPages)
        
        this.totalPages.splice(6, this.totalPages.length-7);
        //console.log("splice(6, this.totalPages.length-7)",this.currentpage)
        //console.log("splice(6, this.totalPages.length-7)",this.totalPages)
        
        this.currentpage++
        //console.log("outside if condition++ ",this.currentpage)
      }
      if(this.currentpage==this.pages-5){
        //console.log('this.currentpage>=15')
        // //$(".page-next").addClass("disabled");
        
        this.totalPages.splice(5, 1,this.currentpage+2);
        this.totalPages.splice(6, 0,this.currentpage+3);
        this.totalPages.splice(7, 0,this.currentpage+4);
        this.totalPages.splice(2, 2);
        // this.totalPages.splice(6, 0,this.currentpage+5);
        //console.log( "1, 2,'...'",this.totalPages)
      }
      if(this.currentpage>=1 && this.currentpage<5){
        this.currentpage++
      }
      if(this.currentpage>=this.pages-5 && this.currentpage<this.pages){
        this.currentpage++
      }
      break 

//--------------nav button switch condition of previous -----------------------------


      case 'pre':
      if(this.pages-6 && this.currentpage>=6  ){
       
        //console.log(" initial values of total pages -----------------",this.totalPages)
        //console.log("page Squence Function -----------------",this.currentpage)
      //------------------------ middle values exchange-------------------
        this.totalPages.splice(2, 1, this.currentpage-2);
        this.totalPages.splice(3, 1, this.currentpage-1); 
        this.totalPages.splice(4, 1, this.currentpage); 
        this.totalPages.splice(5, 1, '...'); 
        
        //console.log(" splice(1, 2,'...') -----------------",this.totalPages)
        
        this.currentpage--
         //console.log(" current page decrmrnt--",this.currentpage)

        } 
        if(this.currentpage>this.pages-6){
          this.totalPages=[];
          this.totalPages.splice(0,1,1)
          this.totalPages.splice(1,1,'...')
          this.totalPages.splice(2,1,this.pages-4)
          this.totalPages.splice(3,1,this.pages-3)
          this.totalPages.splice(4,1,this.pages-2)
          
          this.totalPages.splice(5,1,this.pages-1)
          this.totalPages.splice(6,1,this.pages)
          this.currentpage--
          
        }
  
       //-------------  previous first values exchange-----------------------------------
      if(this.currentpage<=5 && this.currentpage>1){
        this.totalPages=[];
        this.totalPages.splice(0,1,1)
        this.totalPages.splice(1,1,2)
        this.totalPages.splice(2,1,3)
        this.totalPages.splice(3,1,4)
        this.totalPages.splice(4,1,5)
        
        this.totalPages.splice(5,1,'...')
        this.totalPages.splice(6,1,this.pages)
        this.currentpage--
      }
      
      break

    }
    
  }

}
 