import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmorePayslipComponent } from './addmore-payslip.component';

describe('AddmorePayslipComponent', () => {
  let component: AddmorePayslipComponent;
  let fixture: ComponentFixture<AddmorePayslipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmorePayslipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmorePayslipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
