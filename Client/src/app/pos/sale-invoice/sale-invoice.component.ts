import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../lbd/services/script-loader.service';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';
import { sale_invoice } from '../pos-schema';

@Component({
  selector: 'app-sale-invoice',
  templateUrl: './sale-invoice.component.html',
  styleUrls: ['./sale-invoice.component.scss']
})
export class SaleInvoiceComponent implements OnInit {

  exp: any;

  date: any;
  inven: any;
  nameresult: any;

  constructor(
    private navbarTitleService: NavbarTilteService,
    private _script: ScriptLoaderService,
  ) {
    Object.assign(this, { sale_invoice })
  }

  ngOnInit() {
    this.navbarTitleService.updateTitle('Sale Invoice');

  }
  ngAfterViewInIt() {
    this._script.load('app-extended-forms',
      '../../assets/js/bootstrap-checkbox-radio-switch-tags.js',
      '../../assets/js/light-bootstrap-dashboard.js')
  }


}
