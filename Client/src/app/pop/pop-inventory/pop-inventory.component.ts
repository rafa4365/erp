import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../lbd/services/script-loader.service';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';
import { pop_inventory } from '../pop-schema';

@Component({
  selector: 'app-pop-inventory',
  templateUrl: './pop-inventory.component.html',
  styleUrls: ['./pop-inventory.component.scss']
})

export class PopInventoryComponent implements OnInit {

  constructor(
    private navbarTitleService: NavbarTilteService,
    private _script: ScriptLoaderService,
  ) {
    Object.assign(this, {pop_inventory})        
  }

  ngOnInit() {
    this.navbarTitleService.updateTitle('Pop Inventory');
  }

  ngAfterViewInIt() {
    this._script.load('app-extended-forms',
      '../../assets/js/bootstrap-checkbox-radio-switch-tags.js',
      '../../assets/js/light-bootstrap-dashboard.js')
  }
}
