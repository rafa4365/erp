import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HRSummaryComponent } from './hr-summary.component';

describe('HRSummaryComponent', () => {
  let component: HRSummaryComponent;
  let fixture: ComponentFixture<HRSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HRSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HRSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
