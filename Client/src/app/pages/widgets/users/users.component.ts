import { Component, OnInit,AfterViewInit } from '@angular/core';
import { NavbarTilteService } from '../../../lbd/services/navbar-tilte.service';
import {validForm} from '../widgets-schema';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  validForm:any;
  constructor(private navbarTitleService: NavbarTilteService) {
    Object.assign(this, {validForm})            
    
   }

  ngOnInit() {
    this.navbarTitleService.updateTitle('User Info');    
  }


}
 