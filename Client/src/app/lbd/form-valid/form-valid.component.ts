import { T2fschemaService } from '../../lbd/services/t2fschema.service';
import { Component, OnInit, Input,AfterViewInit, OnDestroy, ChangeDetectorRef , trigger, state, style, transition, animate} from '@angular/core';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';
import { Subscription } from 'rxjs/Subscription';
import { PouchbService } from '../../lbd/services/pouchb.service';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { ValidationService } from '../../lbd/services/validation.service';
import {NotificationService, NotificationType, NotificationOptions}  from '../../lbd/services/notification.service';

@Component({

  selector: 'app-form-valid',
  templateUrl: './form-valid.component.html',
  styleUrls: ['./form-valid.component.scss'],
  animations: [
    trigger('carduserprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out'),
      ])
    ]),
    trigger('cardprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1})),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0.25s ease-out')
      ])
    ])
  ]
})
export class FormValidComponent implements OnInit, OnDestroy {
  form: any;
  eq_data: any;
  data: any;
  subscription1: Subscription;
  subscription2: Subscription;
  updateValues: any;
  C_udata: any = []
  refnum:boolean=false;
  // refnumDisable:boolean=false;
  constructor(
    private t2f: T2fschemaService,
    private navbarTitleService: NavbarTilteService,
    private db: PouchbService,
    private cdr: ChangeDetectorRef,
    private _location: Location,
    private _valid: ValidationService,
    private notificationService: NotificationService,
    private router: Router) {
    this.subscription1 = this.t2f.myvalue$.subscribe(val => this.form = val);
    this.subscription2 = this.t2f.uvalue$.subscribe(val => this.updateValues = val);

    if (this.form === 'default message') {
      this._location.back();
    }
  }
 
  ngOnInit() {
    this.navbarTitleService.updateTitle('');
    this.form.submit = true;
    this.updateval(this.form.rows);
    console.log('form init this form', this.form);
  }
  Create() {
    this._valid.formdata().then((v) => {
      console.log('create form submitted v', v);
      this.data = v;
      this.data['_id'] = Date.now().toString();
      this.data['view'] = this.form.table.view;
      if(this.form.table.status){
      this.data['status'] ={
        type:this.form.table.status.type,
        color:'btn-warning',
        // info:this.form.table.status.type,
      }}
      if (this.data.inventory) {
        console.log("form valid data If condition check", this.data.inventory.total)

        this.data['total'] = this.data.inventory.total;
      }

      if (this.data.expense) {
        console.log("form valid data If condition check", this.data.expense.total)

        this.data['expenseTotal'] = this.data.expense.total;
      }


      this.db.query(this.data);
      this.showNotification('top','center',2,`Form ${this.form.title} is ${this.form.type}`,'pe-7s-check')
      this._location.back();
    })
  }
  Update() {
    this._valid.formdata().then((v) => {
      console.log('update form submitted v', v);
      console.log('update form submitted this.updateValues', this.updateValues);
      this.data = v;
      this.data['_id'] = this.updateValues._id;
      this.data['_rev'] = this.updateValues._rev;
      this.data['view'] = this.updateValues.view;
      this.data['edited'] = true;
      if(this.form.table.status){
        this.data['status'] ={
          type:this.form.table.status.type,
          color:'btn-warning',
          // info:this.form.table.status.type,
        }}
      if (this.data.inventory) {
        console.log("form valid data If condition check", this.data.inventory.total)

        this.data['total'] = this.data.inventory.total;
      }

      if (this.data.expense) {
        console.log("form valid data If condition check", this.data.expense.total)

        this.data['expenseTotal'] = this.data.expense.total;
      }

      this.db.query(this.data);
      this.showNotification('top','center',2,`Form ${this.form.title} is ${this.form.type}`,'pe-7s-check')
      
      this._location.back();
    })
  }

  refSelect(value){
    console.log('ref sale order form valid dddddddddddddd',value) 
    console.log('ref sale order form valid dddddddddddddd form',this.form) 
    var h = 0
    
    for(var i=0;i< this.form.rows.length;i++){
      for(var j=0;j< this.form.rows[i].col.length;j++){
        if(value[this.form.rows[i].col[j].data.name]!==undefined){
          this.form.rows[i].col[j].data.value=value[this.form.rows[i].col[j].data.name]
          this.form.rows[i].col[j].data.index = h++
          this.C_udata.push(this.form.rows[i].col[j])
          
          console.log('ref sale order form valid loooooooooooop form',value[this.form.rows[i].col[j].data.name],this.form.rows[i].col[j].data.value) 
        }
        this.toggleref(this.refnum);
      }        
    }

      console.log('ref sale order form valid data form',this.form.rows) 
      console.log('this.C_udata.push(this.form.rows[i].col[j])',this.C_udata) 
      // this.updatevalonref(this.form.rows,value);
    
  }
  back() { this._location.back(); }
  toggleref(value){
    setTimeout(()=>{
      this.refnum=!value
    }, 1000);
  }
  ngDoCheck() {
    this._valid.get(this.form)
  }
  updateval(value) {
    if (this.updateValues !== '') {
      // this.refnumDisable=true
      console.log("updateValues this.updateValues !== ''", this.updateValues,value)
      var i = 0
      value.forEach((element) => {
        element.col.forEach((element) => {
          if (element.data.fieldtype !== 'datepicker') {
            element.data.value = this.updateValues[element.data.name]
            element.data.index = i++
            this.C_udata.push(element)
          }            
          if (element.data.fieldtype === 'datepicker') {
            // element.data.class = 'datetimepicker';
            element.data.value = this.updateValues[element.data.name]
            element.data.index = i++
            this.C_udata.push(element)
            console.log("updateValues this.updateValues !== '' (element)", element)
            
          }            

        })
      })
    }
    if (this.updateValues === '') {
      console.log("updateValues this.updateValues === ''", this.updateValues)
      var i = 0
      value.forEach((element) => {
        element.col.forEach((element) => {
          if (element.data.fieldtype !== 'datepicker') {
            element.data.value = '';
            element.data.class = 'form-control';
          }
          if (element.data.fieldtype === 'datepicker') {
            element.data.class = 'datetimepicker';
            element.data.value = '';
            console.log("updateValues this.updateValues === '' (element)", element)
            
          }
        })
      })
    }
    console.log('updatemmmmmmmmmmmmmmm data form',value) 
    
  }
  updatevalonref(value,val) {
    if (val !== '') {
      console.log("updateValues !", val)
      var i = 0
      value.forEach((element) => {
        element.col.forEach((element) => {
          element.data.index = i++
          element.data.value = val[element.data.name]
          this.C_udata.push(element)
        })
      })
    }
    // if (val === '') {
    //   console.log("updateValues", val)
    //   var i = 0
    //   value.forEach((element) => {
    //     element.col.forEach((element) => {
    //       if (element.data.fieldtype !== 'datepicker') {
    //         element.data.value = '';
    //         element.data.class = 'form-control';
    //       }
    //       if (element.data.fieldtype === 'datepicker') {
    //         element.data.class = 'datetimepicker';
    //         element.data.value = '';
    //       }
    //     })
    //   })
    // }
    console.log('updatemmmmmmmmmmmmmmm data form',value) 
    console.log('this.C_udata',this.C_udata) 
    
  }
  ngOnDestroy() {
    this.subscription1.unsubscribe(); this.subscription2.unsubscribe();
  }
  public showNotification(from: string, align: string,type: NotificationType,message:any,icon:any) {
    this.notificationService.notify(new NotificationOptions({
      message: message,
      icon: icon,
      type: <NotificationType>(type),
      from: from,
      align: align
    }));
  }
}