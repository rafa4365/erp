import { Injectable } from '@angular/core';

@Injectable()
export class ValidationService {
  data:any={};
  check=0;
  count=0;
  form;
  b_state;
  constructor() { }

enablebutton(value){
  // console.log('check and count',this.check,'-',this.count)
  if(this.count===this.check){
    this.form.submit=false
}else{
this.form.submit=true
}
} 

get(value){
  this.data={}
 this.check=0
 this.count=0
 this.form=value

 value.rows.forEach((element)=>{
  this.count+=element.col.length;
 });
  value.rows.forEach((element)=>{
    // console.log('exp',this.count)
    element.col.forEach((element)=>{
      if(element.data.name){
        this.validate(element.data)
        this.transform(element.data)
      }
    })
  }) 
}
validate(value){
  var regex= new RegExp(value.ragexp) 
  if(value.ragexp){
    if(regex.test(value.value) && value.value!==''){
      value.class='form-control valid';
      value.valid=true;
      this.data[value.name]
      this.check++;
      this.enablebutton(this.form.submit)    
    }
  } 
  if(!value.ragexp && value.value!==''){
    this.data[value.name]
    this.check++;
    this.enablebutton(this.form.submit) 
  }
  if(!value.ragexp && value.valid===false){
    this.check--;
    this.enablebutton(this.form.submit) 
  }
  if(!regex.test(value.value) && value.value!==''){
    value.class='form-control invalid';
    value.valid=false;
    this.form.submit=true
  }

}
transform(value){
  if(value.fieldtype){
    this.data[value.name]=value.value    
  }

}
formdata(): Promise<any>{
  return new Promise((resolve, reject) => {
    console.log('form validation service this data',this.data);
    resolve(this.data)     
  });
}
}
