import { TestBed, inject } from '@angular/core/testing';

import { PouchbService } from './pouchb.service';

describe('PouchbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PouchbService]
    });
  });

  it('should be created', inject([PouchbService], (service: PouchbService) => {
    expect(service).toBeTruthy();
  }));
});
