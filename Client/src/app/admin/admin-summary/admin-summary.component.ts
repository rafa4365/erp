import { Component, OnInit } from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';


@Component({
  selector: 'app-admin-summary',
  templateUrl: './admin-summary.component.html',
  styleUrls: ['./admin-summary.component.scss']
})
export class AdminSummaryComponent implements OnInit {

  customerSummary: any;
  supplierSummary: any;
  expenseSummary: any;

  chartData: any = [];
  view: any[] = [600, 300];

  colorScheme = {
    domain: [
      '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
      '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
    ]
  };




  constructor(private db: PouchbService, ) {
  }

  onSelect(event) {
    console.log(event);
  }

  ngOnInit() {

    //------------------ total of sale parts--------
    var mapReduceFunCustomer = {
      map: function (doc, emit) {
        if (doc.view === "saleInvoice") {
          emit(doc.Customer.name, doc.total)
        }
      },
      reduce: '_stats'
    };
    let optionsCustomer: Object = {

      reduce: true,
      group: true,
    };

    this.db.summaryFun(mapReduceFunCustomer, optionsCustomer).then((sum: any) => {
      console.log("------Customer function summary ts--------", this.customerSummary = sum)
      var totalPrice = 0;
      var dataArray = [];

      for (var i = 0; i < this.customerSummary.length; i++) {
        dataArray[i] =
          {
            name: this.customerSummary[i].key,
            value: this.customerSummary[i].value.sum
          }
        totalPrice += this.customerSummary[i].value.sum;
      }
      console.log("total price", totalPrice);

      this.customerSummary.totalPrice = totalPrice;
      this.chartData.push({
        name: 'TotalSales',
        value: totalPrice
      })
      console.log("chartdata of sales", this.chartData);

    });

    //----------------------- total of purchase and Expense part--------------

    //---------------Supplier Summary-------------

    var mapReduceFunSupplier = {
      map: function (doc, emit) {
        if (doc.view === "purchaseInvoice") {
          emit(doc.Supplier.name, doc.total)
        }
      },
      reduce: '_stats'
    };
    let optionsSupplier: Object = {

      reduce: true,
      group: true,
    };

    this.db.summaryFun(mapReduceFunSupplier, optionsSupplier).then((sum: any) => {
      console.log("------Supplier function summary ts--------", this.supplierSummary = sum)
      var totalPrice = 0;
      var dataArray = [];

      for (var i = 0; i < this.supplierSummary.length; i++) {
        // dataArray[i] =
        //   {
        //     name: this.supplierSummary[i].key,
        //     value: this.supplierSummary[i].value.sum
        //   }
        totalPrice += this.supplierSummary[i].value.sum;
      }
      console.log("total price", totalPrice);
      console.log("single object", dataArray);

      this.supplierSummary.totalPrice = totalPrice;
      this.chartData.push({
        name: 'TotalPurchase',
        value: totalPrice
      })
      console.log("chartdata of Purchases", this.chartData);

    });

    //---------------------Expense Summary----------

    var mapReduceFunExpense = {
      map: function (doc, emit) {
        if (doc.view === "expense") {
          for (var i = 0; i < doc.expense.length; i++) {
            emit(doc.expense[i].name, parseInt(doc.expense[i].amount))
          }
        }
      },
      reduce: '_stats'
    };

    let optionsExpense: Object = {

      reduce: true,
      group: true,
    };
    this.db.summaryFun(mapReduceFunExpense, optionsExpense).then((sum: any) => {
      console.log("@@@@@@@@@@@@@@@@----expense Result", this.expenseSummary = sum);
      var totalPrice = 0;
      var dataArray = [];

      for (var i = 0; i < this.expenseSummary.length; i++) {
        dataArray[i] =
          {
            name: this.expenseSummary[i].key,
            value: this.expenseSummary[i].value.sum
          }
        totalPrice += this.expenseSummary[i].value.sum;
      }
      console.log("supplier total", totalPrice);
      console.log("single object", dataArray);

      this.expenseSummary.totalPrice = totalPrice;
      // this.expenseChartData = dataArray;
      this.chartData.push({
        name: 'TotalExpense',
        value: totalPrice
      })
      console.log("chartdata of expenses111111111", this.chartData);
      // this.chartData.push({
      //   name: 'NetprofitLoss',
      //   value: totalPrice
      // })
    });


  }

}
