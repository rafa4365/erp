import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmoreExpenseValidComponent } from './addmore-expense-valid.component';

describe('AddmoreExpenseValidComponent', () => {
  let component: AddmoreExpenseValidComponent;
  let fixture: ComponentFixture<AddmoreExpenseValidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmoreExpenseValidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmoreExpenseValidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
