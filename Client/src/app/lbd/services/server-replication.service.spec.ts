import { TestBed, inject } from '@angular/core/testing';

import { ServerReplicationService } from './server-replication.service';

describe('ServerReplicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServerReplicationService]
    });
  });

  it('should be created', inject([ServerReplicationService], (service: ServerReplicationService) => {
    expect(service).toBeTruthy();
  }));
});
