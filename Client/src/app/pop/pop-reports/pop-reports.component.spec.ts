import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopReportsComponent } from './pop-reports.component';

describe('PopReportsComponent', () => {
  let component: PopReportsComponent;
  let fixture: ComponentFixture<PopReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
