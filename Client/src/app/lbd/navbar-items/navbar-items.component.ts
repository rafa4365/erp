import { Component, OnInit, Input, HostListener } from '@angular/core';
import { NavItem } from '../services/interface';
import { AuthLoginService } from '../../lbd/services/auth-login.service';
import { Router } from '@angular/router';

declare var demo;
@Component({
  selector: 'app-navbar-items',
  templateUrl: './navbar-items.component.html',
  styleUrls: ['./navbar-items.component.scss']
})
export class NavbarItemsComponent implements OnInit {

  // mobile_menu_initialized;
  path: any = "light-bootstrap-dashboard";
  constructor(private loginService: AuthLoginService, private router: Router) { }
  public mobNav: boolean;
  public padding: string;

  @Input()
  navItems: NavItem[];

  @Input()
  navbarClass: string;

  @Input()
  listClass: string;

  @Input()
  showTitles: boolean;

  ngOnInit() {
    // if(!this.mobile_menu_initialized){
    this.mobNav = false

    //       }
  }

  @HostListener('window:resize')
  onWindowResize() {
    if (window.innerWidth > 991) {
      this.mobNav = false
      this.padding = '4px 16px 3px 16px'
    } else {
      this.mobNav = true
      this.padding = '4px 60px 3px 60px'

    }
    if (window.innerWidth < 991) {
      this.mobNav = true

    }
    // this.path.mob();

    // //console.log('Resize size ',window.innerWidth);

  }
  logout() {
    this.loginService.setlogout();
    this.router.navigate(['/'])

  }
}
