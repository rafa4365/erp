import { Component, OnInit ,AfterViewInit} from '@angular/core';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';
import { SweetAlertsService , swalType, swalOptions } from '../../lbd/services/sweet-alerts.service';
import { ScriptLoaderService } from '../../lbd/services/script-loader.service';

@Component({
  selector: 'app-sweet-alert',
  templateUrl: './sweet-alert.component.html',
  styleUrls: ['./sweet-alert.component.scss']
})
export class SweetAlertComponent implements OnInit {

  items:any[]

  constructor(private navbarTitleService: NavbarTilteService,private SweetAlertsService: SweetAlertsService,private _script: ScriptLoaderService) { }
  ngAfterViewInit(){
    this._script.load('app-sweet-alert',
    '../../assets/js/sweetalert2.js');
  }
    ngOnInit() {
      this.navbarTitleService.updateTitle('Sweet Alerts');
      this.items=[
        {header:"Basic example",buttontext:"Try me!",type:1,options:{title:"Here's a message!",showConfirmButton: true}},
        {header:"A title with a text under",buttontext:"Try me!",type:2,options:{
          title:"Here's a message!",
          text:"It's pretty, isn't it?",
          showConfirmButton: true
          
        }},
        {header:"A success message",buttontext:"Try me!",type:3,options:{
          title:"Good job!",
          text:"You clicked the button!",
          showConfirmButton: true
          
        }},
        {header:"Custom HTML description",buttontext:"Try me!",type:4,options:{
          title: 'HTML example',
          html: 'You can use <b>bold text</b>, ' +
              '<a href="http://github.com">links</a> ' +
              'and other HTML tags',
          showConfirmButton: true
          
        }},
        {header:"A warning message, with a function attached to the Confirm Button...",buttontext:"Try me!",type:5,options:{
          title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn btn-info btn-fill",
          confirmButtonText: "Yes, delete it!",
          cancelButtonClass: "btn btn-danger btn-fill",
          closeOnConfirm: false,
          showConfirmButton: true,
          closeOnCancel: true
          
           
        }},
        {header:"...and by passing a parameter, you can execute something else for Cancel",buttontext:"Try me!",type:6,options:{
          title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn btn-info btn-fill",
          confirmButtonText: "Yes, delete it!",
          cancelButtonClass: "btn btn-danger btn-fill",
          closeOnConfirm: false,
          showConfirmButton: true
          
        }},
        {header:"A message with auto close timer set to 2 seconds",buttontext:"Try me!",type:7,options:{
          title: "Auto close alert!",
          text: "I will close in 2 seconds.",
          timer: 2000,
          showConfirmButton: false
        }},
        {header:"Modal window with input field",buttontext:"Try me!",type:8,options:{
          title: 'Input something',
          html: '<p><input id="input-field" class="form-control">',
          showCancelButton: true,
          closeOnConfirm: false,
          allowOutsideClick: false,
          showConfirmButton: true,
          closeOnCancel: true,
          
          
        }},
      ];
    }
    public swAlert(value,outerType){
      this.SweetAlertsService.swAlert(new swalOptions(value),outerType);

    }

}
 