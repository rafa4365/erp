import { Component, OnInit, AfterViewInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ScriptLoaderService } from '../../../lbd/services/script-loader.service';
import { NavbarTilteService } from '../../../lbd/services/navbar-tilte.service';
import { T2fschemaService } from '../../../lbd/services/t2fschema.service';
import { Subscription } from 'rxjs/Subscription';
import { PouchbService } from '../../../lbd/services/pouchb.service';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.scss']
})
export class TemplateFormComponent implements OnInit, OnDestroy {

  schema: any;
  subscription1: Subscription;
  subscription2: Subscription;
  formValues: object = {};
  updateValues: any;
  udate: any;
  uSselect: any;
  uSelect2: any;
  utypeAhead: any;
  uaddmore: any;

  array = [];
  number;


  constructor(
      private navbarTitleService: NavbarTilteService,
      private _script: ScriptLoaderService,
      private db: PouchbService, private cdr: ChangeDetectorRef,
      private t2f: T2fschemaService,
      private _location: Location,
      private router: Router
  ) {
    this.subscription1 = this.t2f.myvalue$.subscribe(val => this.schema = val);
    this.subscription2 = this.t2f.uvalue$.subscribe(val => this.updateValues = val);
    console.log('hello', this.subscription2)

    // console.log('hello',this.schema)
    if (this.schema === 'default message') {
      // this._location.back();
      this.router.navigate(['../'])
      // this._location.back();
    }

  }

  ngOnInit() {

    this.navbarTitleService.updateTitle('');
    this.firstRow();
    console.log('update ng oninit template comp', this.updateValues)
    // setTimeout(()=> {

    for (var i = 0; i < this.schema.form.length; i++) {

      console.log("form name arrays", this.formValues[this.schema.form[i].name])

      if (this.updateValues) {
        this.formValues[this.schema.form[i].name] = this.updateValues[this.schema.form[i].name]
      }
      if (this.schema.form[i].fieldtype === 'date') {
        this.udate = this.updateValues[this.schema.form[i].name]
        // console.log("ddddddddddddddddddddddddd",this.udate)
      }

      if (this.schema.form[i].fieldtype === 'sselect') {

        this.uSselect = this.updateValues[this.schema.form[i].name]
        // console.log("ssssssssssssssssss ssselect",this.uSselect)
      }

      if (this.schema.form[i].fieldtype === 'select2') {
        this.uSelect2 = this.updateValues[this.schema.form[i].name]
        // console.log("ssssssssssssssssss ssselect",this.uSselect)
      }
      if (this.schema.form[i].fieldtype === 'typeAhead') {
        this.utypeAhead = this.updateValues[this.schema.form[i].name]
        // console.log("ssssssssssssssssss ssselect",this.uSselect)
      }
      if (this.schema.form[i].fieldtype === 'addmore') {
        this.uaddmore = this.updateValues[this.schema.form[i].name]
        // console.log("ssssssssssssssssss ssselect",this.uSselect)
      }




    }
    // }, 5000);


  }

  ngAfterViewInit() {
    this._script.load('app-template-form', '../../assets/js/light-bootstrap-dashboard.js')
    // setTimeout(()=> {

    for (var i = 0; i < this.schema.form.length; i++) {
      this.formValues[this.schema.form[i].name];
    }
    // }, 5000);
  }

  nameSelect(value) {
    this.formValues[value.schema.name.name] = value.result.name;
    this.formValues[value.schema.result.name] = value.result.address;
  }

  date(value) {
    this.formValues[value.name] = value.value;
  }
  select(value) {
    this.formValues[value.name] = value.value;
  }
  mselect(value) {
    this.formValues[value.name] = value.value;
  }

  select2(value) {
    this.formValues[value.name] = value.value;
    // this.db.fetchSupplier(this.schema.table,);

  }
  typeAhead(value) {
    this.formValues[value.name] = value.value;
  }
  addMore(obj) {
    // this.formValues[value.name]=value.value;
    //    var  addMoreArray:Array<Object>=[{}]
    // console.log('pooooooooooooo',obj)
    //     this.formValues[obj.name] = obj.value;
    //     for(var i=0; i<obj.value.length;i++){
    //       console.log('for i',i)

    // for(var j=0;j<obj.value[i].length;j++){ 
    //   console.log('for j',j)
    //   console.log('for ij',i,j,obj.value[i][j].name,obj.value[i][j].value)

    //   addMoreArray[i][j]={
    //     // name:obj.value[i].name ,
    //     [obj.value[i][j].name]: obj.value[i][j].value
    //     // test:'hello',         
    //     // val:j
    //   }
    // }

    //     }
    console.log("array values final parent", obj)
    // console.log("addMoreArray ", addMoreArray)
    this.formValues[obj.name] = obj.value;


  }

  back() { this._location.back(); }

  firstRow() { this.number = 1; this.array.push(this.number); }

  delete(i) { console.log('delete fun', i); this.array.splice(i, 1) }

  submit() {
    console.log('sumit', this.formValues)

    this.formValues['_id'] = Date.now().toString();
    this.formValues['view'] = this.schema.table.view;

    console.log("output nested arra in template form", this.formValues)
    this.db.query(this.formValues);
    this._location.back();
  }
  update() {
    this.formValues['_id'] = this.updateValues._id;
    this.formValues['_rev'] = this.updateValues._rev;
    this.formValues['view'] = this.updateValues.view;

    console.log('update values', this.formValues)

    this.db.update(this.formValues);
    this._location.back();
  }

  searchselect(obj) {
    // if(obj.value!==''){
    //   console.log('parent tem-form searchselect value emited',obj)
    // this.db.dbsearchselect(obj).then((result:any)=>{
    console.log('parent tem-form dbsearchselect value promised', obj)
    //   })
    // }
  }

  ngOnDestroy() {
    this.subscription1.unsubscribe(); this.subscription2.unsubscribe();
    //  this.subscription2=null;
    //  console.log('destroy ',this.subscription2)
    // for(var i=0;i<this.schema.form.length;i++){ 
    //   // this.formValues[this.schema.form[i].name=''];
    // }
  }
}
