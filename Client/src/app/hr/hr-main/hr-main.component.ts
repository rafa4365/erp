import { Component, OnInit } from '@angular/core';
import { sidebarData,navItems } from '../hr-schema';


@Component({
  selector: 'app-hr-main',
  templateUrl: './hr-main.component.html',
  styleUrls: ['./hr-main.component.scss']
})
export class HRMainComponent implements OnInit {

  constructor() {
      Object.assign(this, {sidebarData, navItems})    
   }

  ngOnInit() {}
}
