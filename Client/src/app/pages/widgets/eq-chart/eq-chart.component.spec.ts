import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EqChartComponent } from './eq-chart.component';

describe('EqChartComponent', () => {
  let component: EqChartComponent;
  let fixture: ComponentFixture<EqChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EqChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EqChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
