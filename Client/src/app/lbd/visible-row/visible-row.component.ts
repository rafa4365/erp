import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';

declare var $;
@Component({
  selector: 'app-visible-row',
  templateUrl: './visible-row.component.html',
  styleUrls: ['./visible-row.component.scss']
})
export class VisibleRowComponent implements OnInit {
@Input() public schema:any;
@Output() public returnVisibleRowsData= new EventEmitter<any>();

dataArray=[];
setpagelimit;
currentRows;
rowsInfo;
totalObjects;
pagearray=[];
currentPage:number=1;
indexing=0;


  constructor(    private db:PouchbService ) { }

  ngOnInit() {
    console.log('visible rows component schema',this.schema)

    this.db.fetchData(this.schema.dpSize,this.schema.table).then((doc)=>{
      
           this.totalObjects = doc.total_rows;
           for(var i =0;i<this.totalObjects;i++){
             this.pagearray.push(i)
           }  
           for(var i=0;i < doc.rows.length;i++){
             this.dataArray[i]=doc.rows[i].doc;
           };
           this.getpagelimit(this.schema.dpSize)
       
           this.currentRows=  doc.rows.length
           this.rowsInfo=`${this.currentRows} rows visible out of ${this.totalObjects}`;
      this.returnVisibleRowsData.emit(this.dataArray)
      
           
         });
  }

  getpagelimit(value){
    if(value===''){
      this.setpagelimit=value 
      this.schema.dpSize='Showing All';   
      
      $('#paginate').pagination('hide')
    }else{
      this.schema.dpSize=value;   
      this.setpagelimit=value 

   
      $('#paginate').pagination({
        dataSource:  this.pagearray,
        pageSize: this.setpagelimit,
        pageRange: 1,
        totalNumber: 44,
        showPageNumbers: true,
        showGoInput: true,
        showGoButton: false,
        activeClassName:'active', 
        className:'pull-right pagination',
        disableClassName:'disabled',
        ulClassName:'pagination',
        showNavigator: true,
        beforePageOnClick:(data)=>{
          this.pagination(parseInt(data.target.innerText));
        },
        beforeNextOnClick:(data)=>{
          this.pagination('next');
        },
        beforePreviousOnClick:(data)=>{ 
          this.pagination('pre');
        },
        beforeGoInputOnEnter:(data)=>{
          this.pagination(data.currentTarget.value);
        }
    })
    }

    this.db.fetchData(value,this.schema.table).then((doc)=>{
      this.dataArray=[];
    this.currentRows=  doc.rows.length;
    this.rowsInfo=`${this.currentRows} rows visible out of ${this.totalObjects}`;
    if(this.currentRows=== this.totalObjects){
      this.rowsInfo=  'Showing All'
    }
      for(var i=0;i < doc.rows.length;i++){
        
              this.dataArray[i]=doc.rows[i].doc;
        
            };      
      this.returnVisibleRowsData.emit({data:this.dataArray,skip:0,cp:this.currentPage})
      
    });
  }

  pagination(value){
    if(value=='next'){
      this.currentPage++
    }else if(value=='pre'){
      this.currentPage--
    }else{
      this.currentPage=value
    }

    var skip=(this.schema.dpSize*this.currentPage)-this.schema.dpSize;

    this.db.pagination(skip,this.setpagelimit,this.schema.table).then((doc)=>{
      this.dataArray=[]
      for(var i=0;i < doc.rows.length;i++){this.dataArray[i]=doc.rows[i].doc}; 
      console.log('visible rows pagination function',this.dataArray)
      this.returnVisibleRowsData.emit({data:this.dataArray,skip:skip,cp:this.currentPage})
    })
  }


}
