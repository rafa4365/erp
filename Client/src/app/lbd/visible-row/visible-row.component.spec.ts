import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisibleRowComponent } from './visible-row.component';

describe('VisibleRowComponent', () => {
  let component: VisibleRowComponent;
  let fixture: ComponentFixture<VisibleRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisibleRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisibleRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
