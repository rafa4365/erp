var nameresult = {
  name: { name: 'customer', value: '', placeholder: 'Enter Name', err: "customer is required",valid:'',ragexp:'^([a-z]{5,})$',class:'form-control', view: 'customer' },
  result: { name: 'adress', value: '', placeholder: 'Enter Adress', err: "address is required",valid:'',ragexp:'^([a-z]{5,})$',class:'form-control' },
}
var join_date= {
  title: 'Datetime Picker', class: 'datetimepicker', placeholder: 'Joining date here', options: {
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-chevron-up",
      down: "fa fa-chevron-down",
      previous: 'fa fa-chevron-left',
      next: 'fa fa-chevron-right',
      today: 'fa fa-screenshot',
      clear: 'fa fa-trash',
      close: 'fa fa-remove'
    }
  }
}
var addmore=[
  [
    { type: 'text', value: '', fieldtype: 'searchselect', view: 'inventory', placeholder: 'Enter Name', name: 'itemName', colclass: 'col-md-3' },
    { type: 'text', value: '', aline: 'full', colclass: 'col-md-3', fieldtype: 'input', name: 'descrip', placeholder: 'discription', err: "salary is required" },
    { type: 'text', value: 1, aline: 'full', colclass: 'col-md-1', fieldtype: 'input', name: 'qty', placeholder: 'qty', err: "country is required" },
    { type: 'text', value: '', aline: 'full', colclass: 'col-md-1', fieldtype: 'input', name: 'unitprice', placeholder: 'unit price', err: "country is required" },
    { type: 'text', value: '', aline: 'full', colclass: 'col-md-2', fieldtype: 'input', name: 'dis', placeholder: 'discount', err: "country is required" },
    { type: 'text', value: '', aline: 'full', colclass: 'col-md-2', fieldtype: 'input', name: 'amount', placeholder: 'amount', err: "country is required" },
  ]
]
var addmoreExpense=[
  [
    { type: 'text', value: '', fieldtype: 'searchselect', view: 'expenseCategory', placeholder: 'Enter Name', name: 'itemName', colclass: 'col-md-4' },
    { type: 'text', value: '', aline: 'full', colclass: 'col-md-6', fieldtype: 'input', name: 'descrip', placeholder: 'discription', err: "salary is required" },
    { type: 'text', value: '', aline: 'full', colclass: 'col-md-2', fieldtype: 'input', name: 'amount', placeholder: 'amount', err: "country is required" },
  ]
]
export var validForm={

    title:'Profile',
    type:null,
    table: {
      docDesign: 'widgets', 
      view: 'appuser',
      export: false,
      colView: false,
      gridView: false,
      refresh: true,
      search: false,
      dateRange: false  ,
      settings:false,
      options:{
        display:true,
        rowView:false,
        rowDelete:true,
        rowUpdate:true,
      },
    },
    numrow: [10, 25, 100],    
    rows:[
        {col:[
          {class:'col-md-5',data:{label:'Company',name:'company',type:'text',fieldtype:'input',showCol:true,placeholder:'Company',value:'',valid:'',ragexp:'^[a-z A-Z]+$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-3',data:{label:'Username',name:'username',type:'text',fieldtype:'input',showCol:false,placeholder:'Username',value:'',valid:'',ragexp:'^([a-z0-9]{5,})$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-4',data:{label:'Email address',name:'email',type:'text',fieldtype:'input',showCol:false,placeholder:'Email',value:'',valid:'',ragexp:'/([\w\.]+)@([\w\.]+)\.(\w+)/',class:'form-control',err:'this is invalid error '}}
        ]},
        {col:[
          {class:'col-md-6',data:{label:'First Name',name:'firstName',type:'text',fieldtype:'input',showCol:true,placeholder:'First Name',value:'',valid:'',ragexp:'^([a-zA-Z]{1,})$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-6',data:{label:'Last Name',name:'lastName',type:'text',fieldtype:'input',showCol:false,placeholder:'Last Name',value:'',valid:'',ragexp:'^([a-zA-Z]{1,})$',class:'form-control',err:'this is invalid error '}},
        ]},
        {col:[
          {class:'col-md-6',data:{label:'Username',name:'username',type:'text',fieldtype:'input',showCol:false,placeholder:'Username',value:'',valid:'',ragexp:'^([a-z0-9]{5,})$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-6',data:{label:'Password',name:'password',type:'password',fieldtype:'input',showCol:false,placeholder:'Password',value:'',valid:'',ragexp:'^([a-zA-Z0-9\!\@\$\%\^\&\*]{8,})$',class:'form-control',err:'this is invalid error '}}
        ]},
        {col:[
          {class:'col-md-12',data:{label:'Address',name:'address',type:'text',fieldtype:'input',showCol:false,placeholder:'Address',value:'',valid:'',ragexp:'^[a-z A-Z 0-9 \#]+$',class:'form-control',err:'this is invalid error '}},
        ]},
        {col:[
          {class:'col-md-4',data:{label:'City',name:'city',type:'text',fieldtype:'input',showCol:true,placeholder:'City',value:'',valid:'',ragexp:'^[a-z A-Z]+$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-4',data:{label:'Country',name:'country',type:'text',fieldtype:'input',showCol:true,placeholder:'Country',value:'',valid:'',ragexp:'^[a-z A-Z]+$',class:'form-control',err:'this is invalid error '}},
          {class:'col-md-4',data:{label:'Postal Code',name:'postCode',type:'text',fieldtype:'input',showCol:true,placeholder:'Zip Code',value:'',valid:'',ragexp:'^([0-9]{5,})$',class:'form-control',err:'this is invalid error '}},
        ]},
        // {col:[
        //   {class:'col-md-6',data:{label:'Joining Date',schema:join_date,name:'joinDate',fieldtype:'datepicker',showCol:false,value:'',valid:'',ragexp:'^([a-z]{5,})$',err:'this is invalid error of text area'}},
        //   {class:'col-md-3',data:{label:'Leaving Date',schema:join_date,name:'leavingDate',fieldtype:'datepicker',showCol:false,value:'',valid:'',ragexp:'^([a-z]{5,})$',err:'this is invalid error of text area'}},
        //   {class:'col-md-3',data:{label:'Form Filling Date',schema:join_date,name:'fillingDate',fieldtype:'datepicker',showCol:false,value:'',valid:'',ragexp:'^([a-z]{5,})$',err:'this is invalid error of text area'}},
        // ]},
        // {col:[
        //   {class:'col-md-12',data:{schema:{schema: nameresult}, fieldtype: 'nameresult',showCol:false}},
        // ]},
        // {col:[
        //   {class:'col-md-12',data:{schema:{schema: addmore, name: 'inventory' },fieldtype: 'addmore', showCol: false}}
        // ]},
        // {col:[
        //   {class:'col-md-12',data:{schema:{schema: addmore, name: 'inventory' },fieldtype: 'addmoreSale', showCol: false}}
        // ]},
        // {col:[
        //   {class:'col-md-12',data:{schema:{schema: addmoreExpense, name: 'expenseCategory' },fieldtype: 'addmoreExpense', showCol: false}}
        // ]},
        {col:[
          // {class:'col-md-12',data:{label:'Add Picture',name:'uploadimage',type:'file',fieldtype:'imageuploader',showCol:false}},
          {class:'col-md-12',data:{label:'About Me',name:'aboutMe',type:'textarea',rows:'5',fieldtype:'textarea',showCol:false,placeholder:'Here can be your description',value:'',valid:'',ragexp:'^[a-z A-Z 0-9]+$',class:'form-control',err:'this is invalid error of text area'}},
        ]},
         ]
        }     