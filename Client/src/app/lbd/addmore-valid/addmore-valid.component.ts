import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';

declare var $;

@Component({
  selector: 'app-addmore-valid',
  templateUrl: './addmore-valid.component.html',
  styleUrls: ['./addmore-valid.component.scss']
})
export class AddmoreValidComponent implements OnInit {
@Input() schema;
@Output() notify = new EventEmitter<any>();
@Input() public update: any;

obj={
  name:null,
  discp:'',
  qty:null,
  up:null,
  discount:null,
  amount:null,
}
array:any=[];
total=0;
select2data=[];
name={
  class:'form-control',
  valid:null
};
dis={
  class:'form-control',
  valid:null
};
qty={
  class:'form-control',
  valid:null
};
discount={
  class:'form-control',
  valid:null
};


  constructor(private db: PouchbService) { }

  ngOnInit() {
    console.log('addmore valid schema',this.schema);
    (<HTMLInputElement>document.getElementById("more")).disabled = true;    
    
    this.select();
    this.updatevalues(this.update)
  }
  updatevalues(value){
    if(value){
      this.array=value
      this.totalFun()
    }
  }
  select(){
    var obj={
      view:this.schema.view,
      docDesign:this.schema.docDesign
    }
    this.db.fetchData('Showing All',obj).then((r:any)=>{
      for(var i=0;i< r.rows.length;i++){
        this.select2data[i]={
          id:i,
          text:r.rows[i].doc['itemname'],
          obj:r.rows[i]
        }
      }
      console.log('fetch result ',this.select2data)
      $(".addmorevalid").select2({
        placeholder: "Select",
        allowClear: true,
        
        data: this.select2data
      })
    })
    console.log('nameresult', this.schema)


    // $(".addmorevalid").on('load',  (e)=> {
    //   $(".addmorevalid").val(null).trigger("change");
    // })
      
$(".addmorevalid").on('select2:select',  (e)=> {
  // Do something
  // console.log('e', e.params.data.text)
  
  console.log('select2 value',this.select2data[e.target.value])
  this.obj.name=e.params.data.text
  this.obj.discp=this.select2data[e.target.value].obj.doc.description
  this.obj.up=this.select2data[e.target.value].obj.doc[this.schema.searchvalue]
  this.obj.qty=1
  this.obj.discount=0
  // this.result=e.target.value;
  this.math()
  this.change()
});
  }
  add(){
    console.log('addmore valid add function',this.obj);
    (<HTMLInputElement>document.getElementById("more")).disabled = true;    
    
    this.array.push(this.obj);
    this.totalFun() 
    this.obj={
      name:null,
      discp:'',
      qty:null,
      up:null,
      discount:null,
      amount:null,
    }
    this.check();
    this.notify.emit(this.array);
    
  }
  remove(i){
    this.array.splice(i,1)
    this.totalFun()   
    if(this.array.length===0){
      this.schema.valid=false;
      this.notify.emit(this.array);
      console.log('remove',this.schema)

    }  
  }
  math(){
    var dis=0;
    this.obj.amount=this.obj.qty*this.obj.up
    dis=this.obj.amount*(this.obj.discount/100)
    this.obj.amount=this.obj.amount-dis
    this.totalFun()     
  }
  totalFun(){
    this.total=0
    this.array.forEach((element)=>{
      this.total+=element.amount
    })
    this.array.total=this.total;
    
  }
change(){
  this.math();
  this.check();
}
check(){
  var _name=new RegExp(/^([a-z A-Z.]{2,})$/);
  var dis_exp=new RegExp(/^([a-z A-Z.]{1,})$/);
  var qty_exp=new RegExp(/^([0-9]{1,})$/);
  var discount_exp=new RegExp(/^([0-9]{1,})$/);
  // var exp=[]
  this.validation(this.obj.name,_name,this.name);
  this.validation(this.obj.discp,dis_exp,this.dis);
  this.validation(this.obj.qty,qty_exp,this.qty);
  this.validation(this.obj.discount,discount_exp,this.discount);
}
  validation(value,regex,obj){
    
    if(regex.test(value) && value!==''){
      obj.class='form-control valid';
      obj.valid=true;
      // this.enablebutton(count)
    }
    if(!regex.test(value) && value!==''){
      obj.class='form-control invalid';
      (<HTMLInputElement>document.getElementById("more")).disabled = true;    
      
      obj.valid=false;
    }
    if(value ==='' || value===null){
      obj.class='form-control';
      obj.valid='';
    }
    if(this.name.valid===true && this.dis.valid===true && this.qty.valid===true && this.discount.valid===true){
      (<HTMLInputElement>document.getElementById("more")).disabled = false;    
      
    }
  // //  var exp =/^0*(?:[1-9][0-9]?|100)$/
  //   var exp= /^(0*[1-9][0-9]*)$/
  //   // var exp= /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;
  //   var re= new RegExp(exp) ;
  //   console.log('regex exp test :::::',re.test(this.obj.discp))
  }

}
