import { Injectable } from '@angular/core';
import {NotificationService, NotificationType, NotificationOptions}  from '../../lbd/services/notification.service';

declare let PouchDB: any;
declare let emit: Function;

@Injectable()
export class ServerReplicationService {

  private pouchDb: any;
  private pouchDbEventEmitter: any;
  private pouchDbSyncEventEmitter: any;
  
  constructor(    private notificationService: NotificationService  ) {
    
   }
   repli(remoteIP,localIP){
    //  localIP='localhost'
    this.pouchDb = new PouchDB(`http://${localIP}:3000/testdb`);
    var r=`http://192.168.1.30:3000/testdb`
    var remoteDB = new PouchDB(r);
    this.pouchDb.sync(remoteDB, {
      live: true,
      retry: true
    })
    // this.pouchDb.replicate.to(remoteDB).on('complete',  ()=> {
    //   // yay, we're done!
    //   this.showNotification('bottom','center',2,`Replicating to remote IP : ${remoteIP} `,'pe-7s-close')
      
    // }).on('error',  (err)=> {
    //   // boo, something went wrong!
    //   this.showNotification('bottom','center',4,`Not Replicating to remote IP : ${remoteIP} `,'pe-7s-close')
      
    // });
    // this.pouchDb.replicate.from(remoteDB).on('complete',  ()=> {
    //   // yay, we're done!
    //   this.showNotification('bottom','center',2,`Replicating from remote IP : ${remoteIP} `,'pe-7s-close')
      
    // }).on('error',  (err)=> {
    //   // boo, something went wrong!
    //   this.showNotification('bottom','center',4,`Not Replicating from remote IP : ${remoteIP} `,'pe-7s-close')
      
    // });
    
   }
  //  Save(remoteIP){
  //    var data={
  //      ['_id']:'remoteDb',
  //      remoteDb:'remoteIP',
  //      view:'remoteIP'
  //    }
  //   this.pouchDb.put(data).then(() => {
  //     this.showNotification('bottom','center',2,`remote IP : ${remoteIP} is saved ! `,'pe-7s-close')
      
  //   })
  //  }
get(){
  var options={
    include_docs: true,
    
  }
  this.pouchDb.query(`admin/remoteIP`, options)
  .then((result: any) => {

    // resolve(result);
  })
  // .catch(reject)

}
   public showNotification(from: string, align: string,type: NotificationType,message:any,icon:any) {
    this.notificationService.notify(new NotificationOptions({
      message: message,
      icon: icon,
      type: <NotificationType>(type),
      from: from,
      align: align
    }));
  }
}
