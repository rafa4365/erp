import { Component, OnInit , Input} from '@angular/core';

@Component({
  selector: 'app-fixed-plugin',
  templateUrl: './fixed-plugin.component.html',
  styleUrls: ['./fixed-plugin.component.scss']
})
export class FixedPluginComponent implements OnInit {

  constructor() { }
  
  @Input()
  public schema: any;
  
  ngOnInit() {
  }

}
