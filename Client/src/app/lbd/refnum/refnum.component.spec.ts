import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefnumComponent } from './refnum.component';

describe('RefnumComponent', () => {
  let component: RefnumComponent;
  let fixture: ComponentFixture<RefnumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefnumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefnumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
