import { Component, OnInit,Input,OnDestroy , trigger, state, style, transition, animate} from '@angular/core';
import { T2fschemaService } from '../../lbd/services/t2fschema.service';
import { ScriptLoaderService } from '../../lbd/services/script-loader.service';
import { PouchbService } from '../../lbd/services/pouchb.service';
import {NotificationService, NotificationType, NotificationOptions}  from '../../lbd/services/notification.service';
// import { swalOptions } from '../../lbd/services/sweet-alerts.service';

declare var moment;
declare var $;
declare var swal; 

@Component({
  selector: 'app-valid-table',
  templateUrl: './valid-table.component.html',
  styleUrls: ['./valid-table.component.scss'],
  animations: [
    trigger('cardtable1', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1})),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)', 
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out')
      ])
    ]),
    trigger('cardtable2', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1})),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0.25s ease-out')
      ])
    ])
  ]
})
export class ValidTableComponent implements OnInit ,OnDestroy{
@Input() schema; 
defaultPageSize:any;
dataArray: any=[];
toggleVar:boolean=false;
setpagelimit;
viewOption={}
viewModal:any={type:'View', buttonIcon: "fa fa-eye",buttonClass:'table-action eye',modalTitle:'view'};
indexing=0;
limit:any
searchInputObject:any;
visibleRowsInputObject:any;
metadata;
  constructor(
    // private SweetAlertsService: SweetAlertsService,
    private _script: ScriptLoaderService,
    private t2f: T2fschemaService,
    private db:PouchbService,
    private notificationService: NotificationService,
    
  ) { }

  ngOnInit() {
    console.log('valid table data ',this.schema) 
    this.db.createView(this.schema.table,this.schema.numrow[0]).then((doc)=>{
      for(var i=0;i < doc.rows.length;i++){
        this.dataArray[i]=doc.rows[i].doc;
      };
    console.log('dataArray ',this.dataArray)   
    this.dueDate(this.dataArray) 
    })
    this.defaultPageSize=this.schema.numrow[0]
    this.setpagelimit=this.schema.numrow[0]
    
    //-----search -------
    this.searchInputObject={
      form:this.schema.form,
      table:this.schema.table,
      dpSize:this.defaultPageSize,
    }
    
    this.visibleRowsInputObject={
      num:this.schema.numrow,
      table:this.schema.table,
      dpSize:this.defaultPageSize,
    }
   
  this.viewOption={
    export: this.schema.table.export || false,
    colView: this.schema.table.colView || false,
    gridView: this.schema.table.gridView || false,
    refresh: this.schema.table.refresh || false,
    search: this.schema.table.search || false,
    dateRange: this.schema.table.dateRange || false,
  }
  this.metadata={
    options:{
    title: "Are you sure?",
    text: "You are closeing this payment!",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn btn-info btn-fill",
    confirmButtonText: "Yes, close payment!",
    cancelButtonClass: "btn btn-danger btn-fill",
    closeOnConfirm: false,
    showConfirmButton: true 
  }
}
  }
  dueDate(value){
    var nowdate;
    nowdate=moment().format('D/MM/YYYY h:mm:ss A');
    for(var i=0;i<value.length;i++){
      
      console.log('value i valid table',value[i].DueDate,nowdate)
      console.log('date com < valid table',value[i].DueDate<nowdate)        
      console.log('date com > valid table',value[i].DueDate>nowdate)        
      console.log('date com = valid table',value[i].DueDate==nowdate)        
      if(value[i].DueDate<nowdate && value[i].status.type !== this.schema.table.status.success){
        value[i].status.color='btn-danger'
      }
    }
  }
  create(){
    this.schema.type='create'    
    this.t2f.setvalue(this.schema) 
    this.t2f.updateValue('')
  }
  status(ele){
    // this.swAlert(this.metadata.options,this.metadata.type)
    // console.log('status update',ele)
    
    swal(this.metadata.options, (isConfirm) =>{
      if (isConfirm) {
          swal("Success!", "Your Payment has been closed.", "success");
          console.log('status update',ele)
          ele.status.type=this.schema.table.status.success;
          ele.status.color='btn-success';
          this.db.query(ele);
          
      } else {
          swal("Cancelled", "Your Payment has not been closed.", "error");
      }
  });
  }
  // public swAlert(value,outerType){
  //   this.SweetAlertsService.swAlert(new swalOptions(value),outerType);
    
  // }
  remove(value,i){
    // this.indexing=1
    this.dataArray.splice(i,1)
    this.db.remove(value);
    this.showNotification('top','center',4,`Item # ${this.indexx(i)} is deleted`,'pe-7s-trash')
    // this.refresh()
  }
  refresh(){

    this.db.createView(this.schema.table,this.limit).then((doc)=>{
      for(var i=0;i < doc.rows.length;i++){
        this.dataArray[i]=doc.rows[i].doc;
      };
    // console.log('dataArray ',this.dataArray)
    this.dueDate(this.dataArray) 
    
    })
  }
  showCol(col,value){
    col.data.showCol=value
  }
  showAction(value){
    this.schema.table.options.display=value
  }
  update(value){
    this.schema.type='update'
    this.t2f.setvalue(this.schema)
    this.t2f.updateValue(value)
  }
  view(value){
    this.schema.type='view'
    this.t2f.setvalue(this.schema)
    this.t2f.updateValue(value)
  }
  search(value){
    // this.dataArray=value
    this.dueDate(this.dataArray) 
    
    console.log('search',value)
  }
  
    // showCol(value,name,colstate,colindex){this.experiment.form[colindex].showCol=value;}
  
    toggleView(value){this.toggleVar = value}
  
    getpagelimit(value){
      this.dataArray=value.data
    this.dueDate(this.dataArray) 
    
      this.indexing=value.cp
      this.limit=value.skip
      console.log('indexing',this.indexing)
      console.log('limit',this.limit)
    }
    indexx(i){
      var r=0;
      r=this.limit+i+this.indexing-(this.indexing-1);
      console.log('indexing r',r)
      return r
    }

  ngAfterViewInit(){this._script.load('app-valid-table','../../assets/js/light-bootstrap-dashboard.js' )}  
  ngOnDestroy() {
    this.schema={}
  }  
  public showNotification(from: string, align: string,type: NotificationType,message:any,icon:any) {
    this.notificationService.notify(new NotificationOptions({
      message: message,
      icon: icon,
      type: <NotificationType>(type),
      from: from,
      align: align
    }));
  }  
}
  