import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmoreExpenseComponent } from './addmore-expense.component';

describe('AddmoreExpenseComponent', () => {
  let component: AddmoreExpenseComponent;
  let fixture: ComponentFixture<AddmoreExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmoreExpenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmoreExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
