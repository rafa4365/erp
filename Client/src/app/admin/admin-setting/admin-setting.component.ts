import { Component, OnInit } from '@angular/core';
import { settings } from '../admin-schema';

@Component({
  selector: 'app-admin-setting',
  templateUrl: './admin-setting.component.html',
  styleUrls: ['./admin-setting.component.scss']
})
export class AdminSettingComponent implements OnInit {

  constructor() {
    Object.assign(this, {settings})        
    
   }

  ngOnInit() {
  }

}
 