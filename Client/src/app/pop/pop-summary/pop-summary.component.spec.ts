import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopSummaryComponent } from './pop-summary.component';

describe('PopSummaryComponent', () => {
  let component: PopSummaryComponent;
  let fixture: ComponentFixture<PopSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
