import { Component, OnInit, trigger, state, style, transition, animate, AfterViewInit } from '@angular/core';
import { PouchbService } from '../../../lbd/services/pouchb.service';
import { Location } from '@angular/common';
import { NotificationService, NotificationType, NotificationOptions } from '../../../lbd/services/notification.service';
import { validation } from '../../../validation';

declare var $;
@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss'],
  animations: [
    trigger('carduserprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({
          opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out'),
      ])
    ]),
    trigger('cardprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({
          opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0.25s ease-out')
      ])
    ])
  ]
})
export class RegisterPageComponent implements OnInit {
  form:any;
  inputArray:any;
  view='registration';
  docd={
    docDesign: 'admin',
    view: 'registration'
  }
  constructor(    
    private db: PouchbService,
    private _location: Location,
    private notificationService: NotificationService,

  ) { }

  ngOnInit() {
    (<HTMLInputElement>document.getElementById("submit_btn")).disabled = true;
    // $('body').perfectScrollbar();  


    this.inputArray = {};
    this.form = [
      { type: 'text', name: 'firstName', value: '', valid: '', ragexp: validation.username.regex, class: 'form-control', placeholder: 'Your First Name', err: validation.username.msg },
      { type: 'text', name: 'lastName', value: '', valid: '', ragexp: validation.username.regex, class: 'form-control', placeholder: 'Your Last Name', err: validation.username.msg },
      { type: 'text', name: 'company', value: '', valid: '', ragexp: validation.fullname.regex, class: 'form-control', placeholder: 'Company', err: validation.fullname.msg },
      { type: 'email', name: 'email', value: '', valid: '', ragexp: validation.email.regex, class: 'form-control', placeholder: 'Enter email', err: validation.email.msg },
      { type: 'text', name: 'userName', value: '', valid: '', ragexp: validation.username.regex, class: 'form-control', placeholder: 'Enter username', err: validation.username.msg },
      { type: 'text', name: 'department', value: '', valid: '', ragexp: '', class: 'form-control', placeholder: 'Enter Department', err: 'You can only chose one from the options given below!' },
      { type: 'password', name: 'password', value: '', valid: '', ragexp: validation.password.regex, class: 'form-control', placeholder: 'Password', err: validation.password.msg },
      // { type: 'password', name: 'coPassword', value: '', valid: '', ragexp: validation.password.regex, class: 'form-control', placeholder: 'Password Confirmation', err: validation.password.msg }
    ]
  }
  submit() {
    this.db.check(this.inputArray['userName'],'userName',this.view).then((a)=>{
      console.log('condition return',a)
      if(a===true){
        this.showNotification('top', 'center', 4, `Username already exists`, 'pe-7s-check')
      }else if(a===false){
        console.log("this is register form data", this.inputArray)
        this.inputArray['_id'] = Date.now().toString();
        this.inputArray['view'] = this.view;
        this.inputArray['allowed'] = false;
        this.db.query(this.inputArray);
        this.showNotification('top', 'center', 2, `Account Request Is Sent To The Admin`, 'pe-7s-check')
        this._location.back();
      }
    })

  }
  public showNotification(from: string, align: string, type: NotificationType, message: any, icon: any) {
    this.notificationService.notify(new NotificationOptions({
      message: message,
      icon: icon,
      type: <NotificationType>(type),
      from: from,
      align: align
    }));
  }
  validate(e, i) {
    var re = new RegExp(this.form[i].ragexp);
    var count = 0;
    if(this.form[i].name==='department'){
      if (this.form[i].value ==='hr' || this.form[i].value==='admin'|| this.form[i].value==='pop' || this.form[i].value==='pos'  && e.target.className.search("ng-dirty") !== -1) {
        this.inputArray[this.form[i].name] = this.form[i].value
        this.form[i].class = 'form-control valid'
        this.form[i].valid = true;
      }
      else if (e.target.className.search("ng-dirty") !== -1) {
        this.form[i].class = 'form-control invalid'
        this.form[i].valid = false;
      }
      else if (e.target.className.search("ng-untouched") !== -1) {
        this.form[i].class = 'form-control';
        this.form[i].valid = '';
      }
    }
    if (this.form[i].name!=='department' && re.test(this.form[i].value) && e.target.className.search("ng-dirty") !== -1) {
      this.inputArray[this.form[i].name] = this.form[i].value
      this.form[i].class = 'form-control valid'
      this.form[i].valid = true;
    }
    else if (this.form[i].name!=='department' && e.target.className.search("ng-dirty") !== -1) {
      this.form[i].class = 'form-control invalid'
      this.form[i].valid = false;
    }
    else if (this.form[i].name!=='department' && e.target.className.search("ng-untouched") !== -1) {
      this.form[i].class = 'form-control';
      this.form[i].valid = '';
    }
    for (var j = 0; j < this.form.length; j++) {
      if (this.form[j].valid === false) {
        (<HTMLInputElement>document.getElementById("submit_btn")).disabled = true;
      } else if (this.form[j].valid === true) {
        count++;
      }
      if (count === this.form.length) {
        (<HTMLInputElement>document.getElementById("submit_btn")).disabled = false;
      }

    }
  }
  ngAfterViewInit() {
    // $('.wrapper').perfectScrollbar();

  }
}
