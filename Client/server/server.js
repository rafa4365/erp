var express = require('express');
var PouchDB = require('pouchdb');
var cors = require('cors')
var colors = require('colors');

var app = express();
var pouchdb = require('express-pouchdb')({
    mode: 'fullCouchDB',
    overrideMode: {
        include: ['routes/fauxton']
    }
});

var corsOptions = {
    origin: "http://localhost:4200",
    credentials: true
};

pouchdb.setPouchDB(require('pouchdb'));


app.use('/', cors(corsOptions), pouchdb);

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header('Access-Control-Allow-Credentials', false);
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });


app.listen(3000, function() {
    console.log('Pouchdb Server Listening On Port 3000....!!!'.rainbow.bold); // rainbow

    // console.log('Example app server listening on port 3000!');
});