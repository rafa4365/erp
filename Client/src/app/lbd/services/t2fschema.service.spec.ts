import { TestBed, inject } from '@angular/core/testing';

import { T2fschemaService } from './t2fschema.service';

describe('T2fschemaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [T2fschemaService]
    });
  });

  it('should be created', inject([T2fschemaService], (service: T2fschemaService) => {
    expect(service).toBeTruthy();
  }));
});
