import { Injectable } from '@angular/core';
declare var $;
declare var swal; 

export enum swalType{
  basic = 1,
  titleAndText,
  successMessage,
  customHtml,
  warningMessageAndConfirmation,
  warningMessageAndCancel,
  autoClose,
  inputField,
  calender

}
export class swalOptions{
  public title: string;
  public text: string;
  public html: string;
  public type: string ;
  public allowOutsideClick: boolean;
  public showCancelButton: boolean;
  public showConfirmButton: boolean;
  public confirmButtonClass: string;
  public confirmButtonText: string;
  public cancelButtonClass: string;
  public cancelButtonText: string;
  public closeOnConfirm: boolean;
  public closeOnCancel: boolean;
  public timer;
  public start;
  public end;
  public calender;


  public constructor(
    fields: {
  title?: string,
  text?: string,
  html?: string,
  type?: string,
  showConfirmButton?: boolean,
  allowOutsideClick?: boolean,
  showCancelButton?: boolean,
  confirmButtonClass?: string,
    confirmButtonText?: string,
    cancelButtonClass?: string,
    cancelButtonText?: string,
    closeOnConfirm?: boolean,
    closeOnCancel?: boolean;
    
    timer?: number
    start?: any
    end?: any
    calender?:any

}) {

    this.title = fields.title;
    this.text = fields.text || this.text;
    this.html = fields.html || this.html;
    this.timer = fields.timer || this.timer;
    this.start = fields.start || this.start;
    this.end = fields.end || this.end;
    this.calender = fields.calender || this.calender;
    this.type = fields.type || this.type;
    this.allowOutsideClick = fields.allowOutsideClick || this.allowOutsideClick || false;
    this.showConfirmButton = fields.showConfirmButton || this.showConfirmButton || false;
    this.showCancelButton = fields.showCancelButton || this.showCancelButton || false;
    this.confirmButtonClass = fields.confirmButtonClass || this.confirmButtonClass;
    this.confirmButtonText = fields.confirmButtonText || this.confirmButtonText;
    this.cancelButtonClass = fields.cancelButtonClass || this.cancelButtonClass;
    this.cancelButtonText = fields.cancelButtonText || this.cancelButtonText;
    this.closeOnConfirm = fields.closeOnConfirm || this.closeOnConfirm || false;
    this.closeOnCancel = fields.closeOnCancel || this.closeOnCancel || false;
    
  }
}
@Injectable()
export class SweetAlertsService {

  constructor() {

// $.ajax({
//     url: '../../../../assets/js/sweetalert2.js',
//     dataType: 'script',
//     async: false
// });
   }
  
    public swAlert(options: swalOptions,type:swalType): void {
      let typeString;
      //console.log('options',options)
      switch (type) {

        case swalType.titleAndText:
        typeString = 'title-and-text';
        //console.log('typeString',typeString)
        
        break;
        case swalType.successMessage:
        typeString = 'success-message';
        //console.log('typeString',typeString)
        
        break;
        case swalType.warningMessageAndConfirmation:
        typeString = 'warning-message-and-confirmation';
        //console.log('typeString',typeString)
        
        break;
        case swalType.warningMessageAndCancel:
        typeString = 'warning-message-and-cancel';
        //console.log('typeString',typeString)
        
        break;
        case swalType.customHtml:
        typeString = 'custom-html';
        //console.log('typeString',typeString)
        
        break;
        case swalType.autoClose:
        typeString = 'auto-close';
        //console.log('typeString',typeString)
        
        break;
        case swalType.inputField:
        typeString = 'input-field';
        //console.log('typeString',typeString)
        
        break;
        case swalType.calender:
        typeString = 'calender';
        //console.log('typeString',typeString)
        
        break;
        default:
          typeString = 'basic';
          //console.log('typeString default',typeString)
          
          break;
      }

      if (typeString=== 'basic') {
        swal(options);

    } else if (typeString=== 'title-and-text') {
        swal(options)

    } else if (typeString=== 'success-message') {
        swal("Good job!", "You clicked the button!", "success")

    } else if (typeString=== 'warning-message-and-confirmation') {
        swal(options, function() {
            swal("Deleted!", "Your imaginary file has been deleted.", "success");
        });

    } else if (typeString=== 'warning-message-and-cancel') {
        swal(options, function(isConfirm) {
            if (isConfirm) {
                swal("Deleted123!", "Your imaginary file has been deleted.", "success");
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
        });

    } else if (typeString=== 'custom-html') {
        swal(options);

    } else if (typeString=== 'auto-close') {
        swal(options);
    } else if (typeString=== 'input-field') {
        swal(options,
            function() {
                swal({
                    html: 'You entered: <strong>' +
                        $('#input-field').val() +
                        '</strong>'
                });
            })
    }else if (typeString=== 'calender') {
        swal(options,
            function() {
                
                                  var eventData;
                                  var event_title = $('#input-field').val();
                
                                  if (event_title) {
                                      eventData = {
                                          title: event_title,
                                          start: options.start,
                                          end: options.end
                                      };
                                      options.calender.fullCalendar('renderEvent', eventData, true); // stick? = true
                                  }
                
                                  options.calender.fullCalendar('unselect');
                
                              })
    }

  
}



}
