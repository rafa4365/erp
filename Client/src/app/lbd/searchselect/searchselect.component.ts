import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';

@Component({
  selector: 'app-searchselect',
  templateUrl: './searchselect.component.html',
  styleUrls: ['./searchselect.component.scss']
})
export class SearchselectComponent implements OnInit {
  @Input()  public schema: any;
  @Input()  public index: any;
  @Output() notify= new EventEmitter<any>();
  
  dropdown=[];
  show:boolean=false;
  result:any;

    constructor(private db:PouchbService) {}
  
  
  ngOnInit() {
    console.log('searchselect schema',this.schema)
  }
  search(){
    console.log('searchselect input field',this.schema.value)
    var obj={
      name:this.schema.name,
      value:this.schema.value,
      view:this.schema.view,
      index:this.index
    }
    if(obj.value!==undefined){
      console.log('init addmorestatic searchselect value emited',obj)
      this.dropdown=[]
      this.db.dbsearchselect(obj).then((r:any)=>{
        this.result=r;
        console.log('init addmorestatic dbsearchselect value promised',this.result)
        for(let i=0;i<this.result.length;i++){
          this.dropdown[i]={name:this.result[i][this.schema.name],result:this.result[i]}
          console.log('this.result[i]',this.result[i])
        }
        if(this.result.length==0){
          this.dropdown=[]
        }
        console.log('dropdown',this.dropdown)
        if(this.dropdown.length>0){this.show=true}
        if(obj.value===''){this.show=false}
      });
    }
  }   
  select(value){
    this.show=false
console.log('init addmorestatic select value',value)
var obj={
  name:this.schema.name,
  view:this.schema.view,
  index:this.index,
  value:value.name,
  result:value.result
}
this.schema.value=value.name
    this.notify.emit(obj) 
  }
}
