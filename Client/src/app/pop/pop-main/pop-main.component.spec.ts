import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopMainComponent } from './pop-main.component';

describe('PopMainComponent', () => {
  let component: PopMainComponent;
  let fixture: ComponentFixture<PopMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
