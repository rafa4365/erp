import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../lbd/services/script-loader.service';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';
import { purchase_order } from '../pop-schema';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss']
})
export class PurchaseOrderComponent implements OnInit {

  constructor(
    private navbarTitleService: NavbarTilteService,
    private _script: ScriptLoaderService,
  ) {
    Object.assign(this, {purchase_order})        
    }

  ngOnInit() {
    this.navbarTitleService.updateTitle('Purchase Order');

  }
  ngAfterViewInIt() {
    this._script.load('app-extended-forms',
      '../../assets/js/bootstrap-checkbox-radio-switch-tags.js',
      '../../assets/js/light-bootstrap-dashboard.js')
  }

}
