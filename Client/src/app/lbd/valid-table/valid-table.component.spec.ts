import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidTableComponent } from './valid-table.component';

describe('ValidTableComponent', () => {
  let component: ValidTableComponent;
  let fixture: ComponentFixture<ValidTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
