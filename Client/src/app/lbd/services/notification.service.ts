import { Injectable } from '@angular/core';
declare var $;

export enum NotificationType {
  Info = 1,
  Success,
  Warning,
  Danger
}

export class NotificationOptions {
  public message: string;
  public icon: string = null;
  public timer = 10;
  public type: NotificationType = NotificationType.Info;
  public from = 'top';
  public align = 'right';

  public constructor(
    fields: {
      message: string,
      icon?: string,
      timer?: number,
      type?: NotificationType,
      from?: string,
      align?: string
    }) {

    this.message = fields.message;
    this.icon = fields.icon || this.icon;
    this.timer = fields.timer || this.timer;
    this.type = fields.type || this.type;
    this.from = fields.from || this.from;
    this.align = fields.align || this.align;
  }
}

@Injectable()
export class NotificationService {

  constructor() { }

  public notify(options: NotificationOptions): void {
    //console.log('options',options)
    
    let typeString;
    switch (options.type) {
      case NotificationType.Success:
        typeString = 'success';
        //console.log('typeString',typeString)
        
        break;
      case NotificationType.Warning:
        typeString = 'warning';
        //console.log('typeString',typeString)
        
        break;
      case NotificationType.Danger:
        typeString = 'danger';
        //console.log('typeString',typeString)
        
        break;
      default:
        typeString = 'info';
        //console.log('typeString',typeString)
        
        break;
    }

    $.notify(
      {
        icon: options.icon,
        message: options.message
      },
      {
        type: typeString,
        timer: options.timer,
        placement: {
          from: options.from,
          align: options.align
        }
      });
  }
}
