import { NavItem, NavItemType } from '../lbd/services/interface';
import { validation } from '../validation';

var date = {
  icons: {
    time: "fa fa-clock-o",
    date: "fa fa-calendar",
    up: "fa fa-chevron-up",
    down: "fa fa-chevron-down",
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-screenshot',
    clear: 'fa fa-trash',
    close: 'fa fa-remove'
  }
}
//--------------------------pop main---------------------

export var sidebarData = {
  sidenavItems: [

    { title: 'Pop Summary', routerLink: 'popSummary', iconClass: 'pe-7s-display1' },
    { title: 'Supplier', routerLink: 'supplier', iconClass: 'pe-7s-add-user' },
    { title: 'Purchase Invoice', routerLink: 'purchaseInvoice', iconClass: 'pe-7s-note2' },
    { title: 'Purchase Order', routerLink: 'purchaseOrder', iconClass: 'pe-7s-hammer' },
    { title: 'Inventory', routerLink: 'popInventory', iconClass: 'pe-7s-drawer' },
    // { title: 'Pop Reports', routerLink: 'popReports', iconClass: 'pe-7s-config' },
    { title: 'Expense Category', routerLink: 'expenseCategory', iconClass: 'pe-7s-tools' },
    { title: 'Expense', routerLink: 'expense', iconClass: 'pe-7s-credit' }


  ],
  logoDropdown: {
    header: "Name",
    list: [
      { title: "My Profile", link: "profile" },
      // { title: "Edit Profile" },
      // { title: "Settings" }
    ]
  },
  sidebarMeta: {
    headerTextFull: 'Paper Clip',
    headerTextMini: 'PC',
    headerLink: 'http://www.google.com',
    headerLogoImg: '/assets/images/profile.jpg',
    backgroundColor: 'blue',
    backgroundImg: '../../assets/images/full-screen-image-3.jpg',
  }
}

export var navItems: NavItem[] = [

  {
    type: NavItemType.NavbarRight,
    title: 'Settings',
    iconClass: 'fa fa-list',
    ListClass: 'dropdown dropdown-with-icons  ',
    dropdownClass: 'dropdown-menu dropdown-with-icons',

    dropdownItems: [


      { title: 'Log out', icon: 'pe-7s-close-circle', class: 'text-danger' }
    ]
  },

];


//--------------------p expense-----------------------------

export var expense_exp = {
  title: 'Expense',
  type: null,
  table: {
    docDesign: 'pop',
    view: 'expense',
    export: false,
    colView: false,
    gridView: false,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: false,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [
    {
      col: [
        { class: 'col-md-5', data: { label: 'Expense #', name: 'expensenumber', fieldtype: 'auto', showCol: true, prefix: 'BE', value: '' } },
        { class: 'col-md-3', data: { label: 'Issuse Date', options: date, enable: 'before', class: 'datetimepicker', placeholder: 'date here', name: 'issusedate', fieldtype: 'datepicker', showCol: true, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } }
      ]
    },

    {
      col: [
        { class: 'col-md-6', data: { label: 'Summary', name: 'summary', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Summary', value: '', valid: '', ragexp: validation.shortdescription.regex, class: 'form-control', err: validation.shortdescription.msg } }
      ]
    },

    {
      col: [
        { class: 'col-md-12', data: { name: 'expense', fieldtype: 'addmoreexpensevalid', showCol: false, view: 'expenseCategory', docDesign: 'pop', searchvalue: 'expencename', value: '', valid: null } }
      ]
    },

    {
      col: [
        { class: 'col-md-12', data: { label: 'Note', name: 'note', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: true, placeholder: 'Here can be your description or note', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.regex } },
      ]
    }

  ]
}

//-------------------------------p expence category-------------------------
export var expense_category = {
  title: 'Business Expence Category',
  type: null,
  table: {
    docDesign: 'pop',
    view: 'expenseCategory',
    export: false,
    colView: false,
    gridView: false,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: false,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [
    {
      col: [
        { class: 'col-md-6', data: { label: 'Expence Name', name: 'expencename', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Expence Name', value: '', valid: '', ragexp: validation.shortdescription.regex, class: 'form-control', err: validation.shortdescription.msg } },
      ]
    },
  ]
}

//--------------------------------p inventory----------------------------

export var pop_inventory = {
  title: 'Inventory',
  type: null,
  table: {
    docDesign: 'pop',
    view: 'inventory',
    export: false,
    colView: false,
    gridView: false,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: false,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [
    {
      col: [
        { class: 'col-md-5', data: { label: 'Item Code #', name: 'itemcode', fieldtype: 'auto', showCol: true, prefix: 'IC', value: '' } },
        { class: 'col-md-3', data: { label: 'Item Name', name: 'itemname', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Item Name', value: '', valid: '', ragexp: validation.itemname.regex, class: 'form-control', err: validation.itemname.msg } },
        { class: 'col-md-4', data: { label: 'Unit Name', name: 'unitname', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Unit Name', value: '', valid: '', ragexp: validation.unit.regex, class: 'form-control', err: validation.unit.msg } }
      ]
    },
    {
      col: [
        { class: 'col-md-4', data: { label: 'Cost Price', name: 'costprice', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Cost Price', value: '', valid: '', ragexp: validation.price.regex, class: 'form-control', err: validation.price.msg } },
        { class: 'col-md-4', data: { label: 'Sale Price', name: 'saleprice', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Sale Price', value: '', valid: '', ragexp: validation.price.regex, class: 'form-control', err: validation.price.msg } },
      ]
    },


    {
      col: [
        { class: 'col-md-12', data: { label: 'Description', name: 'description', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.shortdescription.regex, class: 'form-control', err: validation.shortdescription.msg } },
      ]
    },

  ]
}


//--------------------------p invoice-------------------------


export var purchase_invoice = {
  title: 'Purchase Invoice',
  type: null,
  table: {
    docDesign: 'pop',
    view: 'purchaseInvoice',
    status: {
      type: 'Payable',
      success: 'Paid'
    },
    export: false,
    colView: true,
    gridView: true,
    refresh: true,
    search: false,
    dateRange: true,
    settings: false,
    options: {
      display: true,
      rowView: true,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [
    {
      col: [
        { class: 'col-md-4', data: { label: 'Purchase Order #', name: 'reference_order', fieldtype: 'refnum', showCol: true, value: '', valid: '', prefix: 'PO', view: 'purchaseorder', docDesign: 'pop', searched: 'reference_order' } },

        { class: 'col-md-4 col-md-offset-4', data: { label: 'Purchase Invoice #', name: 'reference', fieldtype: 'auto', showCol: false, prefix: 'PI', value: '' } }

        // { class: 'col-md-4', data: { label: 'Purchase Order #', name: 'purchase_Order', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'POXXXXX', value: '', valid: '', ragexp: validation.purchaseorder.regex, class: 'form-control', err: validation.purchaseorder.msg } },
        // { class: 'col-md-4 col-md-offset-4', data: { label: 'Purchase Invoice #', name: 'reference', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'PIXXXXXX', value: '', valid: '', ragexp: validation.purchaseinvoice.regex, class: 'form-control', err: validation.purchaseinvoice.msg } }
      ]
    },
    {
      col: [
        { class: 'col-md-4', data: { label: 'Invoice Date', options: date, enable: 'now', class: 'datetimepicker', placeholder: 'Invoice date here', name: 'invoiceDate', fieldtype: 'datepicker', showCol: true, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
        { class: 'col-md-4 col-md-offset-4', data: { label: 'Due Date', options: date, enable: 'after', class: 'datetimepicker', placeholder: 'Due Date', name: 'DueDate', fieldtype: 'datepicker', showCol: false, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'Supplier', fieldtype: 'nameresult', showCol: false, view: 'supplier', docDesign: 'pop', searchvalue: 'name', resultvalue: 'address', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Description', name: 'description', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Description', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.msg } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'inventory', fieldtype: 'addmorevalid', showCol: false, view: 'inventory', docDesign: 'pop', searchvalue: 'costprice', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { label: 'Internal Information', name: 'InternalInformation', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.information.regex, class: 'form-control', err: validation.information.msg } },
      ]
    },
  ]

}

//-----------------------------p order------------------------

export var purchase_order = {
  title: 'Purchase Order',
  type: null,
  table: {
    docDesign: 'pop',
    view: 'purchaseorder',
    export: false,
    colView: false,
    gridView: false,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: true,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [

    {
      col: [
        { class: 'col-md-4 ', data: { label: 'Purchase Order #', name: 'reference_order', fieldtype: 'auto', showCol: false, prefix: 'PO', value: '' } }

        // { class: 'col-md-4 ', data: { label: 'Purchase Order #', name: 'reference', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'POXXXXXX', value: '', valid: '', ragexp: validation.purchaseorder.regex, class: 'form-control', err: validation.purchaseorder.msg } }
      ]
    },
    {
      col: [
        { class: 'col-md-4', data: { label: 'Issue Date', options: date, enable: 'now', class: 'datetimepicker', placeholder: 'Issue date here', name: 'issueDate', fieldtype: 'datepicker', showCol: true, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
        { class: 'col-md-4 col-md-offset-4', data: { label: 'Delivery Date', options: date, enable: 'after', class: 'datetimepicker', placeholder: 'Delivery Date', name: 'DeliveryDate', fieldtype: 'datepicker', showCol: false, value: '', valid: '', ragexp: '^([a-zA-z0-9 /:]{5,})$', err: 'this is invalid error of text area' } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'Supplier', fieldtype: 'nameresult', showCol: false, view: 'supplier', docDesign: 'pop', searchvalue: 'name', resultvalue: 'address', value: '', valid: null } },
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Description', name: 'description', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Description', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.regex } },
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { name: 'inventory', fieldtype: 'addmorevalid', showCol: false, view: 'inventory', docDesign: 'pop', searchvalue: 'costprice', value: '', valid: null } },
      ]
    },

    {
      col: [
        { class: 'col-md-12', data: { label: 'Delivery Address', name: 'deliveryaddress', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.address.regex, class: 'form-control', err: validation.address.msg } },
      ]
    },

    {
      col: [
        { class: 'col-md-12', data: { label: 'Delivery Instructions', name: 'deliveryinstructions', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.description.regex, class: 'form-control', err: validation.description.msg } },
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Authorised By', name: 'authorisedby', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Authorised By', value: '', valid: '', ragexp: validation.fullname.regex, class: 'form-control', err: validation.fullname.msg } },
      ]
    },


  ]
}
//----------------------------pop  supplier-----------------------------------

export var supplier = {
  title: 'Suppliers',
  type: null,
  table: {
    docDesign: 'pop',
    view: 'supplier',
    export: false,
    colView: true,
    gridView: true,
    refresh: true,
    search: false,
    dateRange: false,
    settings: false,
    options: {
      display: true,
      rowView: false,
      rowDelete: true,
      rowUpdate: true,
    },
  },
  numrow: [10, 25, 100],
  rows: [
    {
      col: [
        { class: 'col-md-2 ', data: { label: 'Supplier #', name: 'reference', fieldtype: 'auto', showCol: false, prefix: 'SUP', value: '' } },

        { class: 'col-md-4', data: { label: 'Name', name: 'name', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Name', value: '', valid: '', ragexp: validation.fullname.regex, class: 'form-control', err: validation.fullname.msg } },
        { class: 'col-md-3', data: { label: 'TelePhone', name: 'telephone', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'TelePhone', value: '', valid: '', ragexp: validation.phone.regex, class: 'form-control', err: validation.phone.msg } },
        { class: 'col-md-3', data: { label: 'Mobile', name: 'mobile', type: 'text', fieldtype: 'input', showCol: true, placeholder: 'Mobile', value: '', valid: '', ragexp: validation.phone.regex, class: 'form-control', err: validation.phone.msg } }
      ]
    },
    {
      col: [
        { class: 'col-md-6', data: { label: 'Email address', name: 'email', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Email', value: '', valid: '', ragexp: validation.email.regex, class: 'form-control', err: validation.email.msg } },
        { class: 'col-md-6', data: { label: 'Address', name: 'address', type: 'text', fieldtype: 'input', showCol: false, placeholder: 'Address', value: '', valid: '', ragexp: validation.address.regex, class: 'form-control', err: validation.address.msg } }
      ]
    },
    {
      col: [
      ]
    },
    {
      col: [
        { class: 'col-md-12', data: { label: 'Additional Information', name: 'AdditionalInfo', type: 'textarea', rows: '5', fieldtype: 'textarea', showCol: false, placeholder: 'Here can be your description', value: '', valid: '', ragexp: validation.information.regex, class: 'form-control', err: validation.information.msg } }
      ]
    },

  ]
}