import { Component, OnInit } from '@angular/core';
import { PouchbService } from '../../lbd/services/pouchb.service';

@Component({
  selector: 'app-pop-summary',
  templateUrl: './pop-summary.component.html',
  styleUrls: ['./pop-summary.component.scss']
})
export class PopSummaryComponent implements OnInit {

  //--------------------vertical bar  chart of Customer summary ----------------

  customer_ChartData = [];
  customer_View: any[] = [400, 400];

  // options of vertical bar chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Customer';
  showYAxisLabel = true;
  yAxisLabel = 'Total';

  colorScheme = {
    domain: [
      '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
      '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
    ]
  };

  //--------------------pie-grid chart of posInventory ----------------

  posInventory_ChartData = [];
  posInventory_View: any[] = [400, 400];

  posInventory_colorScheme = {
    domain: [
      '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
      '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
    ]
  };

  //--------------------pie-grid chart of pop inventory ----------------

  singleInventoryChartData = [];
  viewInventoryPOP: any[] = [400, 400];

  colorSchemeInventory = {
    domain: [
      '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
      '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
    ]
  };

  // line, area
  autoScale = true

  // line, area
  posInventory_autoScale = true

  //--------------------pie-grid chart of  ----------------
  expenseChartData = [];
  viewExpense: any[] = [500, 500];
  showLegendpExpense = true;

  showLabelsExpense = true;
  explodeSlicesExpense = false;
  doughnutExpense = false;

  colorSchemeExpense = {
    domain: [
      '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
      '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
    ]
  };
  //-----------------  sale Invoice bar chart------------

  saleInvoice_view: any[] = [400, 400];
  saleInvoice_ChartData = [];

  saleInvoice_xAxisLabel = 'date';
  saleInvoice_yAxisLabel = 'total';

  saleInvoice_colorScheme = {
    domain: [
      '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
      '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
    ]
  };

  //--------------------pie-grid chart of remaining Qty ----------------

  remQty_ChartData = [];
  remQty_View: any[] = [400, 400];

  remQty_colorScheme = {
    domain: [
      '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
      '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
    ]
  };

  // line, area
  remQty_autoScale = true

  chartData_remQty: any = [];
  view_remQty: any[] = [400, 400];

  colorScheme_remQty = {
    domain: [
      '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
      '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
    ]
  };


  customerSummary: any;
  finalCustomerSummary: any;

  posInventorySummary: any = [];
  finalPosInventorySummary: any = [];


  saleInvoiceSummary: any;
  saleOrderSummary: any;
  deliveryNoteSummary: any;
  inventorySummary: any = [];
  finalInventorySummary: any = [];

  remainingInventory: any = [];


  dateRangePicker: any;


  constructor(private db: PouchbService, ) {
  }
  onSelect(event) {
    console.log(event);
  }

  datePicker(value) {
    this.dateRangePicker = value;
    console.log("in pos summary date values", this.dateRangePicker)

    //---------------Customer Summary-------------

    var mapReduceFunCustomer = {
      map: function (doc, emit) {
        if (doc.view === "purchaseInvoice") {
          emit([doc.invoiceDate, doc.Supplier.name], doc.total)
        }
      },
      reduce: '_stats'
    };
    let optionsCustomer: Object = {

      reduce: true,
      group: true,
      startkey: [value.start, '', ''],
      endkey: [value.end, '', ''] 

    };

    this.db.summaryFun(mapReduceFunCustomer, optionsCustomer).then((sum: any) => {
      console.log("------Customer function summary ts--------", this.customerSummary = sum)
      var totalPrice = 0;
      var dataArray = [];
      var finalArray = [];

      var itemName;
      var qty = 0;

      for (var i = 0; i < this.customerSummary.length; i++) {

        totalPrice += this.customerSummary[i].value.sum;

        itemName = this.customerSummary[i].key[1];
        qty = this.customerSummary[i].value.sum


        var obj = finalArray.filter((obj) => {

          return obj.itemName === itemName;

        })[0];

        if (obj) {
          for (var j = 0; j < finalArray.length; j++) {
            console.log("if obj", obj, "final array", finalArray, "length", finalArray.length)
            if (!(itemName === undefined)) {
              if (finalArray[j].itemName === itemName) {
                console.log("finalArray[itemName] === itemName", j, finalArray[j].itemName === itemName)

                finalArray[j].qty += qty
              }
            }
          }
        }
        else {

          console.log("else obj", obj)

          if (!(itemName === undefined)) {
            console.log("else if item Name ", itemName, qty)

            finalArray.push({
              itemName: itemName,
              qty: qty,
            });
          }
        }
      }
      console.log("total price", totalPrice);
      this.customerSummary.totalPrice = totalPrice;
      console.log("finalllllllllllllllllllllllllllll", finalArray);

      this.finalCustomerSummary = finalArray;

      for (var k = 0; k < finalArray.length; k++) {
        dataArray[k] =
          {
            name: finalArray[k].itemName,
            value: finalArray[k].qty
          }

      }
      console.log("charts data of inventory", dataArray);



      console.log("single object", dataArray);
      this.customer_ChartData = dataArray;
    });

    //---------------------- POP Inventory Summary
    var mapReduceFunInventory = {
      map: function (doc, emit) {

        if (doc.view === "purchaseInvoice") {
          for (var i = 0; i < doc.inventory.length; i++) {
            emit([doc.invoiceDate, doc.inventory[i].name, doc.inventory[i].up], [parseInt(doc.inventory[i].qty), parseInt(doc.inventory[i].amount)])
          }
        }
      },
      reduce: '_stats'
    };

    let optionsInventory: Object = {
      reduce: true,
      group: true,
      startkey: [value.start, '', ''],
      endkey: [value.end, '', '']

    };

    this.db.summaryFun(mapReduceFunInventory, optionsInventory).then(
      (sum: any) => {
        console.log("------ Inventory summary ts--------", this.inventorySummary = sum)
        var totalPrice = 0;
        var dataArray = [];
        var finalArray = [];

        var itemName;
        var unitPrice;
        var qty = 0;

        for (var i = 0; i < this.inventorySummary.length; i++) {

          totalPrice += this.inventorySummary[i].value.sum[1];

          itemName = this.inventorySummary[i].key[1];
          unitPrice = this.inventorySummary[i].key[2];
          qty = this.inventorySummary[i].value.sum[0]


          var obj = finalArray.filter((obj) => {

            return obj.itemName === itemName;

          })[0];

          if (obj) {
            for (var j = 0; j < finalArray.length; j++) {
              console.log("if obj", obj, "final array", finalArray, "length", finalArray.length)
              if (!(itemName === undefined)) {
                if (finalArray[j].itemName === itemName) {
                  console.log("finalArray[itemName] === itemName", j, finalArray[j].itemName === itemName)

                  finalArray[j].qty += qty
                }
              }
            }
          }
          else {

            console.log("else obj", obj)

            if (!(itemName === undefined)) {
              console.log("else if item Name ", itemName, qty)

              finalArray.push({
                itemName: itemName,
                unitPrice: unitPrice,
                qty: qty,
              });
            }
          }
        }

        console.log("total price", totalPrice);
        this.inventorySummary.totalPrice = totalPrice;
        console.log("finalllllllllllllllllllllllllllll", finalArray);

        this.finalInventorySummary = finalArray;
        for (var k = 0; k < finalArray.length; k++) {
          dataArray[k] =
            {
              name: finalArray[k].itemName,
              value: finalArray[k].qty
            }

        }
        console.log("charts data of inventory", dataArray);
        this.singleInventoryChartData = dataArray;

      });

    //---------------------- POS Inventory Summary
    var mapReduceFun_POSInventory = {
      map: function (doc, emit) {

        if (doc.view === "saleInvoice") {
          for (var i = 0; i < doc.inventory.length; i++) {
            emit([doc.invoiceDate, doc.inventory[i].name, doc.inventory[i].up], [parseInt(doc.inventory[i].qty), parseInt(doc.inventory[i].amount)])
          }
        }
      },
      reduce: '_stats'
    };

    let options_POSInventory: Object = {
      reduce: true,
      group: true,
      startkey: [value.start, '', ''],
      endkey: [value.end, '', '']

    };

    this.db.summaryFun(mapReduceFun_POSInventory, options_POSInventory).then(
      (sum: any) => {
        console.log("------ Inventory summary ts--------", this.posInventorySummary = sum)
        var totalPrice = 0;
        var dataArray = [];
        var finalArray = [];

        var itemName;
        var unitPrice;
        var qty = 0;

        for (var i = 0; i < this.posInventorySummary.length; i++) {

          totalPrice += this.posInventorySummary[i].value.sum[1];

          itemName = this.posInventorySummary[i].key[1];
          unitPrice = this.posInventorySummary[i].key[2];
          qty = this.posInventorySummary[i].value.sum[0]


          var obj = finalArray.filter((obj) => {

            return obj.itemName === itemName;

          })[0];

          if (obj) {
            for (var j = 0; j < finalArray.length; j++) {
              console.log("if obj", obj, "final array", finalArray, "length", finalArray.length)
              if (!(itemName === undefined)) {
                if (finalArray[j].itemName === itemName) {
                  console.log("finalArray[itemName] === itemName", j, finalArray[j].itemName === itemName)

                  finalArray[j].qty += qty
                }
              }
            }
          }
          else {

            console.log("else obj", obj)

            if (!(itemName === undefined)) {
              console.log("else if item Name ", itemName, qty)

              finalArray.push({
                itemName: itemName,
                unitPrice: unitPrice,
                qty: qty,
              });
            }
          }
        }

        console.log("total price", totalPrice);
        this.posInventorySummary.totalPrice = totalPrice;
        console.log("finalllllllllllllllllllllllllllll", finalArray);
        this.finalPosInventorySummary = finalArray;
        for (var k = 0; k < finalArray.length; k++) {
          dataArray[k] =
            {
              name: finalArray[k].itemName,
              value: finalArray[k].qty
            }

        }
        console.log("charts data of inventory", dataArray);
        this.posInventory_ChartData = dataArray;

        //----------------------- rem_Qty Data------------------
        var charData = [];
        for (var k = 0; k < this.finalInventorySummary.length; k++) {
          for (var j = 0; j < this.finalPosInventorySummary.length; j++) {
            if (this.finalInventorySummary[k].itemName === this.finalPosInventorySummary[j].itemName) {
              this.remainingInventory[j] =
                {
                  name: this.finalPosInventorySummary[j].itemName,
                  popQty: this.finalInventorySummary[k].qty,
                  posQty: this.finalPosInventorySummary[j].qty,
                  remQty: this.finalInventorySummary[k].qty - this.finalPosInventorySummary[j].qty
                }
              charData[j] = {
                name: this.finalPosInventorySummary[j].itemName,
                value: this.finalInventorySummary[k].qty - this.finalPosInventorySummary[j].qty
              }
            }
          }
        }
        console.log("remaing qtyyyyyyyyyyyyyyyyyyyy", this.remainingInventory);

        this.chartData_remQty = charData;

      });


  }

  ngOnInit() {


    //---------------Supplier Summary-------------

    var mapReduceFunSupplier = {
      map: function (doc, emit) {
        if (doc.view === "purchaseInvoice") {
          emit(doc.Supplier.name, doc.total)
        }
      },
      reduce: '_stats'
    };
    let optionsSupplier: Object = {

      reduce: true,
      group: true,
    };

    // this.db.summaryFun(mapReduceFunSupplier, optionsSupplier).then((sum: any) => {
    //   console.log("------Supplier function summary ts--------", this.supplierSummary = sum)
    //   var totalPrice = 0;
    //   var dataArray = [];

    //   for (var i = 0; i < this.supplierSummary.length; i++) {
    //     dataArray[i] =
    //       {
    //         name: this.supplierSummary[i].key,
    //         value: this.supplierSummary[i].value.sum
    //       }
    //     totalPrice += this.supplierSummary[i].value.sum;
    //   }
    //   console.log("total price", totalPrice);
    //   console.log("single object", dataArray);

    //   this.supplierSummary.totalPrice = totalPrice;
    //   this.singleSupplierChartData = dataArray;
    // });

    //---------------inventory summary-------------

    var mapReduceFunInventory = {
      map: function (doc, emit) {

        if (doc.view === "purchaseInvoice") {
          for (var i = 0; i < doc.inventory.length; i++) {
            emit([doc.inventory[i].name, doc.inventory[i].up], [parseInt(doc.inventory[i].qty), parseInt(doc.inventory[i].amount)])
          }
        }
      },
      reduce: '_stats'
    };

    let optionsInventory: Object = {

      reduce: true,
      group: true,
    };

    // this.db.summaryFun(mapReduceFunInventory, optionsInventory).then(
    //   (sum: any) => {
    //     console.log("------ Inventory summary ts--------", this.inventorySummary = sum)
    //     var totalPrice = 0;
    //     var dataArray = [];
    //     for (var i = 0; i < this.inventorySummary.length; i++) {
    //       dataArray[i] =
    //         {
    //           name: this.inventorySummary[i].key[0],
    //           value: this.inventorySummary[i].value.sum[0]
    //         }
    //       totalPrice += this.inventorySummary[i].value.sum[1];
    //     }
    //     console.log("total price", totalPrice);
    //     console.log("charts data of inventory", dataArray);
    //     this.inventorySummary.totalPrice = totalPrice;

    //     this.singleInventoryChartData = dataArray;

    //   });

    //--------------  purchaseInvoice Summary----------
    var mapReduceFunInvoice = {
      map: function (doc, emit) {

        if (doc.view === "purchaseInvoice") {
          emit([doc.view, doc.invoiceDate], doc.total)
        }
      },
      reduce: '_stats'
    };

    let optionsInvoice: Object = {

      reduce: true,
      group: true,
    };

    // this.db.summaryFun(mapReduceFunInvoice, optionsInvoice).then((sum: any) => {
    //   console.log("------ purchaseInvoice summary ts--------", this.purchaseInvoiceSummary = sum)
    //   var dataArray = [];
    //   for (var i = 0; i < this.purchaseInvoiceSummary.length; i++) {
    //     dataArray[i] =
    //       {
    //         name: this.purchaseInvoiceSummary[i].key[1],
    //         value: this.purchaseInvoiceSummary[i].value.sum
    //       }


    //   }

    //   console.log("purchaseInvoice chart data", dataArray);
    //   this.invoiceChartData = dataArray;
    //   // [
    //   // {
    //   //   "name": "Germany",
    //   //   "series": [
    //   //     {
    //   //       "name": "2010",
    //   //       "value": 7300000
    //   //     },
    //   //     {
    //   //       "name": "2011",
    //   //       "value": 8940000
    //   //     }
    //   //   ]
    //   // }

    //   //   {
    //   //     "name": "USA",
    //   //     "series": [
    //   //       {
    //   //         "name": "2010",
    //   //         "value": 7870000
    //   //       },
    //   //       {
    //   //         "name": "2011",
    //   //         "value": 8270000
    //   //       }
    //   //     ]
    //   //   },

    //   //   {
    //   //     "name": "France",
    //   //     "series": [
    //   //       {
    //   //         "name": "2010",
    //   //         "value": 5000002
    //   //       },
    //   //       {
    //   //         "name": "2011",
    //   //         "value": 5800000
    //   //       }
    //   //     ]
    //   //   }
    //   // ];


    // });

    //---------------------purchaseOrder Summary----------
    var mapReduceFunOrder = {
      map: function (doc, emit) {

        if (doc.view === "purchaseorder") {
          emit(doc.view, doc.total)
        }
      },
      reduce: '_stats'
    };

    let optionsOrder: Object = {

      reduce: true,
      group: true,
    };

    // this.db.summaryFun(mapReduceFunOrder, optionsOrder).then((sum: any) => {
    //   console.log("------ purchaseOrder summary ts--------", this.purchaseOrderSummary = sum)

    // });

    //---------------------Expense Summary----------

    var mapReduceFunExpense = {
      map: function (doc, emit) {
        if (doc.view === "expense") {
          for (var i = 0; i < doc.expense.length; i++) {
            emit(doc.expense[i].name, parseInt(doc.expense[i].amount))
          }
        }
      },
      reduce: '_stats'
    };

    let optionsExpense: Object = {

      reduce: true,
      group: true,
    };
    // this.db.summaryFun(mapReduceFunExpense, optionsExpense).then((sum: any) => {
    //   console.log("@@@@@@@@@@@@@@@@----expense Result", this.expenseSummary = sum);
    //   var totalPrice = 0;
    //   var dataArray = [];

    //   for (var i = 0; i < this.expenseSummary.length; i++) {
    //     dataArray[i] =
    //       {
    //         name: this.expenseSummary[i].key,
    //         value: this.expenseSummary[i].value.sum
    //       }
    //     totalPrice += this.expenseSummary[i].value.sum;
    //   }
    //   console.log("supplier total", totalPrice);
    //   console.log("single object", dataArray);

    //   this.expenseSummary.totalPrice = totalPrice;
    //   this.expenseChartData = dataArray;

    // });

    //---------------POS Inventory summary-------------

    var mapReduceFun_posInventory = {
      map: function (doc, emit) {

        if (doc.view === "saleInvoice") {
          for (var i = 0; i < doc.inventory.length; i++) {
            emit([doc.inventory[i].name, doc.inventory[i].up], [parseInt(doc.inventory[i].qty), parseInt(doc.inventory[i].amount)])
          }
        }
      },
      reduce: '_stats'
    };

    let options_posInventory: Object = {

      reduce: true,
      group: true,
    };

    // this.db.summaryFun(mapReduceFun_posInventory, options_posInventory).then(
    //   (sum: any) => {
    //     console.log("------ posInventory summary ts--------", this.posInventorySummary = sum)
    //     var totalPrice = 0;
    //     var dataArray = [];
    //     for (var i = 0; i < this.posInventorySummary.length; i++) {
    //       dataArray[i] =
    //         {
    //           name: this.posInventorySummary[i].key[0],
    //           value: this.posInventorySummary[i].value.sum[0]
    //         }
    //       totalPrice += this.posInventorySummary[i].value.sum[1];
    //     }
    //     console.log("total price", totalPrice);
    //     console.log("charts data of inventory", dataArray);
    //     this.posInventorySummary.totalPrice = totalPrice;

    //     this.posInventory_ChartData = dataArray;

    //----------------------- rem_Qty Data------------------
    //   var charData = [];
    //   for (var k = 0; k < this.inventorySummary.length; k++) {
    //     for (var j = 0; j < this.posInventorySummary.length; j++) {
    //       if (this.inventorySummary[k].key[0] === this.posInventorySummary[j].key[0]) {
    //         this.remainingInventory[j] =
    //           {
    //             name: this.posInventorySummary[j].key[0],
    //             popQty: this.inventorySummary[k].value.sum[0],
    //             posQty: this.posInventorySummary[j].value.sum[0],
    //             remQty: this.inventorySummary[k].value.sum[0] - this.posInventorySummary[j].value.sum[0]
    //           }
    //         charData[j] = {
    //           name: this.posInventorySummary[j].key[0],
    //           value: this.inventorySummary[k].value.sum[0] - this.posInventorySummary[j].value.sum[0]
    //         }
    //       }
    //     }
    //   }
    //   console.log("remaing qtyyyyyyyyyyyyyyyyyyyy", this.remainingInventory);

    //   this.chartData_remQty = charData;

    // });

  }


}