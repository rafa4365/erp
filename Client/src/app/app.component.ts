import { Component } from '@angular/core';
import { PouchbService } from './lbd/services/pouchb.service';
import { ServerReplicationService } from './lbd/services/server-replication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private db: PouchbService,private rep: ServerReplicationService ){
    this.db.declare()
    this.rep.repli('192.168.1.30','localhost')
    
  }
}
