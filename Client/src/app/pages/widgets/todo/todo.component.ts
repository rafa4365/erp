import { Component, OnInit ,AfterViewInit} from '@angular/core';
import { ScriptLoaderService } from '../../../lbd/services/script-loader.service';
import { NavbarTilteService } from '../../../lbd/services/navbar-tilte.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  exp:any;

  date:any;
  range:any;
  select2:any;
  inven:any;
  nameresult:any;

  constructor(
    private navbarTitleService: NavbarTilteService,
    private _script: ScriptLoaderService,
  ) { 
    // console.clear() 
  }
  
  ngOnInit() {
    this.navbarTitleService.updateTitle('Todo');
    this.nameresult={
      name:{type:'text',aline:'right',fieldtype:'textarea',name:'adress',placeholder:'Enter Adress',err:"salary is required"},
      result:{type:'text',aline:'right',fieldtype:'textarea',name:'adress',placeholder:'Enter Adress',err:"salary is required"},
    }
    this.date={title:'Datetime Picker',class:'datetimepicker',placeholder:'Datetime Picker Here',options:{
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    }
      }}
  
    this.range={
      type: "double",
      grid: true,
      from: 1,
      to: 5,
      values: [0, 10, 100, 1000, 10000, 100000, 1000000]
    }
    this.inven=[
      [
      {type:'text',value:'',fieldtype:'searchselect',placeholder:'Enter Name',name:'adress',colclass:'col-md-3'},
      {type:'text',value:'',aline:'full',colclass:'col-md-3',fieldtype:'input',name:'descrip',placeholder:'discription',err:"salary is required"},
      {type:'number',value:'',aline:'full',colclass:'col-md-1',fieldtype:'input',name:'qty',placeholder:'qty',err:"country is required"}, 
      {type:'number',value:'',aline:'full',colclass:'col-md-1',fieldtype:'input',name:'unitprice',placeholder:'unit price',err:"country is required"}, 
      {type:'number',value:'',aline:'full',colclass:'col-md-2',fieldtype:'input',name:'dis',placeholder:'discount',err:"country is required"}, 
      {type:'number',value:'',aline:'full',colclass:'col-md-2',fieldtype:'input',name:'amount',placeholder:'amount',err:"country is required"}, 
    ]
  ]

    this.select2={
      
    }


    this.exp={
      table:{
        docDesign:'widgets',
        view:'todo',
        export:true,
        colView:true,
        gridView:true,
        refresh:true,
        search:true,
        dateRange:true,
      },  
      newform:true,
        form:[
          {type:'text',aline:'left',fieldtype:'input',name:'name',showCol:true,placeholder:'Enter Name',err:"name is required",range:'[3]'},
          {type:'text',aline:'right',fieldtype:'input',name:'salary',showCol:true,placeholder:'Enter Salary',err:"salary is required"},
          {type:'text',aline:'left',fieldtype:'input',name:'country',showCol:true,placeholder:'Enter Country Name',err:"country is required"}, 
          {type:'text',aline:'right',fieldtype:'input',name:'city',showCol:true,placeholder:'Enter City Name',err:"city is required"},
          
          {type:'text',aline:'left',schema:this.date,fieldtype:'date',name:'date',showCol:true,err:"name is required"},
          // {type:'text',aline:'right',schema:this.lang,fieldtype:'sselect',name:'language',showCol:true,err:"salary is required"},
          {type:'text',aline:'right',fieldtype:'textarea',name:'adress',placeholder:'Enter Adress',err:"salary is required"},
          // {type:'text',aline:'right',schema:this.lang,fieldtype:'select2',width:'365px',name:'adress',showCol:true,err:"salary is required"},
          {type:'text',aline:'right',fieldtype:'searchselect',placeholder:'Enter Adress',name:'adress',showCol:true,err:"salary is required"},
          // {type:'text',aline:'left',schema:this.lang,fieldtype:'typeAhead',name:'type',showCol:true,err:"salary is required"},
          
          // {type:'text',aline:'right',schema:this.range,fieldtype:'range',name:'range',showCol:true,err:"city is required"}  
          {type:'text',aline:'full',schema:this.inven,fieldtype:'addmore',name:'inventory',showCol:false}, 
          {schema:this.inven,fieldtype:'nameresult'}, 
        ],

      numrow:[2,6,8]
    }
  }
  ngAfterViewInIt(){
    this._script.load('app-extended-forms',
    '../../assets/js/bootstrap-checkbox-radio-switch-tags.js',
    '../../assets/js/light-bootstrap-dashboard.js' )
  }

}
// ,mlml