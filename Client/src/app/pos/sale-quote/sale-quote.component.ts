import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../lbd/services/script-loader.service';
import { NavbarTilteService } from '../../lbd/services/navbar-tilte.service';
import { sale_quote } from '../pos-schema';


@Component({
  selector: 'app-sale-quote',
  templateUrl: './sale-quote.component.html',
  styleUrls: ['./sale-quote.component.scss']
})
export class SaleQuoteComponent implements OnInit {

  exp: any;

  date: any;
  lang: any;
  color: any;
  range: any;
  select2: any;
  inven: any;

  constructor(
    private navbarTitleService: NavbarTilteService,
    private _script: ScriptLoaderService,
  ) {
    Object.assign(this, { sale_quote })
  }

  ngOnInit() {
    this.navbarTitleService.updateTitle('Sale Quote');
  }
  ngAfterViewInIt() {
    this._script.load('app-extended-forms',
      '../../assets/js/bootstrap-checkbox-radio-switch-tags.js',
      '../../assets/js/light-bootstrap-dashboard.js')
  }

}
