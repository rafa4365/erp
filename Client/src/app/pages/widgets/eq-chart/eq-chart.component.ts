import { Component, OnInit } from '@angular/core';
import { eq_data } from './data';
import { NavbarTilteService } from '../../../lbd/services/navbar-tilte.service';

@Component({
  selector: 'app-eq-chart',
  templateUrl: './eq-chart.component.html',
  styleUrls: ['./eq-chart.component.scss']
})
export class EqChartComponent implements OnInit {

  constructor(private navbarTitleService: NavbarTilteService) {
    Object.assign(this, {eq_data})    
  }

  ngOnInit() {
    this.navbarTitleService.updateTitle('Eq Charts');    
    
  }


}
