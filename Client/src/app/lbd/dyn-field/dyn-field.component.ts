import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { DynFieldService } from '../services/dyn-field.service';

@Component({
  selector: 'app-dyn-field',
  templateUrl: './dyn-field.component.html',
  styleUrls: ['./dyn-field.component.scss']
})

export class DynFieldComponent implements OnInit {
  @Input() public schema:any;
  @Input() public UpdateSchema:any;
  @Output() notify=new EventEmitter<any>();
  
  
constructor(private _DFS:DynFieldService) { }

  ngOnInit() {

     this.subComponents(this.UpdateSchema);
     console.log('dyn fffff this.subComponents(this.UpdateSchema);',this.UpdateSchema)
   }
  subComponents(element){
    console.log('dyn fffff this.subComponents(this.UpdateSchema);',element)
    
      if(element){       
        if(element.data.fieldtype==='datepicker'){
          // this.C_udata.datepicker=this.updateValues[element.data.name]
          this.UpdateSchema=element.data.value
        }
        if(element.data.fieldtype==='addmorevalid'){
          this.UpdateSchema=element.data.value          
        }
        if(element.data.fieldtype==='addmoreexpensevalid'){
          this.UpdateSchema=element.data.value          
          
        }
        if(element.data.fieldtype==='nameresult'){
          this.UpdateSchema=element.data.value          
          
        }
        if(element.data.fieldtype==='refnum'){
          this.UpdateSchema=element.data.value          
          
        }
      }
   }
   date(value){
     console.log(`date picker value emitted ${JSON.stringify(value)}`)
     this.schema.data.value=value.value
   }
   nameSelect(value){
    console.log(`NameResult value emitted ${JSON.stringify(value)}`)
    this.schema.data.value=value  
    if(value!==null){
      this.schema.data.valid=true    
    }  
  }
   addmorevalid(value){
     
      this.check(value)
   }
   addmoreexpensevalid(value){
    this.check(value)
    
   }
   addmorepayslip(value){
    this.check(value)
    
   }
   autoref(value){
    this.schema.data.value=value
    this.schema.data.valid=true
  }

   check(value){
  console.log(` value emitted ${JSON.stringify(value)}`,value)
  console.log(` value emitted`,value.length)
  
  this.schema.data.value=value
  
  if(this.schema.data.value.length > 0){
    this.schema.data.valid=true   
    console.log('grester',this.schema.data.valid) 
  }
  else {
    this.schema.data.valid=false   
    console.log('less',this.schema.data.valid) 
    
    
  }
}

refSaleOrder(value){
  console.log('ref sale orderrrrrrrrrrrrrrrrrrrr',value) 
  
  this.notify.emit(value);
  // setTimeout(()=>{
    // this.subComponents(this.UpdateSchema);
  //  }, 3000);
  

}
}
