import {Component, OnInit, trigger, state, style, transition, animate} from '@angular/core';
import { NavbarTilteService } from '../../../lbd/services/navbar-tilte.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  public formData: any;
  public userAbout: string;

  constructor(private navbarTitleService: NavbarTilteService) { }
  public ngOnInit() {
    this.navbarTitleService.updateTitle('User Profile');

    this.userAbout = '"Lamborghini Mercy <br>Your chick she so thirsty <br>I\'m in that two seat Lambo"';

    this.formData = {
      username: 'michael23',
      email: '',
      firstName: 'Mike',
      lastName: 'Andrew',
      address: 'Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09',
      city: 'Mike',
      country: 'Andrew',
      postCode: null,
      aboutMe: 'Lamborghini Mercy, Your chick she so thirsty, I\'m in that two seat Lambo.'
    };
  }

  public onSubmit() {
    //console.log('Submitting values', this.formData);
  }

}
