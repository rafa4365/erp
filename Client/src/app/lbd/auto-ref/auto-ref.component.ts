import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-auto-ref',
  templateUrl: './auto-ref.component.html',
  styleUrls: ['./auto-ref.component.scss']
})
export class AutoRefComponent implements OnInit {
  @Input() schema;
  @Input() update;
  @Output() notify = new EventEmitter<any>();

num;
  constructor() { }

  ngOnInit() {
    console.log('autoooooo',this.update)
    if(this.update){
      this.num=this.update.data.value
    console.log('autoooooo nummmmmmmmmmmm',this.num)
    
    }else{
      this.num=`${this.schema.prefix}${Date.now().toString()}` ;
      this.notify.emit(this.num);
    }
  }


}
