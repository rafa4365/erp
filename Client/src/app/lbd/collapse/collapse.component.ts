import { Component, OnInit,Input ,AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../lbd/services/script-loader.service';

@Component({
  selector: 'app-collapse',
  templateUrl: './collapse.component.html',
  styleUrls: ['./collapse.component.scss']
})
export class CollapseComponent implements OnInit {
  @Input()
  public title: string;
  @Input()
  public subTitle: string;
  @Input()
  public toggleId: string;
  @Input()
  public toggleType: string;
  @Input() 
  public data: string;
  @Input()
  public properties: any;
  constructor(private _script: ScriptLoaderService) { }

  ngOnInit() {
  }
  ngAfterViewInit(){
    this._script.load('app-collapse',
    '../../assets/js/light-bootstrap-dashboard.js');
  }

}
