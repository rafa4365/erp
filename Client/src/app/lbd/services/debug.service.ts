import { Injectable } from '@angular/core';
import { global } from './debug.config';
declare var console:any;

@Injectable()
export class DebugService {

show=false
  constructor() {}
   check(component,local){
    //  console.log('local:'+local +'   ' +'global:'+global.start)
    if(global.start && local){
      this.show=true      
      console.log(`%c debug ♥ service { Global & Component mood of ${component} } is ON 😘 !!! `, ' ;font-size:20px');      
    }else
    if(!global.start && !local){
      this.show=false
    }else
    if(global.start && !local){
      this.show=true
      console.log('%c debug ♥ service { Global moode } is ON 😘 !!! ', 'color: #af2cc5; font-weight:bold;font-size:20px');
    }else
    if(local && !global.start){
      this.show=true
       console.log(`%c debug ♥ service { Component moode of ${component} } is ON 🤓  !!! `, 'color: #af2cc5; font-weight:bold; font-size:20px');
    }
   }
   
   clog(string,type:any,value:any=''){
   
     if( this.show ){

       switch(type){
         case 'i':
         console.log(`%c ${string} :`,`color:#00d3ee ; font-weight:bold;font-size:14px`,value);
         break;
         case 'w':
         console.log(`%c ${string} :`,`color:#ffa21a; font-weight:bold;font-size:14px`,value);
         break;
         case 'e':
         console.log(`%c ${string} :`,`color:#f55a4e; font-weight:bold;font-size:14px`,value);
         break;  
         case 's':
         console.log(`%c ${string} :`,`color:#5cb860; font-weight:bold;font-size:14px`,value);
         break;
        }     
      }

   }


}
