var express = require('express');
var PouchDB = require('pouchdb');
var cors = require('cors')
var colors = require('colors');

var app = express();
var pouchdb = require('express-pouchdb')({
    mode: 'fullCouchDB',
    overrideMode: {
        include: ['routes/fauxton']
    }
});

var corsOptions = {
    origin: "http://localhost:4200",
    credentials: true
};

pouchdb.setPouchDB(require('pouchdb'));


app.use('/', cors(corsOptions), pouchdb);



app.listen(3000, function () {
    console.log('Pouchdb Server Listening On Port 3000....!!!'.rainbow.bold); // rainbow

});